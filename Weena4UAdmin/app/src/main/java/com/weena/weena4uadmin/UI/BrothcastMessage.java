package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.databinding.BrothcastmessageBinding;

public class BrothcastMessage extends AppCompatActivity {

    private BrothcastmessageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.brothcastmessage);

        InitView();
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BrothcastMessage.this);
        });

        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.BroadcastMessage));
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BrothcastMessage.this);
    }
}