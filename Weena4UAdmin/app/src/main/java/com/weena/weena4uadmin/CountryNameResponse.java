package com.blood.bloodbankadmin;

import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountryNameResponse {

    @SerializedName("name")
    private CountryData countryData;

    @SerializedName("flags")
    private FlagsDataModel flagsData;
}
