package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.nex3z.togglebuttongroup.button.CircularToggle;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.DetailsofregisterblooddonorBinding;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;

public class DetailsOFRegisterBloodDonor extends AppCompatActivity {

    private DetailsofregisterblooddonorBinding binding;
    private DonorModel donorModel;
    private LocationViewModel locationViewModel;
    private LocationDataAdapter locationDataAdapter;
    private ProvinceNameDataAdapter provinceNameDataAdapter;
    private DistrictNameDataAdapter districtNameDataAdapter;
    private ViewModel viewModel;
    private String BloodGroup = null;
    private CircularToggle circularToggle;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.detailsofregisterblooddonor);
        donorModel = (DonorModel) getIntent().getSerializableExtra(DataManager.Data);
        binding.setBloodDonor(donorModel);
        progressDialog = new ProgressDialog();

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        SetToolbar();
        InitView();
        UpdateData();
        SetLocation();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        BloodGroupSet();
    }

    private void BloodGroupSet(){
        if(donorModel.getBloodGroup().equals(DataManager.APlus)){
            binding.APlus.setChecked(true);
            BloodGroup = DataManager.APlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.AMinus)){
            binding.AMinus.setChecked(true);
            BloodGroup  = DataManager.AMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BPlus)){
            binding.BPlus.setChecked(true);
            BloodGroup = DataManager.BPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BMinus)){
            binding.BMinus.setChecked(true);
            BloodGroup = DataManager.BMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABPlus)){
            binding.ABPlus.setChecked(true);
            BloodGroup = DataManager.ABPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABMinus)){
            binding.ABMinus.setChecked(true);
            BloodGroup = DataManager.ABMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OPlus)){
            binding.OPlus.setChecked(true);
            BloodGroup = DataManager.OPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OMinus)){
            binding.OMinus.setChecked(true);
            BloodGroup = DataManager.OMinus;
        }
    }


    private void SetLocation() {

        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(DetailsOFRegisterBloodDonor.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });
    }

    private void UpdateData() {

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });
        binding.UploadBtn.setOnClickListener(view -> {
            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameInput.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();
            var PhoneNuber = binding.MainNumberInput.getText().toString().trim();
            var RoshanNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();


            if (Integer.valueOf(Age) < 17) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Weight must be at least 55");
            }else {
                progressDialog.ProgressDialog(DetailsOFRegisterBloodDonor.this);
                viewModel.UpdateRegisterAsDonor(donorModel.getSenderUID(), GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName, PhoneNuber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress)
                        .observe(this, aBoolean -> {
                            if(aBoolean){
                                progressDialog.CancelProgressDialog();
                                finish();
                                Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
                            }else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
            }

        });
    }

    private void InitView() {
        binding.EditBtn.setOnClickListener(view -> {
            var intent = new Intent(this, EditregisterDonationDate.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });

        binding.DonationHistoryBtn.setOnClickListener(view -> {
            HandleActivity.GotoLastDonationHistory(this, donorModel.getSenderUID());
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });
    }

    private void SetToolbar() {
        binding.Toolbar.ToolbarTitle.setText("Edit/Delete");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
    }


    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    //todo get all location name
}