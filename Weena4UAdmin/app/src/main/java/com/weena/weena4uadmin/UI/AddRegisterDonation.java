package com.blood.bloodbankadmin.UI;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.DatePicker;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.firebase.messaging.ImageDownload;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Data.SharePref;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.Utils.Permission;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.DatePickerDialogFragment;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.AddregisterdonationBinding;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;

public class AddRegisterDonation extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private AddregisterdonationBinding binding;
    private LocationViewModel locationViewModel;
    private ViewModel viewModel;
    private LocationDataAdapter locationDataAdapter;
    private ProvinceNameDataAdapter provinceNameDataAdapter;
    private DistrictNameDataAdapter districtNameDataAdapter;
    private ProgressDialog progressDialog;
    private SharePref sharePref ;
    private ActivityResultLauncher<Intent> launcher;
    private String ImageDownloadUri = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharePref = new SharePref(getApplicationContext());
        binding = DataBindingUtil.setContentView(this, R.layout.addregisterdonation);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();
        progressDialog = new ProgressDialog();

        InitView();
        SetLocation();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        UploadData();
        GetImageFromInternalStorage();
    }

    private void GetImageFromInternalStorage(){
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            progressDialog.ProgressDialog(AddRegisterDonation.this);
            if(result.getResultCode() == RESULT_OK){
                viewModel.UploadRegisterDonationImage(result.getData().getData()).observe(this, s -> {
                    ImageDownloadUri = s;
                    progressDialog.CancelProgressDialog();
                });
            }else {
                progressDialog.CancelProgressDialog();
            }
        });
    }

    private void UploadData(){
        binding.UploadBtn.setOnClickListener(view -> {
            var LastDonationDate = binding.LastDonationDateInput.getText().toString().trim();
            var BloodQuantity = binding.QuantityofblooddonatedInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var HospitalName = binding.HospitalNameInput.getText().toString().trim();

            if(LastDonationDate.isEmpty()){
                Toast.SetMessage(getApplicationContext(), "Last donation date is empty");
            }else if(BloodQuantity.isEmpty()){
                Toast.SetMessage(getApplicationContext(), "Blood quantity is empty");
            }else if(Country == ""){
                Toast.SetMessage(getApplicationContext(), "Country name is empty");
            }else if(Province == ""){
                Toast.SetMessage(getApplicationContext(), "Province name is empty");
            }else if(District == ""){
                Toast.SetMessage(getApplicationContext(), "District name is empty");
            }else if(HospitalName == ""){
                Toast.SetMessage(getApplicationContext(), "Hospital name is empty");
            }else {
                progressDialog.ProgressDialog(AddRegisterDonation.this);
                viewModel.AddRegisterDonation(ImageDownloadUri, sharePref.GetData(DataManager.RegisterDonorUID), LastDonationDate, Country, District, HospitalName, Province, BloodQuantity).observe(this, aBoolean -> {
                    if(aBoolean){
                        viewModel.DonorUserLastDonationDateUpdate(sharePref.GetData(DataManager.RegisterDonorUID), LastDonationDate).observe(this, aBoolean1 -> {
                            if(aBoolean1){
                                finish();
                                Animatoo.animateSlideRight(AddRegisterDonation.this);
                                progressDialog.CancelProgressDialog();
                            }else {
                                progressDialog.CancelProgressDialog();
                            }
                        });

                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void SetLocation() {

        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(AddRegisterDonation.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(AddRegisterDonation.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(AddRegisterDonation.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(AddRegisterDonation.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(AddRegisterDonation.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(AddRegisterDonation.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(AddRegisterDonation.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(AddRegisterDonation.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(AddRegisterDonation.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText("Add Donation");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddRegisterDonation.this);
        });

        binding.LastDonationDateInput.setKeyListener(null);
        binding.LastDonationDateInput.setOnClickListener(view -> {
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });

        binding.PickImage.setOnClickListener(view -> {
            if(Permission.ReadExternalMemoryPermission(this, 511)){
                var intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                launcher.launch(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddRegisterDonation.this);
    }



    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(AddRegisterDonation.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(AddRegisterDonation.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(AddRegisterDonation.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(AddRegisterDonation.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(AddRegisterDonation.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(AddRegisterDonation.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        var month = i1+1;
        binding.LastDonationDateInput.setText(i + "-" + month + "-" + i2);
    }
    //todo get all location name
}