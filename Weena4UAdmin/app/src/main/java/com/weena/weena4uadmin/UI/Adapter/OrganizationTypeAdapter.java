package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.OrganizationTypeModel;
import com.blood.bloodbankadmin.UI.ViewHolder.LocationViewHolder;
import com.blood.bloodbankadmin.databinding.LocationitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class OrganizationTypeAdapter extends RecyclerView.Adapter<LocationViewHolder> {
    @Inject
    public OrganizationTypeAdapter(){
    }

    @Setter
    @Getter
    private List<OrganizationTypeModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LocationitemBinding.inflate(l, parent, false);
        return new LocationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.binding.LocationName.setText(list.get(position).getCategory());
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getCategory());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String Name);
    }
    public void OnClickListner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
