package com.blood.bloodbankadmin.Utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.blood.bloodbankadmin.CountryNameModel;
import com.blood.bloodbankadmin.DistrictNameModel;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.OrganizationTypeModel;
import com.blood.bloodbankadmin.ProvinceNameModel;

@Database(entities = {CountryNameModel.class, ProvinceNameModel.class, DistrictNameModel.class, OrganizationTypeModel.class, LostAndFoundTypeModel.class}, version = 3)
public abstract class CountryNameDatabase extends RoomDatabase {

    public abstract LocationDao countryDao();
    public static volatile CountryNameDatabase getInstance;

    public static CountryNameDatabase GetContactDatabase(Context context) {
        if (getInstance == null) {
            synchronized (CountryNameDatabase.class) {
                if (getInstance == null) {
                    getInstance = Room.databaseBuilder(context, CountryNameDatabase.class, "contact")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return getInstance;
    }

}
