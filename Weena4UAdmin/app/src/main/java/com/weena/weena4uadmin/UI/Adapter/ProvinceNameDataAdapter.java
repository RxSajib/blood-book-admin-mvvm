package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.ProvinceNameModel;
import com.blood.bloodbankadmin.UI.ViewHolder.LocationViewHolder;
import com.blood.bloodbankadmin.databinding.LocationitemBinding;
import java.util.List;
import javax.inject.Inject;
import lombok.Getter;
import lombok.Setter;

public class ProvinceNameDataAdapter extends RecyclerView.Adapter<LocationViewHolder> {

    @Inject
    public ProvinceNameDataAdapter(){

    }

    @Setter
    @Getter
    private List<ProvinceNameModel> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        var l = LayoutInflater.from(parent.getContext());
        var v  = LocationitemBinding.inflate(l, parent, false);
        return new LocationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.binding.LocationName.setText(list.get(position).getProvance());
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getProvance());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String ProvinceName);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }

}
