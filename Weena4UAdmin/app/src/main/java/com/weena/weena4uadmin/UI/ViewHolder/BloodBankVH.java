package com.blood.bloodbankadmin.UI.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.HospitalitemBinding;

public class BloodBankVH extends RecyclerView.ViewHolder {

    public HospitalitemBinding binding;

    public BloodBankVH(@NonNull HospitalitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
