package com.blood.bloodbankadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Network.AndroidViewModel.CountryViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.databinding.HomeBinding;

import java.text.DecimalFormat;

public class Home extends AppCompatActivity {

    private HomeBinding binding;
    private ViewModel viewModel;
    private CountryViewModel countryViewModel;
    private DecimalFormat precision =  new DecimalFormat("00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.home);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        countryViewModel = new ViewModelProvider(this).get(CountryViewModel.class);


        InitView();
        GetCountData();
    }

    private void GetCountData() {

        viewModel.BloodRequestSize().observe(this, integer -> {
            if(integer != null){
                if (integer <= 9) {
                    binding.BloodRequestCount.setText(String.valueOf(precision.format(integer)));
                } else {
                    binding.BloodRequestCount.setText(String.valueOf(integer));
                }
            }
        });

        viewModel.GetHospitalNameList().observe(this, bloodBankModels -> {
            if (bloodBankModels != null) {

                binding.BloodBankCount.setText(String.valueOf(precision.format(bloodBankModels.size())));
            }
        });

        viewModel.UserSize().observe(this, integer -> {
            if (integer != null) {
                if (integer <= 9) {
                    binding.UserCount.setText(String.valueOf(precision.format(integer)));
                } else {
                    binding.UserCount.setText(String.valueOf(integer));
                }
            }
        });

        viewModel.BloodDonorSize().observe(this, integer -> {
            if(integer != null) {
                if (integer <= 9) {
                    binding.BloodDonorCount.setText(String.valueOf(precision.format(integer)));
                } else {
                    binding.BloodDonorCount.setText(String.valueOf(integer));
                }
            }
        });

        countryViewModel.GetCountry().observe(this, countryModels -> {
            if(countryModels != null){
                if(countryModels.size() <= 9){
                    binding.LocationCount.setText(String.valueOf(countryModels.size()));
                }else {
                    binding.LocationCount.setText(String.valueOf(countryModels.size()));
                }
            }
        });

        viewModel.GetTelephonyDirectorySize().observe(this, integer -> {
            if(integer != null){
                if(integer <= 9){
                    binding.TelephonyManagerCount.setText(String.valueOf(integer));
                }else {
                    binding.TelephonyManagerCount.setText(String.valueOf(integer));
                }
            }
        });

        viewModel.LostFoundSize().observe(this, integer -> {
            if(integer != null){
                if(integer <= 9){
                    binding.LostAndFoundCount.setText(String.valueOf(precision.format(integer)));
                }else {
                    binding.LostAndFoundCount.setText(String.valueOf(integer));
                }
            }
        });

        viewModel.SizeOFAdManager().observe(this, integer -> {
            if(integer != null){
                if(integer <= 9){
                    binding.AdCount.setText(String.valueOf(precision.format(integer)));
                }else {
                    binding.AdCount.setText(String.valueOf(integer));
                }
            }
        });
    }

    private void InitView() {
        binding.TelephoneDirectory.setOnClickListener(view -> {
            HandleActivity.GotoTeliphonyManager(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });
        binding.BloodDonor.setOnClickListener(view -> {
            HandleActivity.GotoBloodDonor(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.BloodRequest.setOnClickListener(view -> {
            HandleActivity.GotoBloodRequest(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.BloodBank.setOnClickListener(view -> {
            HandleActivity.GotoBloodBank(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.BroadcastMessage.setOnClickListener(view -> {
            HandleActivity.GotoBroadcastMessage(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.AddSubAdmin.setOnClickListener(view -> {
            HandleActivity.GotoAddSubAdmin(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.Users.setOnClickListener(view -> {
            HandleActivity.GotoAppUser(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.AddLocation.setOnClickListener(view -> {
            HandleActivity.GotoAddCountry(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.LostAndFound.setOnClickListener(view -> {
            HandleActivity.GotoLostAndFound(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });

        binding.AdManager.setOnClickListener(view -> {
            HandleActivity.GotoAdManager(Home.this);
            Animatoo.animateSlideLeft(Home.this);
        });
    }

    @Override
    public void onBackPressed() {
        viewModel.UserExists().observe(this, aBoolean -> {
            if (!aBoolean) {
                HandleActivity.GotoSignIn(Home.this);
                Animatoo.animateSlideLeft(Home.this);
                finish();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        viewModel.UserExists().observe(this, aBoolean -> {
            if (!aBoolean) {
                HandleActivity.GotoSignIn(Home.this);
                Animatoo.animateSlideLeft(Home.this);
                finish();
            }
        });
    }






}