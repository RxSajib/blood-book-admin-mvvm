package com.blood.bloodbankadmin.Network;

import android.app.Application;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;
import java.util.HashMap;

public class RegisterDonationImageUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference RegisterDonationRef;
    private FirebaseAuth Mauth;
    private StorageReference RegisterDonationStore;


    public RegisterDonationImageUpdate(Application application){
        this.application = application;
        RegisterDonationRef = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
        RegisterDonationStore = FirebaseStorage.getInstance().getReference().child(DataManager.RegisterDonation);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> RegisterDonationImageUpdate(String UID, Uri Imageuri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            var timestamp = System.currentTimeMillis();
            var storageReference = RegisterDonationStore.child(Imageuri.getLastPathSegment()+String.valueOf(timestamp));
            storageReference.putFile(Imageuri).addOnSuccessListener(taskSnapshot -> {
                if(taskSnapshot.getMetadata() != null){
                    if(taskSnapshot.getStorage() != null){
                        var task = taskSnapshot.getStorage().getDownloadUrl();
                        task.addOnSuccessListener(uri -> {
                            var map = new HashMap<String, Object>();
                            map.put(DataManager.RegisterDonationBloodImage, uri.toString());

                            RegisterDonationRef.document(UID)
                                    .update(map).addOnCompleteListener(task1 -> {
                                        if(task1.isSuccessful()){
                                            data.setValue(true);
                                        }else {
                                            Toast.SetMessage(application, task1.getException().getMessage());
                                            data.setValue(false);
                                        }
                                    }).addOnFailureListener(e -> {
                                        Toast.SetMessage(application, e.getMessage());
                                        data.setValue(false);
                                    });
                        }).addOnFailureListener(e -> {
                            Toast.SetMessage(application, e.getMessage());
                            data.setValue(false);
                        });
                    }
                }
            }).addOnFailureListener(e -> {
                Toast.SetMessage(application, e.getMessage());
                data.setValue(false);
            });

        }
        return data;
    }
}
