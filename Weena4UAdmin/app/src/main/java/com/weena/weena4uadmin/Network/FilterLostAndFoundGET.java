package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.LostAndFoundModel;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.List;

public class FilterLostAndFoundGET {

    private Application application;
    private MutableLiveData<List<LostAndFoundModel>> data;
    private CollectionReference LostAndFoundRef;
    private FirebaseAuth Mauth;

    public FilterLostAndFoundGET(Application application){
        this.application = application;
        LostAndFoundRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFound);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<LostAndFoundModel>> FilterLostAndFound(String country, String province, String district, String orgType){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = LostAndFoundRef.whereEqualTo(DataManager.Country, country).whereEqualTo(DataManager.Province, province)
                    .whereEqualTo(DataManager.District, district).whereEqualTo(DataManager.LostAndFound, orgType);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(LostAndFoundModel.class));
                            Toast.SetMessage(application, "ok");
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
