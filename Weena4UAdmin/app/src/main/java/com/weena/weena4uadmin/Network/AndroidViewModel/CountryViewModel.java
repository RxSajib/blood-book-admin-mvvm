package com.blood.bloodbankadmin.Network.AndroidViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.blood.bloodbankadmin.CountryModel;
import com.blood.bloodbankadmin.Utils.CountryDao;
import com.blood.bloodbankadmin.Utils.CountryDatabase;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CountryViewModel extends AndroidViewModel {

    private CountryDatabase countryDatabase;
    private CountryDao countryDao;
    private Executor executor;

    public CountryViewModel(@NonNull Application application) {
        super(application);

        countryDatabase = CountryDatabase.GetContactDatabase(application);
        countryDao = countryDatabase.countryDao();
        executor = Executors.newSingleThreadExecutor();
    }

    public void AddCountry(CountryModel countryModel){
        executor.execute(() -> countryDao.AddCountry(countryModel));
    }

    public LiveData<List<CountryModel>> GetCountry(){
        return countryDao.getCountry();
    }

    public void DeleteCountry(String Country){
        executor.execute(() -> {
            countryDao.RemoveCoutryData(Country);
        });
    }
}
