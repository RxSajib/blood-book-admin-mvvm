package com.blood.bloodbankadmin.Utils;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.blood.bloodbankadmin.CountryModel;

@Database(entities = {CountryModel.class}, version = 1)
public abstract class CountryDatabase extends RoomDatabase {

    public abstract CountryDao countryDao();
    public static volatile CountryDatabase getInstance;

    public static CountryDatabase GetContactDatabase(Context context) {
        if (getInstance == null) {
            synchronized (CountryNameDatabase.class) {
                if (getInstance == null) {
                    getInstance = Room.databaseBuilder(context, CountryDatabase.class, "country")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return getInstance;
    }
}
