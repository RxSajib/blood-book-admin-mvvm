package com.blood.bloodbankadmin.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.SignupaccountBinding;

import javax.inject.Inject;

public class SignUpAccount extends AppCompatActivity {

    private SignupaccountBinding binding;
    ProgressDialog progressDialog;
    private ViewModel viewModel;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private FirebaseAuth Mauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.signupaccount);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
       /* var component = DaggerComponenet.create();
        component.InjectSignUp(this);*/
        Mauth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog();

        InitView();
        SignUpAccount();
        creating_request();
    }


    private void SignUpAccount() {
        var Email = binding.EmailInput.getText().toString().trim();
        var Password = binding.PasswordInput.getText().toString().trim();
        var ConfirmPassword = binding.RePasswordInput.getText().toString().trim();

        binding.EmailMessage.setVisibility(View.GONE);
        binding.PasswordMessage.setVisibility(View.GONE);
        binding.RePasswordMessage.setVisibility(View.GONE);

        if (Email.isEmpty()) {
            binding.EmailMessage.setVisibility(View.VISIBLE);
        } else if (Password.isEmpty()) {
            binding.PasswordMessage.setVisibility(View.VISIBLE);
            binding.setSetPasswordErrorMessage(getResources().getString(R.string.PasswordRequire));
        } else if (ConfirmPassword.isEmpty()) {
            binding.RePasswordMessage.setVisibility(View.VISIBLE);
            binding.setSetPasswordErrorMessage(getResources().getString(R.string.PasswordRequire));
        } else if (Password.length() < 7) {
            binding.PasswordMessage.setVisibility(View.VISIBLE);
            binding.setSetPasswordErrorMessage(getResources().getString(R.string.PasswordLengthValidation));
        } else if (!Password.equals(ConfirmPassword)) {
            binding.PasswordMessage.setVisibility(View.VISIBLE);
            binding.RePasswordMessage.setVisibility(View.VISIBLE);
            binding.setSetPasswordErrorMessage(getResources().getString(R.string.PasswordNotMatchValidation));
        } else {
            progressDialog.ProgressDialog(SignUpAccount.this);
            viewModel.EmailPasswordSignUpAccount(Email, Password).observe(this, aBoolean -> {
                if (aBoolean) {
                    progressDialog.CancelProgressDialog();
                    HandleActivity.GotoHome(SignUpAccount.this);
                    finish();
                    Animatoo.animateSlideLeft(this);
                } else {
                    progressDialog.CancelProgressDialog();
                }
            });
        }
    }

    private void InitView() {
        binding.Login.setOnClickListener(view -> {
            HandleActivity.GotoSignIn(SignUpAccount.this);
            Animatoo.animateSlideRight(SignUpAccount.this);
        });

        binding.GoogleSignIn.setOnClickListener(view -> {
            signIn();
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SignUpAccount.this);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void creating_request() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(SignUpAccount.this, gso);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            progressDialog.ProgressDialog(SignUpAccount.this);
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Toast.SetMessage(SignUpAccount.this, e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {

        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        Mauth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String Email = task.getResult().getUser().getEmail();
                    String Name = task.getResult().getUser().getDisplayName();
                    String PhotoUri = task.getResult().getUser().getPhotoUrl().toString();
                    SetUpProfileData(Email, Name, PhotoUri);
                } else {

                    Toast.SetMessage(SignUpAccount.this, task.getException().getMessage());
                }
            }
        });
    }

    private void SetUpProfileData(String Email, String Name, String PhotoUri) {
        viewModel.CreateProfile(Email, Name, PhotoUri).observe(this, aBoolean -> {
            if (aBoolean) {
                progressDialog.CancelProgressDialog();
                HandleActivity.GotoHome(SignUpAccount.this);
                Animatoo.animateSlideLeft(SignUpAccount.this);
                finish();
            }
        });
    }
}