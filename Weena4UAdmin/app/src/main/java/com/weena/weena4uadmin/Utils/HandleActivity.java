package com.blood.bloodbankadmin.Utils;

import android.content.Context;
import android.content.Intent;

import com.blood.bloodbankadmin.CountryModel;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.OrganizationModel;
import com.blood.bloodbankadmin.RegisterDonationModel;
import com.blood.bloodbankadmin.UI.AdManager;
import com.blood.bloodbankadmin.UI.AddCountry;
import com.blood.bloodbankadmin.UI.AddCountryLocation;
import com.blood.bloodbankadmin.UI.AddHospitalData;
import com.blood.bloodbankadmin.UI.AddLostAndFound;
import com.blood.bloodbankadmin.UI.AddRegisterDonation;
import com.blood.bloodbankadmin.UI.AppUsers;
import com.blood.bloodbankadmin.UI.BloodBank;
import com.blood.bloodbankadmin.UI.BloodDonationHistoryDetails;
import com.blood.bloodbankadmin.UI.BloodDonors;
import com.blood.bloodbankadmin.Home;
import com.blood.bloodbankadmin.UI.AddSubAdmin;
import com.blood.bloodbankadmin.UI.BloodRequest;
import com.blood.bloodbankadmin.UI.BrothcastMessage;
import com.blood.bloodbankadmin.UI.DetailsOFRegisterBloodDonor;
import com.blood.bloodbankadmin.UI.EditregisterDonationDate;
import com.blood.bloodbankadmin.UI.LastDonationHistory;
import com.blood.bloodbankadmin.UI.LostAndFound;
import com.blood.bloodbankadmin.UI.RegisterAsFriend;
import com.blood.bloodbankadmin.UI.SelectCountry;
import com.blood.bloodbankadmin.UI.SignInAccount;
import com.blood.bloodbankadmin.UI.SignUpAccount;
import com.blood.bloodbankadmin.UI.TeliphonyManager;
import com.blood.bloodbankadmin.UI.UpdateTeliphonyManager;

import org.checkerframework.checker.guieffect.qual.UI;

public class HandleActivity {

    public static void GotoHome(Context context) {
        var intent = new Intent(context, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddCountry(Context context) {
        var intent = new Intent(context, AddCountry.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void BloodDonationHistoryDetails(Context context, RegisterDonationModel registerDonationModel){
        var intent = new Intent(context, BloodDonationHistoryDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, registerDonationModel);
        context.startActivity(intent);
    }

    public static void AddRegisterDonation(Context context){
        var intent = new Intent(context, AddRegisterDonation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoLostAndFound(Context context) {
        var intent = new Intent(context, LostAndFound.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddLostAndFoundAds(Context context) {
        var intent = new Intent(context, AddLostAndFound.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoSelectCountry(Context context) {
        var intent = new Intent(context, SelectCountry.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddCountryLocation(Context context, CountryModel countryModel) {
        var intent = new Intent(context, AddCountryLocation.class);
        intent.putExtra(DataManager.Data, countryModel);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoUpdateTelephonyManager(Context context, OrganizationModel organizationModel) {
        var intent = new Intent(context, UpdateTeliphonyManager.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, organizationModel);
        context.startActivity(intent);
    }

    public static void GotoSignIn(Context context) {
        var intent = new Intent(context, SignInAccount.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAppUser(Context context) {
        var intent = new Intent(context, AppUsers.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoTeliphonyManager(Context context) {
        var intent = new Intent(context, TeliphonyManager.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddHospitalData(Context context) {
        var intent = new Intent(context, AddHospitalData.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoSignUp(Context context) {
        var intent = new Intent(context, SignUpAccount.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodDonor(Context context) {
        var intent = new Intent(context, BloodDonors.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodRequest(Context context) {
        var intent = new Intent(context, BloodRequest.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBloodBank(Context context) {
        var intent = new Intent(context, BloodBank.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoBroadcastMessage(Context context) {
        var intent = new Intent(context, BrothcastMessage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAddSubAdmin(Context context) {
        var intent = new Intent(context, AddSubAdmin.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoAdManager(Context context) {
        var intent = new Intent(context, AdManager.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void GotoLastDonationHistory(Context context, String UID){
        var intent = new Intent(context, LastDonationHistory.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.UID, UID);
        context.startActivity(intent);
    }

    public static void GotoDetailsOFRegisterBloodDonor(Context context, DonorModel model) {
        var intent = new Intent(context, DetailsOFRegisterBloodDonor.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, model);
        context.startActivity(intent);
    }

    public static void GotoEditRegisterDonationDate(Context context){
        var intent = new Intent(context, EditregisterDonationDate.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }


    public static void GotoRegisterAsFriend(Context context, DonorModel model) {
        var intent = new Intent(context, RegisterAsFriend.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(DataManager.Data, model);
        context.startActivity(intent);
    }

}
