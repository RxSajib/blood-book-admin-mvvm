package com.blood.bloodbankadmin.Network;

import android.app.Application;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class RegisterDonationImagePOST {

    private Application application;
    private FirebaseAuth Mauth;
    private StorageReference RegisterDonationRefStorage;
    private MutableLiveData<String> data;

    public RegisterDonationImagePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        RegisterDonationRefStorage = FirebaseStorage.getInstance().getReference().child(DataManager.RegisterDonation);
    }

    public LiveData<String> UploadRegisterDonationImage(Uri ImageUri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var Timestamp = System.currentTimeMillis();
            var s = RegisterDonationRefStorage.child(ImageUri.getLastPathSegment()+String.valueOf(Timestamp));
            s.putFile(ImageUri).addOnSuccessListener(taskSnapshot -> {
                if(taskSnapshot.getMetadata() != null){
                    if(taskSnapshot.getStorage() != null){
                        var newtask = taskSnapshot.getStorage().getDownloadUrl();
                        newtask.addOnSuccessListener(uri -> {
                            data.setValue(uri.toString());
                        }).addOnFailureListener(e -> {
                           data.setValue(null);
                           Toast.SetMessage(application, e.getMessage());
                        });
                    }
                }
            }).addOnFailureListener(e -> {
               data.setValue(null);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }

}
