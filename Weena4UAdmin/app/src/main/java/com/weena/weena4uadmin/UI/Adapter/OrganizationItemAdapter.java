package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.OrganizationTypeModel;
import com.blood.bloodbankadmin.UI.ViewHolder.OrganizationItemViewHolder;
import com.blood.bloodbankadmin.databinding.OrganizationitemBinding;
import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class OrganizationItemAdapter extends RecyclerView.Adapter<OrganizationItemViewHolder> {
    @Setter @Getter
    private List<OrganizationTypeModel> list;
    private OnRemove OnRemove;
    private OnSelect OnSelect;

    @Inject
    public OrganizationItemAdapter(){
    }

    @NonNull
    @Override
    public OrganizationItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = OrganizationitemBinding.inflate(l, parent, false);
        return new OrganizationItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizationItemViewHolder holder, int position) {
        holder.binding.setOrganization(list.get(position));

        holder.binding.DeleteBtn.setOnClickListener(view -> {
            OnRemove.OnClick(list.get(position).getDocumentKey());
        });

        holder.itemView.setOnClickListener(view -> {
            OnSelect.Select(list.get(position).getCategory());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnRemove{
        void OnClick(long DocumentKey);
    }
    public void OnRemoveLisiner(OnRemove OnRemove){
        this.OnRemove = OnRemove;
    }

    public interface OnSelect{
        void Select(String CategoryType);
    }
    public void OnSelectLisiner(OnSelect OnSelect){
        this.OnSelect = OnSelect;
    }
}
