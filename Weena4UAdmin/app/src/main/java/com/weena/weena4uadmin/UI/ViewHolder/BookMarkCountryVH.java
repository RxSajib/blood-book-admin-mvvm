package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.CountrybookmarkitemBinding;

public class BookMarkCountryVH extends RecyclerView.ViewHolder {

    public CountrybookmarkitemBinding binding;

    public BookMarkCountryVH(@NonNull CountrybookmarkitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
