package com.blood.bloodbankadmin.UI.BloodDonor;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.BloodDonorPagerAdapter;
import com.blood.bloodbankadmin.UI.Adapter.DonorItemAdapter;
import com.blood.bloodbankadmin.UI.Adapter.DonorUserAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.databinding.RegisterdonorBinding;

import java.util.List;

public class RegisterDonor extends Fragment {

    private RegisterdonorBinding binding;
    private ViewModel viewModel;
    private DonorItemAdapter donorItemAdapter;

    public RegisterDonor() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.registerdonor, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        donorItemAdapter = new DonorItemAdapter();

        InitView();
        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(donorItemAdapter);

        viewModel.RegisterAsDonorGET().observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();
        });

        donorItemAdapter.OnClickState(model -> {
            HandleActivity.GotoDetailsOFRegisterBloodDonor(getActivity(), model);
            Animatoo.animateSlideLeft(getActivity());
        });
    }


}