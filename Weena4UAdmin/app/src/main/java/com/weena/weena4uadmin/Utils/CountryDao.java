package com.blood.bloodbankadmin.Utils;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.blood.bloodbankadmin.CountryModel;
import java.util.List;

@Dao
public interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void AddCountry(CountryModel countryModel);


    @Query("SELECT * FROM CountryModel ORDER BY CountryName ASC")
    public LiveData<List<CountryModel>> getCountry();

    @Query("DELETE FROM CountryModel WHERE CountryName = :IDOFCountryName")
    public void RemoveCoutryData(String IDOFCountryName);

}
