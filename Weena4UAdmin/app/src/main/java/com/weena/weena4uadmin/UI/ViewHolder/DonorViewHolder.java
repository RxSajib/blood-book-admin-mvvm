package com.blood.bloodbankadmin.UI.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.DonoruseritemBinding;

public class DonorViewHolder extends RecyclerView.ViewHolder {

    public DonoruseritemBinding binding;

    public DonorViewHolder(@NonNull DonoruseritemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
