package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.databinding.RegisterasfriendBinding;

public class RegisterAsFriend extends AppCompatActivity {

    private RegisterasfriendBinding binding;
    private DonorModel donorModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.registerasfriend);
        donorModel = (DonorModel) getIntent().getSerializableExtra(DataManager.Data);
        binding.setDonor(donorModel);

        SetToolbar();
    }

    private void SetToolbar(){
        binding.Toolbar.ToolbarTitle.setText("Edit/Delete");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(RegisterAsFriend.this);
        });


    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(RegisterAsFriend.this);
    }
}