package com.blood.bloodbankadmin.UI.BloodDonor;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DonorItemAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.databinding.RegisterfriendBinding;

import java.util.List;

public class RegisterFriend extends Fragment {

    private RegisterfriendBinding binding;
    private DonorItemAdapter donorItemAdapter;
    private ViewModel viewModel;

    public RegisterFriend() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.registerfriend, container, false);
        donorItemAdapter = new DonorItemAdapter();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        InitView();
        return binding.getRoot();
    }

    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(donorItemAdapter);
        viewModel.RegisterAsFriendGET().observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();

        });

        donorItemAdapter.OnClickState(model -> {
            HandleActivity.GotoRegisterAsFriend(getActivity(), model);
            Animatoo.animateSlideLeft(getActivity());
        });
    }
}