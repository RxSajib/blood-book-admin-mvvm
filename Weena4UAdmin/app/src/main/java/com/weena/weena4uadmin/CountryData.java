package com.blood.bloodbankadmin;

import androidx.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountryData {


    @SerializedName("common")
    private @NonNull
    String CountryName;

    @SerializedName("official")
    private String CountryDetails;
}
