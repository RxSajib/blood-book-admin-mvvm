package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;

public class TelephoneDirectorySize {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference OrgnizationRef;
    private MutableLiveData<Integer> data;

    public TelephoneDirectorySize(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        OrgnizationRef = FirebaseFirestore.getInstance().collection(DataManager.Organization);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Integer> GetTelephoneDirectorySize(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            OrgnizationRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    data.setValue(value.size());
                }else {
                    data.setValue(null);
                }
            }) ;
        }
        return data;
    }
}
