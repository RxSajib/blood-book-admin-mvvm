package com.blood.bloodbankadmin.Utils;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.blood.bloodbankadmin.CountryNameModel;
import com.blood.bloodbankadmin.DistrictNameModel;
import com.blood.bloodbankadmin.LostAndFoundModel;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.OrganizationTypeModel;
import com.blood.bloodbankadmin.ProvinceNameModel;
import java.util.List;

@Dao
public interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void InsertCountryNameData(List<CountryNameModel> countryNameModel);

    @Query("SELECT * FROM CountryNameDB")
    public LiveData<List<CountryNameModel>> GetCountryName();

    @Query("SELECT * FROM CountryNameDB WHERE CountryName Like '%' || :NameData || '%'")
    public LiveData<List<CountryNameModel>> SearchByCountryName(String NameData);

    @Query("DELETE FROM CountryNameDB")
    public void DeleteAllFROMCOuntryData();



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void InsertProvinces(List<ProvinceNameModel> provinceNameModelList);

    @Query("SelECT * FROM ProvinceNameDB")
    public LiveData<List<ProvinceNameModel>> GetProvinceName();

    @Query("SELECT * FROM ProvinceNameDB WHERE Provance Like '%' || :NameData || '%'")
    public LiveData<List<ProvinceNameModel>> SearchProvince(String NameData);

    @Query("DELETE FROM ProvinceNameDB")
    public void DeleteAllFromProvincesData();



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void InsertDistrict(List<DistrictNameModel> districtNameModelList);

    @Query("SELECT * FROM DistrictNameDB")
    public LiveData<List<DistrictNameModel>> GetDistrictName();

    @Query("SELECT * FROM DistrictNameDB WHERE District Like '%' || :NameData || '%'")
    public LiveData<List<DistrictNameModel>> GetDistrictName(String NameData);

    @Query("DELETE FROM DistrictNameDB")
    public void DeleteDistrict();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void InsertOrganizationType(List<OrganizationTypeModel> organizationTypeModel);

    @Query("SELECT * FROM OrganizationTypeDB")
    public LiveData<List<OrganizationTypeModel>> GetOrganizationType();

    @Query("SELECT * FROM OrganizationTypeDB WHERE Category Like '%' || :OrgName || '%'")
    public LiveData<List<OrganizationTypeModel>> SearchOrganizationType(String OrgName);

    @Query("DELETE FROM OrganizationTypeDB")
    public void DeleteOrganizationType();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void InsertLostAndFound(List<LostAndFoundTypeModel> lostAndFoundModels);

    @Query("SELECT * FROM LostAndFoundTypeDB")
    public LiveData<List<LostAndFoundTypeModel>> GetLostAndType();

    @Query("SELECT * FROM LostAndFoundTypeDB WHERE Category Like '%' || :OrgName || '%'")
    public LiveData<List<LostAndFoundTypeModel>> SearchLostAndFoundType(String OrgName);

    @Query("DELETE FROM OrganizationTypeDB")
    public void DeleteLostAndFoundType();
}
