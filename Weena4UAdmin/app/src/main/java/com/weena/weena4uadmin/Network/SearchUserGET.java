package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.UserModel;

import java.util.List;
import java.util.Locale;

public class SearchUserGET {

    private Application application;
    private MutableLiveData<List<UserModel>> data;
    private CollectionReference UserRef;
    private FirebaseAuth Mauth;

    public SearchUserGET(Application application){
        this.application = application;
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<UserModel>> SearchByUserName(String UserName){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var nametolowercase = UserName.toLowerCase(Locale.ROOT);
            var q = UserRef.orderBy(DataManager.Search).startAt(nametolowercase).endAt(nametolowercase+"\uf8ff");
            q.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(!value.isEmpty()){
                       for(var ds : value.getDocumentChanges()){
                           if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                               data.setValue(value.toObjects(UserModel.class));
                           }
                       }
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }
        return data;
    }
}
