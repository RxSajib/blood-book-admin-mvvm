package com.blood.bloodbankadmin;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(tableName = "CountryNameDB")
public class CountryNameModel {
    @PrimaryKey(autoGenerate = false)
    private @NonNull
    String CountryName;
}
