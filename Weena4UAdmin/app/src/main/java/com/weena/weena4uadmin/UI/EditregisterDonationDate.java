package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.databinding.EditregisterdonationdateBinding;

public class EditregisterDonationDate extends AppCompatActivity {

    private EditregisterdonationdateBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.editregisterdonationdate);



        InitView();
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText("Register Donation Date Edit");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(EditregisterDonationDate.this);
        });


    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(EditregisterDonationDate.this);
    }
}