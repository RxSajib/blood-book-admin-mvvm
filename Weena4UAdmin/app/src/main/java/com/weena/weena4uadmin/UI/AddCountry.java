package com.blood.bloodbankadmin.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Canvas;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.CountryModel;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Network.AndroidViewModel.CountryViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.BookMarkCountryAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.databinding.AddcountryBinding;

import java.util.List;

import javax.inject.Inject;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class AddCountry extends AppCompatActivity {

    private AddcountryBinding binding;
    @Inject
    BookMarkCountryAdapter adapter;
    private CountryViewModel countryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addcountry);

        countryViewModel = new ViewModelProvider(this).get(CountryViewModel.class);
        var component = DaggerComponenet.create();
        component.InjectAddCountry(this);

        InitView();
        GetData();
    }

    private void GetData() {
        binding.CountryRecyclerView.setHasFixedSize(true);
        binding.CountryRecyclerView.setAdapter(adapter);

        adapter.OnClickLisiner(countryModel -> {
            HandleActivity.GotoAddCountryLocation(AddCountry.this, countryModel);
            Animatoo.animateSlideLeft(AddCountry.this);
        });


        var callback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                var c = adapter.getList().get(viewHolder.getAdapterPosition()).getCountryName();
                countryViewModel.DeleteCountry(c);
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addActionIcon(R.drawable.ic_delete_sweep_24)
                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        countryViewModel.GetCountry().observe(this, countryModels -> {
            adapter.setList(countryModels);
            adapter.notifyDataSetChanged();
            new ItemTouchHelper(callback).attachToRecyclerView(binding.CountryRecyclerView);
            if (countryModels.size() > 0) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddCountry.this);
        });

        binding.AddCountry.setOnClickListener(view -> {
            HandleActivity.GotoSelectCountry(AddCountry.this);
            Animatoo.animateSlideLeft(AddCountry.this);
        });

        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.Country));
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddCountry.this);
    }
}