package com.blood.bloodbankadmin;

import android.widget.ImageView;
import androidx.databinding.BindingAdapter;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DonorModel implements Serializable {

    private String AreaName, BloodGroup, Country, District, DonateMonth, EmailAddress, EtisalatNumber, FatherName, GivenName, LastDonationDate;
    private String LastDonationDayCount, MTNNumber, MainPhoneNumber, Age, Province, RoshanNumber, SenderUID, Surname, WhatsappNumber, Width, AWCCNumber;
    private long  Timestamp;
    private String EnableMyNumber;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
