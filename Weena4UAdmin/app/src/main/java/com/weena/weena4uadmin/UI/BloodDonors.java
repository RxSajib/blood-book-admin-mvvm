package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.BloodDonorPagerAdapter;
import com.blood.bloodbankadmin.UI.Adapter.DonorUserAdapter;
import com.blood.bloodbankadmin.UI.BloodDonor.RegisterDonor;
import com.blood.bloodbankadmin.UI.BloodDonor.RegisterFriend;
import com.blood.bloodbankadmin.databinding.BlooddonorsBinding;

public class BloodDonors extends AppCompatActivity {

    private BlooddonorsBinding binding;
    DonorUserAdapter donorUserAdapter;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.blooddonors);


        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        donorUserAdapter = new DonorUserAdapter();

        InitView();
        SetFragment();
    }

    private void SetFragment(){
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new BloodDonorPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new RegisterDonor(), getResources().getString(R.string.RegisterDonor));
        fadapter.AddFragement(new RegisterFriend(), getResources().getString(R.string.RegisterFriend));
        binding.ViewPager.setAdapter(fadapter);
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.BloodDonor));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodDonors.this);
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodDonors.this);
    }
}