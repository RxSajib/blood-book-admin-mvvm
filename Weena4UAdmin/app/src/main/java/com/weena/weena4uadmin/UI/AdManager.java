package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.AdModel;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.AdmanagerBinding;

import javax.inject.Inject;

public class AdManager extends AppCompatActivity {

    private AdmanagerBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.admanager);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = DaggerComponenet.create();
        component.InjectAdManager(this);

        InitView();
        SetAds();
        GetAds();
    }

    private void GetAds(){
        viewModel.GetDashBoardAdData().observe(this, adModel -> {
            binding.setDashBoard(adModel);
        });
    }

    private void SetAds() {
        binding.DashBoardAdsBtn.setOnClickListener(view -> {
            var PhotoUri = binding.PhotoUrl.getText().toString().trim();
            var WebUri = binding.WebUrl.getText().toString().trim();

            if (PhotoUri.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), getResources().getString(R.string.PhotoUrlEmpty));
            } else if (WebUri.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), getResources().getString(R.string.WebUrlEmpty));
            } else {
                progressDialog.ProgressDialog(AdManager.this);
                viewModel.DashBoardAdPost(WebUri, PhotoUri, DataManager.DashBoard).observe(this,
                        aBoolean -> {
                            if (aBoolean) {
                                progressDialog.CancelProgressDialog();
                            } else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
            }
        });

        binding.DeleteDashBoardAdsBtn.setOnClickListener(view -> {
            progressDialog.ProgressDialog(AdManager.this);
            viewModel.DeleteDashBoardAd().observe(this, aBoolean -> {
                if(aBoolean){
                    progressDialog.CancelProgressDialog();
                }
            });
        });
    }

    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AdManager.this);
        });

        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.AdManager));
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AdManager.this);
    }
}