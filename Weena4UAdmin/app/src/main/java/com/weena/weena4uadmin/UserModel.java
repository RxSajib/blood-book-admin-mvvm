package com.blood.bloodbankadmin;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import lombok.Data;

@Data
public class UserModel {

    private String Email, FatherName, FirstName, MiddleName, ProfileImage, Surname, Token, IpAddress;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
