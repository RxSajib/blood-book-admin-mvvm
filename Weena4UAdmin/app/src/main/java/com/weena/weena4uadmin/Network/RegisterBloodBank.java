package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class RegisterBloodBank {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference BloodBankRef;
    private FirebaseAuth Mauth;

    public RegisterBloodBank(Application application) {
        this.application = application;
        BloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.Hospital);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> RegisterBloodBank(String HospitalName, String EmailAddress, String Location, String HospitalIcon) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();

        if (FirebaseUser != null) {
            var Timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.HospitalName, HospitalName);
            map.put(DataManager.Email, EmailAddress);
            map.put(DataManager.Location, Location);
            map.put(DataManager.HospitalLogo, HospitalIcon);
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.UID, FirebaseUser.getUid());

            BloodBankRef.document(FirebaseUser.getUid()).set(map)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            data.setValue(true);
                        } else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        Toast.SetMessage(application, e.getMessage());
                        data.setValue(false);
                    });
        }
        return data;
    }
}
