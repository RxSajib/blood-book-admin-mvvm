package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.LostAndFoundModel;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.FilterLostAndFoundTypeAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LostAndFoundAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Permission;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.FilterOptionDialog;
import com.blood.bloodbankadmin.databinding.FilterdialogBinding;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;
import com.blood.bloodbankadmin.databinding.LostandfoundBinding;

import java.util.List;

import javax.inject.Inject;

public class LostAndFound extends AppCompatActivity {

    private LostandfoundBinding binding;
    @Inject
    LostAndFoundAdapter andFoundAdapter;
    private ViewModel viewModel;
    @Inject
    FilterOptionDialog filterOptionDialog;
    private LocationViewModel locationViewModel;
    @Inject
    ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject
    DistrictNameDataAdapter districtNameDataAdapter;
    @Inject
    LocationDataAdapter locationDataAdapter;
    @Inject
    FilterLostAndFoundTypeAdapter filterLostAndFoundTypeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.lostandfound);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = DaggerComponenet.create();
        component.InjectLostAndFound(this);

        InitView();
        GetData();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        GetLostAndFoundType();
    }

    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(andFoundAdapter);

        viewModel.GetLostAndFound().observe(this, lostAndFoundModels -> {
            andFoundAdapter.setList(lostAndFoundModels);
            andFoundAdapter.notifyDataSetChanged();
            if(lostAndFoundModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView(){
        binding.MyToolbar.ToolbarTitle.setText(getResources().getString(R.string.LostAndFound));
        binding.MyToolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(LostAndFound.this);
        });

        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.GotoAddLostAndFoundAds(LostAndFound.this);
            Animatoo.animateSlideLeft(LostAndFound.this);
        });


        filterOptionDialog.OnclickLisiner(Item -> {
            if (Item == 0) {
                GetData();
            }
            if (Item == 1) {
                var alertdialog = new AlertDialog.Builder(LostAndFound.this);
                FilterdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.filterdialog, null, false);
                alertdialog.setView(v.getRoot());

                var dialog = alertdialog.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                DialogView(dialog, v);
                v.ClearBtn.setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        });

        binding.SearchInput.setOnTouchListener((view, motionEvent) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (binding.SearchInput.getRight() - binding.SearchInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    filterOptionDialog.show(getSupportFragmentManager(), "filter");
                    return true;
                }
            }
            return false;
        });


        andFoundAdapter.OnCallLisiner(organizationModel -> {
            if (Permission.PermissionCall(LostAndFound.this, 150)) {
                var intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + organizationModel.getPhoneNumber()));
                startActivity(intent);
            }
        });


        binding.SearchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
               SearchingLostAndFound();
                return true;
            }
            return false;
        });
    }

    private void SearchingLostAndFound(){
        var SearchInputData = binding.SearchInput.getText().toString().trim();
        if(SearchInputData.isEmpty()){
            Toast.SetMessage(getApplicationContext(), "Empty");
        }else {
            viewModel.SearchLostAndFound(SearchInputData).observe(this, lostAndFoundModels -> {
                andFoundAdapter.setList(lostAndFoundModels);
                andFoundAdapter.notifyDataSetChanged();

                if(lostAndFoundModels != null){
                    binding.Icon.setVisibility(View.GONE);
                    binding.Message.setVisibility(View.GONE);
                }else {
                    binding.Icon.setVisibility(View.VISIBLE);
                    binding.Message.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void DialogView(AlertDialog dialog, FilterdialogBinding v) {

        v.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var mydialog = new android.app.AlertDialog.Builder(LostAndFound.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            mydialog.setView(locationitemBinding.getRoot());

            var alertdialog = mydialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();

            viewModel.GetCountryName().observe(LostAndFound.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
                GetCountryName();

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                v.Country.setText(CountryName);
                v.District.setText(null);
                v.Province.setText(null);
                alertdialog.dismiss();
            });
        });

        v.SelectProvince.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(LostAndFound.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var provincedialog = new android.app.AlertDialog.Builder(LostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                provincedialog.setView(locationitemBinding.getRoot());

                var alertdialog = provincedialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(LostAndFound.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    v.Province.setText(ProvinceName);
                    v.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        v.DistrictInput.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            var ProvincesName = v.Province.getText().toString().trim();

            if (CountryName == "") {
                Toast.SetMessage(LostAndFound.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(LostAndFound.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var districtdialog = new android.app.AlertDialog.Builder(LostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                districtdialog.setView(locationitemBinding.getRoot());

                var alertdialog = districtdialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(LostAndFound.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    v.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDistrictName(Data);
                        }
                    }
                });
            }
        });

        v.SelectOrgTypeInput.setOnClickListener(view -> {
            var districtdialog = new android.app.AlertDialog.Builder(LostAndFound.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            districtdialog.setView(locationitemBinding.getRoot());

            var alertdialog = districtdialog.create();
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(filterLostAndFoundTypeAdapter);
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertdialog.show();

            viewModel.GetLostAndFoundType().observe(this, lostAndFoundTypeModels -> {
                locationViewModel.InsertLostAndFound(lostAndFoundTypeModels);
            });

            filterLostAndFoundTypeAdapter.OnClickState(CategoryName -> {
                v.OrgType.setText(CategoryName);
                alertdialog.dismiss();
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var s = editable.toString();
                    if (s.isEmpty()) {
                        GetLostAndFoundType();
                    } else {
                        SearchLostAndFoundType(s);
                    }
                }
            });
        });


        v.ApplyBtn.setOnClickListener(view -> {
            var Country = v.Country.getText().toString().trim();
            var Province = v.Province.getText().toString().trim();
            var District = v.District.getText().toString().trim();
            var OrgType = v.OrgType.getText().toString().trim();

            if (Country == "") {
                Toast.SetMessage(getApplicationContext(), "Country empty");
            } else if (Province == "") {
                Toast.SetMessage(getApplicationContext(), "Province empty");
            } else if (District == "") {
                Toast.SetMessage(getApplicationContext(), "District empty");
            } else if (OrgType == "") {
                Toast.SetMessage(getApplicationContext(), "Organization Type empty");
            } else {
                FilterLostAndFound(Country, Province, District, OrgType);
                dialog.dismiss();
            }
        });
    }

    private void FilterLostAndFound(String country, String province, String district, String orgType) {
        viewModel.FilterLostAndFound(country, province, district, orgType).observe(this, lostAndFoundModels -> {
            andFoundAdapter.setList(lostAndFoundModels);
            andFoundAdapter.notifyDataSetChanged();

            if(lostAndFoundModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(LostAndFound.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }
    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(LostAndFound.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(LostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(LostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(LostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDistrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(LostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetLostAndFoundType(){
        locationViewModel.GetLostAndFoundType().observe(this, list -> {
            filterLostAndFoundTypeAdapter.setList(list);
            filterLostAndFoundTypeAdapter.notifyDataSetChanged();
        });
    }
    private void SearchLostAndFoundType(String NameOFLostAndFound){
        locationViewModel.SearchLostAndFound(NameOFLostAndFound);
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(LostAndFound.this);
    }
}