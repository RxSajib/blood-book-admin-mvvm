package com.blood.bloodbankadmin.Network;
import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;

import java.util.List;

public class RegisterDonorGET {

    private Application application;
    private MutableLiveData<List<DonorModel>> data;
    private CollectionReference DonorUserRef;
    private FirebaseAuth Mauth;

    public RegisterDonorGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<List<DonorModel>> RegisterDonorGET(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = DonorUserRef.whereEqualTo(DataManager.Type, DataManager.TypeRegisterAsDonor);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(DonorModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }

}
