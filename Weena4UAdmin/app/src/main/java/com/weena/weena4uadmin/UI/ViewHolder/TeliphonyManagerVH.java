package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.TeliphonedirectoryitemBinding;

public class TeliphonyManagerVH extends RecyclerView.ViewHolder {

    public TeliphonedirectoryitemBinding binding;

    public TeliphonyManagerVH(@NonNull TeliphonedirectoryitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
