package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.UserModel;

import java.util.List;

public class UserGET {

    private Application application;
    private MutableLiveData<List<UserModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference UserRef;

    public UserGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
    }

    public LiveData<List<UserModel>> GetUser(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            UserRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(UserModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });

        }
        return data;
    }
}
