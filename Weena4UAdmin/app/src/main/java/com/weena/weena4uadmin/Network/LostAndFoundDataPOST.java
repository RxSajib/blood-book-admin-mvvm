package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class LostAndFoundDataPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference LostAndFoundRef;
    private FirebaseAuth Mauth;

    public LostAndFoundDataPOST(Application application) {
        this.application = application;
        LostAndFoundRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFound);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UploadLostAndFound(String PostTitle, String LostAndFound, String TelNumber, String Country, String Province, String District, String FullDetails, String Image) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            var map = new HashMap<String, Object>();

            var Timestamp = System.currentTimeMillis();
            map.put(DataManager.Title, PostTitle);
            map.put(DataManager.LostAndFound, LostAndFound);
            map.put(DataManager.PhoneNumber, TelNumber);
            map.put(DataManager.Country, Country);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.Image, Image);
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.FullDetails, FullDetails);

            LostAndFoundRef.document(String.valueOf(Timestamp))
                    .set(map).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            data.setValue(true);
                        } else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });
        }
        return data;
    }
}
