package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LostAndFoundTypeAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.AddlostandfoundBinding;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;
import com.blood.bloodbankadmin.databinding.OrgnaizationtypedialogBinding;
import com.blood.bloodbankadmin.databinding.OrgtypecategorydialogBinding;

import java.util.List;

import javax.inject.Inject;

public class AddLostAndFound extends AppCompatActivity {

    private AddlostandfoundBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;
    @Inject
    LostAndFoundTypeAdapter lostAndFoundTypeAdapter;
    @Inject
    LocationDataAdapter locationDataAdapter;
    @Inject
    ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject
    DistrictNameDataAdapter districtNameDataAdapter;
    private LocationViewModel locationViewModel;
    private String ImageDownloadUri = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addlostandfound);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);

        var component = DaggerComponenet.create();
        component.InjectAddLostAndFound(this);

        InitView();
        LocationDialog();
        UploadLostAndFound();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        UploadData();

    }

    private void UploadData(){
        binding.UploadButton.setOnClickListener(view -> {
            var Title = binding.PostTitleInput.getText().toString().trim();
            var LostFound = binding.LostAndFound.getText().toString().trim();
            var TelephoneNumbers = binding.TelephoneNumbersInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var FullDetails = binding.FullDetailsInput.getText().toString().trim();


            if(Title.isEmpty()){
                Toast.SetMessage(getApplicationContext(), getResources().getString(R.string.PostTitleEmpty));
            } else if (LostFound == "") {
                Toast.SetMessage(getApplicationContext(), getResources().getString(R.string.LostFoundEmpty));
            }else if(TelephoneNumbers.isEmpty()){
                Toast.SetMessage(AddLostAndFound.this, getResources().getString(R.string.TelephoneNumbersEmpty));
            } else if (Country == "") {
                Toast.SetMessage(AddLostAndFound.this, getResources().getString(R.string.CountryEmpty));
            }else if(Province == ""){
                Toast.SetMessage(AddLostAndFound.this, getResources().getString(R.string.ProvinceEmpty));
            }else if(District == ""){
                Toast.SetMessage(AddLostAndFound.this, getResources().getString(R.string.DistrictEmpty));
            }else if(FullDetails.isEmpty()){
                Toast.SetMessage(AddLostAndFound.this, getResources().getString(R.string.FullDetailsEmpty));
            }else {

                progressDialog.ProgressDialog(AddLostAndFound.this);
                viewModel.LostAndFoundDataPost(Title, LostFound, TelephoneNumbers, Country, Province, District, FullDetails, ImageDownloadUri).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.animateSlideRight(AddLostAndFound.this);
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void LocationDialog() {
        binding.CountryName.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(AddLostAndFound.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);
            alertdialog.show();

            viewModel.GetCountryName().observe(this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });
            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
                binding.District.setText(null);
                binding.Province.setText(null);
            });

        });

        binding.ProvinceStateInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(getApplicationContext(), "Country name empty");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(AddLostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });
                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    binding.Province.setText(ProvinceName);
                    alertdialog.dismiss();
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.SelectDistrictInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName.isEmpty()) {
                Toast.SetMessage(AddLostAndFound.this, "Select Country Name");
            } else if (ProvincesName.isEmpty()) {
                Toast.SetMessage(AddLostAndFound.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(AddLostAndFound.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.show();

                viewModel.GetDistrictName(CountryName, ProvincesName).observe(this, districtNameModels -> {
                    locationViewModel.InsertDistrict(districtNameModels);
                });
                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    binding.District.setText(DictrictName);
                    alertdialog.dismiss();
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });

            }
        });
    }

    private void UploadLostAndFound(){
        binding.UploadButton.setOnClickListener(view -> {

        });
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.PostanAd));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddLostAndFound.this);
        });

        binding.OrgTypeInput.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(AddLostAndFound.this);
            OrgnaizationtypedialogBinding dialogbinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.orgnaizationtypedialog, null, false);
            alertdialog.setView(dialogbinding.getRoot());
            dialogbinding.RecyclerView.setHasFixedSize(true);
            dialogbinding.RecyclerView.setAdapter(lostAndFoundTypeAdapter);

            var dialog = alertdialog.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            viewModel.GetLostAndFoundType().observe(this, lostAndFoundTypeModels -> {
                lostAndFoundTypeAdapter.setList(lostAndFoundTypeModels);
                lostAndFoundTypeAdapter.notifyDataSetChanged();
            });

            lostAndFoundTypeAdapter.OnClickState(Category -> {
                binding.LostAndFound.setText(Category);
                dialog.dismiss();
            });

            lostAndFoundTypeAdapter.OnDeleteState(DocumentID -> {
                progressDialog.ProgressDialog(AddLostAndFound.this);
                viewModel.DeleteLostAndFound(DocumentID).observe(this, aBoolean -> progressDialog.CancelProgressDialog());
            });
        });

        binding.AddOrgTypeBtn.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(AddLostAndFound.this);
            OrgtypecategorydialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.orgtypecategorydialog, null, false);
            alertdialog.setView(v.getRoot());

            var d = alertdialog.create();
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.show();

            v.AddBtn.setOnClickListener(view1 -> {
                var Title = v.OrgTypeInput.getText().toString().trim();
                if (Title.isEmpty()) {
                    Toast.SetMessage(AddLostAndFound.this, "Title require");
                } else {
                    progressDialog.ProgressDialog(AddLostAndFound.this);
                    viewModel.UploadLostAndFoundCategory(Title).observe(this, aBoolean -> {
                        d.dismiss();
                        progressDialog.CancelProgressDialog();
                    });
                    v.OrgTypeInput.setText(null);
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddLostAndFound.this);
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(AddLostAndFound.this, countryNameModels -> {
                locationDataAdapter.setList(countryNameModels);
                locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(AddLostAndFound.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();

        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(AddLostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(AddLostAndFound.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(AddLostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(AddLostAndFound.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
}