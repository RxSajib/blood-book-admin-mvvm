package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.OrganizationModel;

import java.util.List;
import java.util.Locale;

public class SearchTelephonyManager {

    private Application application;
    private MutableLiveData<List<OrganizationModel>> data;
    private CollectionReference TelephonyManagerRef;
    private FirebaseAuth Mauth;

    public SearchTelephonyManager(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        TelephonyManagerRef = FirebaseFirestore.getInstance().collection(DataManager.Organization);
    }

    public LiveData<List<OrganizationModel>> SearchTelephonyManager(String OrgName){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var lowecasehospital = OrgName.toLowerCase(Locale.ROOT);
            var q = TelephonyManagerRef.orderBy(DataManager.Search).startAt(lowecasehospital).endAt(lowecasehospital+"\uf8ff");
            q.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(!value.isEmpty()){
                        for(var ds : value.getDocumentChanges()){
                            if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                                data.setValue(value.toObjects(OrganizationModel.class));
                            }
                        }
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }
        return data;
    }

}
