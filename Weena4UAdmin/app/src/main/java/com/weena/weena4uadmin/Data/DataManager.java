package com.blood.bloodbankadmin.Data;

public class DataManager {

    public static final String TypeRegisterAsFriend = "RegisterAsFriend";
    public static final String TypeRegisterAsDonor = "RegisterAsDonor";
    public static final String Type = "Type";
    public static final String SuperAdmin = "SuperAdmin";
    public static final String Name = "Name";
    public static final String Email = "Email";
    public static final String PhotoUri = "PhotoUri";
    public static final String Token = "Token";
    public static final String RegisterDonation = "RegisterDonation";
    public static final String DonorUser = "DonorUser";
    public static final String BloodGroup = "BloodGroup";
    public static final String Age = "Age";
    public static final String Width = "Width";
    public static final String MainPhoneNumber = "MainPhoneNumber";
    public static final String ProfileImage = "ProfileImage";
    public static final String RegisterDonationBloodImage = "RegisterDonationBloodImage";
    public static final String BloodDonationDate = "BloodDonationDate";
    public static final String Country = "Country";
    public static final String District = "District";
    public static final String Province = "Province";
    public static final String QuantityOfBlood = "QuantityOfBlood";
    public static final String TimetoAM_PMpattern = "hh:mm a";
    public static final String DatePattern = "yyyy-MM-dd";
    public static final String RegisterDonorUID = "RegisterDonorUID";


    public static final String SenderUID = "SenderUID";
    public static final String BloodBank = "BloodBank";
    public static final String HospitalName = "HospitalName";
    public static final String HospitalDetails = "HospitalDetails";
    public static final String HospitalLogo = "HospitalLogo";
    public static final String Timestamp = "Timestamp";
    public static final String DocumentKey = "DocumentKey";
    public static final String Location = "Location";
    public static final String UID = "UID";
    public static final String User = "User";
    public static final String BloodRequest = "BloodRequest";
    public static final String OrganizationLogo = "OrganizationLogo";
    public static final String Organization = "Organization";
    public static final String Search = "Search";

    public static final String GivenName = "GivenName";
    public static final String AreaName = "AreaName";
    public static final String RoshanNumber = "RoshanNumber";
    public static final String EtisalatNumber = "EtisalatNumber";
    public static final String AWCCNumber = "AWCCNumber";
    public static final String MTNNumber = "MTNNumber";
    public static final String WhatsappNumber = "WhatsappNumber";
    public static final String EmailAddress = "EmailAddress";
    public static final String EnableMyNumber = "EnableMyNumber";
    public static final String DonationMonthlyGap = "DonationMonthlyGap";
    public static final String Call = "Call";
    public static final String Message = "Message";
    public static final String Code = "Code";
    public static final String LoginType = "LoginType";
    public static final String LoginAddress = "LoginAddress";
    public static final String Surname = "Surname";
    public static final String FatherName = "FatherName";

    public static final String NameOfOrganization = "NameOfOrganization";
    public static final String OrganizationType = "OrganizationType";
    public static final String Address = "Address";
    public static final String PhoneNumber = "PhoneNumber";
    public static final String Provance = "Provance";
    public static final String CountryName = "CountryName";
    public static final String Category = "Category";
    public static final String Data = "Data";
    public static final String CountryNameAPI = "https://restcountries.com/v3.1/";
    public static final String LostAndFoundType = "LostAndFoundType";
    public static final String LostAndFound = "LostAndFound";

    public static final String Title = "Title";
    public static final String Image = "Image";
    public static final String FullDetails = "FullDetails";
    public static final String Hospital = "Hospital";
    public static final String AdManager = "AdManager";
    public static final String WebUrl = "WebUrl";
    public static final String PhotoUrl = "PhotoUrl";
    public static final String DashBoard = "DashBoard";
    public static final String BloodRequestTimestamp = "BloodRequestTimestamp";

    public static final String APlus = "A+";
    public static final String AMinus = "A-";
    public static final String BPlus = "B+";
    public static final String BMinus = "B-";
    public static final String ABPlus = "AB+";
    public static final String ABMinus = "AB-";
    public static final String OPlus = "O+";
    public static final String OMinus = "O-";

}
