package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.LocationModel;
import com.blood.bloodbankadmin.UI.ViewHolder.CountryLocationVH;
import com.blood.bloodbankadmin.databinding.CountrylocationitemBinding;
import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class CountryLocationAdapter extends RecyclerView.Adapter<CountryLocationVH> {

    @Inject
    public CountryLocationAdapter(){

    }

    @Setter @Getter
    private List<LocationModel> list;

    @NonNull
    @Override
    public CountryLocationVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = CountrylocationitemBinding.inflate(l, parent, false);
        return new CountryLocationVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryLocationVH holder, int position) {
        holder.binding.setLocation(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
}
