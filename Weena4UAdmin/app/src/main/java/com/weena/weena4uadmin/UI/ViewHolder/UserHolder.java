package com.blood.bloodbankadmin.UI.ViewHolder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.UseritemBinding;

public class UserHolder extends RecyclerView.ViewHolder {

    public UseritemBinding binding;

    public UserHolder(@NonNull UseritemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
