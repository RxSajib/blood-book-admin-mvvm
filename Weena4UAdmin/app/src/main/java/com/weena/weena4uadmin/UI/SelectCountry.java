package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.CountryModel;
import com.blood.bloodbankadmin.CountryNameResponse;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.CountryViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.CountryAdapter;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.databinding.SelectcountryBinding;

import java.util.List;

import javax.inject.Inject;

public class SelectCountry extends AppCompatActivity {

    private SelectcountryBinding binding;
    private ViewModel viewModel;
    @Inject
    CountryAdapter countryAdapter;
    private CountryViewModel countryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.selectcountry);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);



        countryViewModel = new ViewModelProvider(this).get(CountryViewModel.class);
        var component = DaggerComponenet.create();
        component.InjectSelectCountry(this);

        GetCountryData();
        InitView();
    }

    private void InitView() {
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.SelectCountry));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(SelectCountry.this);
        });


    }

    private void GetCountryData() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(countryAdapter);
        viewModel.GetCountryDataFromAPI().observe(this, countryNameResponses -> {
            if (countryNameResponses != null) {
                countryAdapter.setList(countryNameResponses);
                countryAdapter.notifyDataSetChanged();
                binding.ProgressBar.setVisibility(View.GONE);
            } else {
                binding.ProgressBar.setVisibility(View.VISIBLE);
            }
        });

        countryAdapter.OnClickLisiner((Name, Icon) -> {
            var model = new CountryModel();
            model.setCountryName(Name);
            model.setIcon(Icon);
            countryViewModel.AddCountry(model);
            finish();
            Animatoo.animateSlideRight(SelectCountry.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SelectCountry.this);
    }
}