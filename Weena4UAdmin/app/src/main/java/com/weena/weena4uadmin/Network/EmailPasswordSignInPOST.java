package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbankadmin.Utils.Toast;

public class EmailPasswordSignInPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public EmailPasswordSignInPOST(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> LoginEmailPassword(String Email, String Password) {
        data = new MutableLiveData<>();

        Mauth.signInWithEmailAndPassword(Email, Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    data.setValue(true);
                    Toast.SetMessage(application, "Success");
                } else {
                    data.setValue(false);
                    Toast.SetMessage(application, task.getException().getMessage());
                }
            }
        }).addOnFailureListener(e -> {
            data.setValue(false);
            Toast.SetMessage(application, e.getMessage());
        });
        return data;

    }
}
