package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.BloodRequestModel;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.BloodDonorPagerAdapter;
import com.blood.bloodbankadmin.UI.Adapter.BloodRequestAdapter;
import com.blood.bloodbankadmin.UI.BloodDonor.RegisterDonor;
import com.blood.bloodbankadmin.UI.BloodDonor.RegisterFriend;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.BloodrequestBinding;

import java.util.Set;

import javax.inject.Inject;

public class BloodRequest extends AppCompatActivity {

    private BloodrequestBinding binding;
    private ViewModel viewModel;
    @Inject
    BloodRequestAdapter bloodRequestAdapter;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodrequest);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = DaggerComponenet.create();
        component.InjectBloodRequest(this);

        InitView();
        GetBloodRequest();
//        SetFragment();
    }




    private void InitView() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodRequestAdapter);
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.BloodRequest));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodRequest.this);
        });
    }

    private void GetBloodRequest() {
        viewModel.GetBloodRequest().observe(BloodRequest.this, bloodRequestModels -> {
            bloodRequestAdapter.setList(bloodRequestModels);
            bloodRequestAdapter.notifyDataSetChanged();
            if (bloodRequestModels == null) {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.message.setVisibility(View.VISIBLE);
            } else {
                binding.Icon.setVisibility(View.GONE);
                binding.message.setVisibility(View.GONE);
            }
        });

        bloodRequestAdapter.OnClickEvent(bloodRequestModel -> {
            var alertdialog = new AlertDialog.Builder(BloodRequest.this);
            alertdialog.setTitle("Delete");
            alertdialog.setMessage("Do you want to delete?");
            alertdialog.setPositiveButton("Yes", (dialogInterface, i) -> {

            });
            alertdialog.setNegativeButton("No", (dialogInterface, i) -> {
            });

            var dialog = alertdialog.create();
            dialog.show();
        });

    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodRequest.this);
    }
}