package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.RegisterDonationModel;
import com.blood.bloodbankadmin.UI.Adapter.HistoryAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.databinding.LastdonationhistoryBinding;

import java.util.List;

public class LastDonationHistory extends AppCompatActivity {

    private LastdonationhistoryBinding binding;
    private ViewModel viewModel;
    private String UID;
    private HistoryAdapter historyAdapter = new HistoryAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.lastdonationhistory);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        UID = getIntent().getStringExtra(DataManager.UID);


        InitView();
        GetData();
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(LastDonationHistory.this);
        });
        binding.Toolbar.ToolbarTitle.setText("History Of Donation");

        historyAdapter.OnClickState(registerDonationModel -> {
            HandleActivity.BloodDonationHistoryDetails(LastDonationHistory.this, registerDonationModel);
            Animatoo.animateSlideLeft(LastDonationHistory.this);
        });

        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.AddRegisterDonation(LastDonationHistory.this);
            Animatoo.animateSlideLeft(LastDonationHistory.this);
        });
    }

    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(historyAdapter);
        viewModel.RegisterDonationGET(UID).observe(this, registerDonationModels -> {
            historyAdapter.setList(registerDonationModels);
            historyAdapter.notifyDataSetChanged();
            if(registerDonationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(LastDonationHistory.this);
    }
}