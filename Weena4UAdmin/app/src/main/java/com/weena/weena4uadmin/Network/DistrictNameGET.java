package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DistrictNameModel;
import com.blood.bloodbankadmin.Utils.Toast;
import java.util.List;

public class DistrictNameGET {
    private Application application;
    private MutableLiveData<List<DistrictNameModel>> data;
    private CollectionReference LocationRef;
    private FirebaseAuth Mauth;

    public DistrictNameGET(Application application){
        this.application = application;
        LocationRef = FirebaseFirestore.getInstance().collection(DataManager.Location);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<DistrictNameModel>> GetDistrictName(String CountryName, String ProvinceName){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = LocationRef.whereEqualTo(DataManager.CountryName, CountryName).whereEqualTo(DataManager.Provance, ProvinceName);
            q.addSnapshotListener((value, error) -> {
                if (error != null) {
                    data.setValue(null);
                    Toast.SetMessage(application, "error null");
                    return;
                }
                if (!value.isEmpty()) {
                    for (var ds : value.getDocumentChanges()) {
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(DistrictNameModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                    Toast.SetMessage(application, "null");
                }
            });
        }
        return data;
    }
}
