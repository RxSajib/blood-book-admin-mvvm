package com.blood.bloodbankadmin.UI;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.BloodBankAdapter;
import com.blood.bloodbankadmin.UserModel;
import com.blood.bloodbankadmin.Utils.Permission;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.BloodbankBinding;


import javax.inject.Inject;

public class BloodBank extends AppCompatActivity {

    private BloodbankBinding binding;
    private ActivityResultLauncher<Intent> launcher;
    private int PermissionCode = 100;
    private ViewModel viewModel;
    ProgressDialog progressDialog;
    private String ImageDownloadUri = null;
    @Inject BloodBankAdapter bloodBankAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.bloodbank);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = DaggerComponenet.create();
        component.InjectBloodBank(this);

        GetImageGallery();
        InitView();
        GetDataFromServer();
    }

    private void GetDataFromServer(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(bloodBankAdapter);

        viewModel.GetBloodBank().observe(this, bloodBankModel -> {
            if(bloodBankModel != null){
                binding.setBloodBank(bloodBankModel);
            }
        });

        viewModel.GetHospitalNameList().observe(this, bloodBankModels -> {
            bloodBankAdapter.setList(bloodBankModels);
            bloodBankAdapter.notifyDataSetChanged();
        });
    }

    private void InitView() {
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BloodBank.this);
        });

        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.AddHospital));


        viewModel.GetCurrentUser().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {
                if (userModel != null) {
                    binding.DetailsText.setText("Account: " + userModel.getEmail());
                }
            }
        });

        binding.AddHospitalIcon.setOnClickListener(view -> {
            if (Permission.ReadExternalMemoryPermission(BloodBank.this, PermissionCode)) {
                var intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                launcher.launch(intent);
            }
        });

        binding.AddHospitalBtn.setOnClickListener(view -> {
            var HospitalName = binding.HospitalNameInput.getText().toString().trim();
            var LocationOfHospital = binding.HospitalDetailsInput.getText().toString().trim();

            if (HospitalName.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Hospital name empty");
            } else if (LocationOfHospital.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Hospital location empty");
            } else {
                progressDialog.ProgressDialog(BloodBank.this);
                viewModel.RegisterBloodBank(HospitalName, "", LocationOfHospital, ImageDownloadUri).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });

    }


    private void GetImageGallery() {
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                progressDialog.ProgressDialog(BloodBank.this);
                if (result.getResultCode() == RESULT_OK) {
                    binding.AddHospitalIcon.setImageURI(result.getData().getData());
                    viewModel.UploadBloodBankIcon(result.getData().getData()).observe(BloodBank.this, new Observer<String>() {
                        @Override
                        public void onChanged(String s) {
                            if (s != null) {
                                progressDialog.CancelProgressDialog();
                                ImageDownloadUri = s;
                            }
                        }
                    });
                } else {
                    progressDialog.CancelProgressDialog();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BloodBank.this);
    }
}