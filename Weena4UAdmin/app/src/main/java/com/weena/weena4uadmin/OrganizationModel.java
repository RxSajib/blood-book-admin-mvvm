package com.blood.bloodbankadmin;

import android.os.Parcelable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrganizationModel implements Serializable {
    private String Address, Country, District, NameOfOrganization, OrganizationLogo, OrganizationType;
    private String PhoneNumber, Province;
    private long DocumentKey, Timestamp;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
