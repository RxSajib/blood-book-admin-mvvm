package com.blood.bloodbankadmin.Network.AndroidViewModel;

import android.app.Application;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.blood.bloodbankadmin.AdModel;
import com.blood.bloodbankadmin.BloodBankModel;
import com.blood.bloodbankadmin.BloodRequestModel;
import com.blood.bloodbankadmin.CountryNameModel;
import com.blood.bloodbankadmin.CountryNameResponse;
import com.blood.bloodbankadmin.DistrictNameGET;
import com.blood.bloodbankadmin.DistrictNameModel;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.LocationModel;
import com.blood.bloodbankadmin.LostAndFoundModel;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.Network.AddHospitalDataPOST;
import com.blood.bloodbankadmin.Network.AddRegisterDonationPOST;
import com.blood.bloodbankadmin.Network.AdsManagerSizeGET;
import com.blood.bloodbankadmin.Network.BloodBankGET;
import com.blood.bloodbankadmin.Network.BloodBankLogoUpdatePOST;
import com.blood.bloodbankadmin.Network.BloodDonationHistoryUpdate;
import com.blood.bloodbankadmin.Network.BloodDonorSize;
import com.blood.bloodbankadmin.Network.BloodRequestGET;
import com.blood.bloodbankadmin.Network.BloodRequestSize;
import com.blood.bloodbankadmin.Network.CountryAPI.CountryNameDataGET;
import com.blood.bloodbankadmin.Network.CountryNameGET;
import com.blood.bloodbankadmin.Network.CreateProfilePOST;
import com.blood.bloodbankadmin.Network.CurrentUserGET;
import com.blood.bloodbankadmin.Network.DashBoardAdGET;
import com.blood.bloodbankadmin.Network.DashboardAdPOST;
import com.blood.bloodbankadmin.Network.DeleteDashBoardAds;
import com.blood.bloodbankadmin.Network.DeleteLostAndFoundType;
import com.blood.bloodbankadmin.Network.DonorUserGET;
import com.blood.bloodbankadmin.Network.DonorUserLastDonationDateUpdate;
import com.blood.bloodbankadmin.Network.EmailPasswordSignInPOST;
import com.blood.bloodbankadmin.Network.EmailPasswordSignUpPOST;
import com.blood.bloodbankadmin.Network.FilterLostAndFoundGET;
import com.blood.bloodbankadmin.Network.FilterOrganizationGET;
import com.blood.bloodbankadmin.Network.HospitalListGET;
import com.blood.bloodbankadmin.Network.LocationGET;
import com.blood.bloodbankadmin.Network.LocationPOST;
import com.blood.bloodbankadmin.Network.LostAndFoundDataPOST;
import com.blood.bloodbankadmin.Network.LostAndFoundGET;
import com.blood.bloodbankadmin.Network.LostAndFoundTypeGET;
import com.blood.bloodbankadmin.Network.LostAndFoundTypePOST;
import com.blood.bloodbankadmin.Network.LostFoundSizeGET;
import com.blood.bloodbankadmin.Network.OrganizationDataGET;
import com.blood.bloodbankadmin.Network.OrganizationImageUploadPOST;
import com.blood.bloodbankadmin.Network.OrganizationItemDelete;
import com.blood.bloodbankadmin.Network.OrganizationTypeDataGET;
import com.blood.bloodbankadmin.Network.OrganizationTypeGET;
import com.blood.bloodbankadmin.Network.OrganizationTypePOST;
import com.blood.bloodbankadmin.Network.ProvinceNameGET;
import com.blood.bloodbankadmin.Network.RegisterAsDonorUpdate;
import com.blood.bloodbankadmin.Network.RegisterAsFriendGET;
import com.blood.bloodbankadmin.Network.RegisterBloodBank;
import com.blood.bloodbankadmin.Network.RegisterDonationGET;
import com.blood.bloodbankadmin.Network.RegisterDonationImagePOST;
import com.blood.bloodbankadmin.Network.RegisterDonationImageUpdate;
import com.blood.bloodbankadmin.Network.RegisterDonorGET;
import com.blood.bloodbankadmin.Network.SearchLostAndFoundGET;
import com.blood.bloodbankadmin.Network.SearchTelephonyManager;
import com.blood.bloodbankadmin.Network.SearchUserGET;
import com.blood.bloodbankadmin.Network.SignInEmailPasswordPOST;
import com.blood.bloodbankadmin.Network.TelePhoneDirectoryPatch;
import com.blood.bloodbankadmin.Network.TelephoneDirectorySize;
import com.blood.bloodbankadmin.Network.TeliphonyManagerDelete;
import com.blood.bloodbankadmin.Network.UserExistsGET;
import com.blood.bloodbankadmin.Network.UserGET;
import com.blood.bloodbankadmin.Network.UserSizeGET;
import com.blood.bloodbankadmin.OrganizationModel;
import com.blood.bloodbankadmin.OrganizationTypeModel;
import com.blood.bloodbankadmin.ProvinceNameModel;
import com.blood.bloodbankadmin.RegisterDonationModel;
import com.blood.bloodbankadmin.UserModel;

import java.util.List;

import io.grpc.internal.DnsNameResolver;

public class ViewModel extends AndroidViewModel {

    private RegisterDonationImagePOST registerDonationImagePOST;
    private DonorUserLastDonationDateUpdate donorUserLastDonationDateUpdate;
    private AddRegisterDonationPOST addRegisterDonationPOST;
    private BloodDonationHistoryUpdate bloodDonationHistoryUpdate;
    private RegisterDonationImageUpdate registerDonationImageUpdate;
    private RegisterDonationGET registerDonationGET;
    private RegisterAsDonorUpdate registerAsDonorUpdate;
    private RegisterDonorGET registerDonorGET;
    private RegisterAsFriendGET registerAsFriendGET;
    private BloodRequestGET bloodRequestGET;
    private SearchUserGET searchUserGET;
    private UserGET userGET;
    private AdsManagerSizeGET adsManagerSizeGET;
    private DeleteDashBoardAds deleteDashBoardAds;
    private DashBoardAdGET dashBoardAdGET;
    private DashboardAdPOST dashboardAdPOST;
    private SignInEmailPasswordPOST  signInEmailPasswordPOST;
    private FilterLostAndFoundGET filterLostAndFoundGET;
    private SearchTelephonyManager searchTelephonyManager;
    private FilterOrganizationGET filterOrganizationGET;
    private OrganizationTypeDataGET organizationTypeDataGET;
    private SearchLostAndFoundGET searchLostAndFoundGET;
    private LostFoundSizeGET lostFoundSizeGET;
    private LostAndFoundGET lostAndFoundGET;
    private LostAndFoundDataPOST lostAndFoundDataPOST;
    private DeleteLostAndFoundType deleteLostAndFoundType;
    private LostAndFoundTypeGET lostAndFoundTypeGET;
    private LostAndFoundTypePOST lostAndFoundTypePOST;
    private TelephoneDirectorySize telephoneDirectorySize;
    private LocationGET locationGET;
    private LocationPOST locationPOST;
    private CountryNameDataGET countryNameDataGET;
    private TeliphonyManagerDelete teliphonyManagerDelete;
    private TelePhoneDirectoryPatch telePhoneDirectoryPatch;
    private OrganizationDataGET organizationDataGET;
    private OrganizationItemDelete organizationItemDelete;
    private OrganizationTypeGET organizationTypeGET;
    private OrganizationTypePOST organizationTypePOST;
    private DistrictNameGET districtNameGET;
    private ProvinceNameGET provinceNameGET;
    private CountryNameGET countryNameGET;
    private AddHospitalDataPOST addHospitalDataPOST;
    private OrganizationImageUploadPOST organizationImageUploadPOST;
    private BloodRequestSize bloodRequestSize;
    private BloodDonorSize bloodDonorSize;
    private UserSizeGET userSizeGET;
    private HospitalListGET hospitalListGET;
    private BloodBankGET bloodBankGET;
    private CurrentUserGET currentUserGET;
    private BloodBankLogoUpdatePOST bloodBankLogoUpdatePOST;
    private RegisterBloodBank registerBloodBank;
    private DonorUserGET donorUserGET;
    public CreateProfilePOST createProfilePOST;
    private EmailPasswordSignUpPOST emailPasswordSignUpPOST;
    private EmailPasswordSignInPOST emailPasswordSignInPOST;
    private UserExistsGET userExistsGET;

    public ViewModel(@NonNull Application application) {
        super(application);

        registerDonationImagePOST = new RegisterDonationImagePOST(application);
        donorUserLastDonationDateUpdate = new DonorUserLastDonationDateUpdate(application);
        addRegisterDonationPOST = new AddRegisterDonationPOST(application);
        bloodDonationHistoryUpdate = new BloodDonationHistoryUpdate(application);
        registerDonationImageUpdate = new RegisterDonationImageUpdate(application);
        registerDonationGET = new RegisterDonationGET(application);
        registerAsDonorUpdate = new RegisterAsDonorUpdate(application);
        registerDonorGET = new RegisterDonorGET(application);
        registerAsFriendGET = new RegisterAsFriendGET(application);
        bloodRequestGET = new BloodRequestGET(application);
        searchUserGET = new SearchUserGET(application);
        userGET = new UserGET(application);
        adsManagerSizeGET = new AdsManagerSizeGET(application);
        deleteDashBoardAds = new DeleteDashBoardAds(application);
        dashBoardAdGET = new DashBoardAdGET(application);
        dashboardAdPOST = new DashboardAdPOST(application);
        signInEmailPasswordPOST = new SignInEmailPasswordPOST(application);
        filterLostAndFoundGET = new FilterLostAndFoundGET(application);
        searchTelephonyManager = new SearchTelephonyManager(application);
        filterOrganizationGET = new FilterOrganizationGET(application);
        organizationTypeDataGET = new OrganizationTypeDataGET(application);
        searchLostAndFoundGET = new SearchLostAndFoundGET(application);
        lostFoundSizeGET = new LostFoundSizeGET(application);
        lostAndFoundGET = new LostAndFoundGET(application);
        lostAndFoundDataPOST = new LostAndFoundDataPOST(application);
        deleteLostAndFoundType = new DeleteLostAndFoundType(application);
        lostAndFoundTypeGET = new LostAndFoundTypeGET(application);
        lostAndFoundTypePOST = new LostAndFoundTypePOST(application);
        telephoneDirectorySize = new TelephoneDirectorySize(application);
        locationGET = new LocationGET(application);
        locationPOST = new LocationPOST(application);
        countryNameDataGET = new CountryNameDataGET(application);
        teliphonyManagerDelete = new TeliphonyManagerDelete(application);
        telePhoneDirectoryPatch = new TelePhoneDirectoryPatch(application);
        organizationDataGET = new OrganizationDataGET(application);
        organizationItemDelete = new OrganizationItemDelete(application);
        organizationTypeGET = new OrganizationTypeGET(application);
        organizationTypePOST = new OrganizationTypePOST(application);
        districtNameGET = new DistrictNameGET(application);
        provinceNameGET = new ProvinceNameGET(application);
        countryNameGET = new CountryNameGET(application);
        addHospitalDataPOST = new AddHospitalDataPOST(application);
        organizationImageUploadPOST = new OrganizationImageUploadPOST(application);
        bloodRequestSize = new BloodRequestSize(application);
        bloodDonorSize = new BloodDonorSize(application);
        userSizeGET = new UserSizeGET(application);
        hospitalListGET = new HospitalListGET(application);
        bloodBankGET = new BloodBankGET(application);
        currentUserGET = new CurrentUserGET(application);
        bloodBankLogoUpdatePOST = new BloodBankLogoUpdatePOST(application);
        registerBloodBank = new RegisterBloodBank(application);
        donorUserGET = new DonorUserGET(application);
        createProfilePOST = new CreateProfilePOST(application);
        emailPasswordSignUpPOST = new EmailPasswordSignUpPOST(application);
        emailPasswordSignInPOST = new EmailPasswordSignInPOST(application);
        userExistsGET = new UserExistsGET(application);
    }

    public LiveData<String> UploadRegisterDonationImage(Uri ImageUri){
        return registerDonationImagePOST.UploadRegisterDonationImage(ImageUri);
    }
    //todo update donor last donation
    public LiveData<Boolean> DonorUserLastDonationDateUpdate(String UID, String Date){
        return donorUserLastDonationDateUpdate.DonorUserLastDonationDateUpdate(UID, Date);
    }
    public LiveData<Boolean> AddRegisterDonation(String Image, String SenderUID, String BloodDonationDate, String Country, String District, String HospitalName, String Province, String QuantityOfBlood){
        return addRegisterDonationPOST.AddRegisterDonation(Image,SenderUID, BloodDonationDate, Country, District, HospitalName, Province, QuantityOfBlood);
    }
    public LiveData<Boolean> RegisterDonationUpdate(String UID, String BloodDonationDate, String Country, String District, String HospitalName, String Province, String QuantityOfBlood){
        return bloodDonationHistoryUpdate.RegisterDonationUpdate(UID, BloodDonationDate, Country, District, HospitalName, Province, QuantityOfBlood);
    }
    public LiveData<Boolean> RegisterDonationImageUpdate(String UID, Uri ImageUri){
        return registerDonationImageUpdate.RegisterDonationImageUpdate(UID, ImageUri);
    }
    public LiveData<List<RegisterDonationModel>> RegisterDonationGET(String UID){
        return registerDonationGET.GetRegisterDonation(UID);
    }
    public LiveData<Boolean> UpdateRegisterAsDonor(String UID,String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Weight, String Country, String Province, String District, String AreaName, String PhoneNumber, String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber, String EmailAddress){
        return registerAsDonorUpdate.UpdateRegisterAsDonor(UID, GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName, PhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress);
    }
    public LiveData<List<DonorModel>> RegisterAsDonorGET(){
        return registerDonorGET.RegisterDonorGET();
    }
    public LiveData<List<DonorModel>> RegisterAsFriendGET(){
        return registerAsFriendGET.RegisterAsFriendGET();
    }
    public LiveData<List<BloodRequestModel>> GetBloodRequest(){
        return bloodRequestGET.GetBloodRequest();
    }
    public LiveData<List<UserModel>> SearchUserByName(String Name){
        return searchUserGET.SearchByUserName(Name);
    }
    public LiveData<List<UserModel>> GetUser(){
        return userGET.GetUser();
    }
    public LiveData<Integer> SizeOFAdManager(){
        return adsManagerSizeGET.GetSizeOFAdManager();
    }
    public LiveData<Boolean> DeleteDashBoardAd(){
        return deleteDashBoardAds.DeleteDashBoardAd();
    }
    public LiveData<AdModel> GetDashBoardAdData(){
        return dashBoardAdGET.GetDashBoardAdData();
    }
    public LiveData<Boolean> DashBoardAdPost(String WebUrl, String PhotoUrl, String Type){
        return dashboardAdPOST.DashboardAdsPOST(WebUrl, PhotoUrl, Type);
    }
    public LiveData<Boolean> SignInEmailPassword(String Email, String Password){
        return signInEmailPasswordPOST.SignInEmailPassword(Email, Password);
    }
    public LiveData<List<LostAndFoundModel>> FilterLostAndFound(String country, String province, String district, String orgType){
        return filterLostAndFoundGET.FilterLostAndFound(country, province, district, orgType);
    }
    public LiveData<List<OrganizationModel>> SearchOrganization(String OrgName){
        return searchTelephonyManager.SearchTelephonyManager(OrgName);
    }
    public LiveData<List<OrganizationModel>> FilterOrganizationData(String Country, String Province, String District, String OrganizationType){
        return filterOrganizationGET.FilterOrganization(Country, Province, District, OrganizationType);
    }
    public LiveData<List<OrganizationTypeModel>> GetOrganizationType(){
        return organizationTypeDataGET.GetOrganizationType();
    }

    public LiveData<List<LostAndFoundModel>> SearchLostAndFound(String LostAndFound){
        return searchLostAndFoundGET.SearchLostAndFound(LostAndFound);
    }

    public LiveData<Integer> LostFoundSize(){
        return lostFoundSizeGET.LostFoundSize();
    }

    public LiveData<List<LostAndFoundModel>> GetLostAndFound(){
        return lostAndFoundGET.GetLostAndFound();
    }
    public LiveData<Boolean> LostAndFoundDataPost(String PostTitle, String LostAndFound, String TelNumber, String Country, String Province, String District, String FullDetails, String Image){
        return lostAndFoundDataPOST.UploadLostAndFound(PostTitle, LostAndFound, TelNumber, Country, Province, District, FullDetails, Image);
    }
    public LiveData<Boolean> DeleteLostAndFound(long DocumentID) {
        return deleteLostAndFoundType.DeleteLostAndFoundType(DocumentID);
    }

    public LiveData<List<LostAndFoundTypeModel>> GetLostAndFoundType() {
        return lostAndFoundTypeGET.GetLostAndFoundType();
    }

    public LiveData<Boolean> UploadLostAndFoundCategory(String Category) {
        return lostAndFoundTypePOST.UploadLostAndFoundType(Category);
    }

    public LiveData<Integer> GetTelephonyDirectorySize() {
        return telephoneDirectorySize.GetTelephoneDirectorySize();
    }

    public LiveData<List<LocationModel>> GetLocationData(String CountryName) {
        return locationGET.GetLocation(CountryName);
    }

    public LiveData<Boolean> AddLocation(String Country, String Province, String District) {
        return locationPOST.AddLocation(Country, Province, District);
    }

    public LiveData<List<CountryNameResponse>> GetCountryDataFromAPI() {
        return countryNameDataGET.GetCountryName();
    }

    public LiveData<Boolean> TeliphonyManagerDelete(long DocumentKey) {
        return teliphonyManagerDelete.DeleteTeliphoneyManager(DocumentKey);
    }

    public LiveData<Boolean> TelephoneyManagerUpdate(long DocumentID, String NameOfOrganization, String OrganizationType, String PhoneNumber, String Country, String Province, String District, String Address, String ImageUri) {
        return telePhoneDirectoryPatch.UpdateTelephoneManager(DocumentID, NameOfOrganization, OrganizationType, PhoneNumber, Country, Province, District, Address, ImageUri);
    }

    public LiveData<List<OrganizationModel>> GetOrganizationData() {
        return organizationDataGET.GetOrganizationData();
    }

    public LiveData<Boolean> OrganizationItemDelete(String UID) {
        return organizationItemDelete.OrganizationItemDelete(UID);
    }

    public LiveData<List<OrganizationTypeModel>> GetOrgnizationType() {
        return organizationTypeGET.GetOrganizationType();
    }

    public LiveData<Boolean> AddOrgType(String OrgTitle) {
        return organizationTypePOST.AddOrganizationType(OrgTitle);
    }

    public LiveData<List<DistrictNameModel>> GetDistrictName(String CountryName, String ProvinceName) {
        return districtNameGET.GetDistrictName(CountryName, ProvinceName);
    }

    public LiveData<List<ProvinceNameModel>> GetProvinceName(String CountryName) {
        return provinceNameGET.GetProvinceData(CountryName);
    }

    public LiveData<List<CountryNameModel>> GetCountryName() {
        return countryNameGET.GetCountryNameData();
    }

    public LiveData<Boolean> AddOrganizationData(String NameOfOrganization, String OrganizationType, String PhoneNumber, String Country, String Province, String District, String Address, String ImageUri) {
        return addHospitalDataPOST.UploadOrganization(NameOfOrganization, OrganizationType, PhoneNumber, Country, Province, District, Address, ImageUri);
    }

    public LiveData<String> OrganizationLogo(Uri ImageUri) {
        return organizationImageUploadPOST.UploadOrganizationImage(ImageUri);
    }

    public LiveData<Integer> BloodRequestSize() {
        return bloodRequestSize.GetBloodRequestSize();
    }

    public LiveData<Integer> BloodDonorSize() {
        return bloodDonorSize.GetBloodDonorSize();
    }

    public LiveData<Integer> UserSize() {
        return userSizeGET.GetUserSize();
    }

    public LiveData<List<BloodBankModel>> GetHospitalNameList() {
        return hospitalListGET.GetHospitalName();
    }

    public LiveData<BloodBankModel> GetBloodBank() {
        return bloodBankGET.GetBloodBank();
    }

    public LiveData<UserModel> GetCurrentUser() {
        return currentUserGET.GetCurrentUser();
    }

    public LiveData<String> UploadBloodBankIcon(Uri ImageUri) {
        return bloodBankLogoUpdatePOST.UploadBloodBankIcon(ImageUri);
    }

    public LiveData<Boolean> RegisterBloodBank(String HospitalName, String EmailAddress, String Location, String HospitalIcon) {
        return registerBloodBank.RegisterBloodBank(HospitalName, EmailAddress, Location, HospitalIcon);
    }

    public LiveData<List<DonorModel>> GetDonor() {
        return donorUserGET.GetDonor();
    }

    public LiveData<Boolean> CreateProfile(String Email, String Name, String PhotoUri) {
        return createProfilePOST.SetUpProfile(Email, Name, PhotoUri);
    }

    public LiveData<Boolean> EmailPasswordSignUpAccount(String Email, String Password) {
        return emailPasswordSignUpPOST.EmailPasswordSignUp(Email, Password);
    }

    public LiveData<Boolean> EmailPasswordSignInAccount(String Email, String Password) {
        return emailPasswordSignInPOST.LoginEmailPassword(Email, Password);
    }

    public LiveData<Boolean> UserExists() {
        return userExistsGET.UserExists();
    }
}
