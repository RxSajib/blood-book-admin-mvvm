package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.CountryModel;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.LocationModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.CountryLocationAdapter;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.AddcountrylocationBinding;
import com.blood.bloodbankadmin.databinding.AddlocationdialogBinding;

import java.util.List;

import javax.inject.Inject;

public class AddCountryLocation extends AppCompatActivity {

    private AddcountrylocationBinding binding;
    private CountryModel countryModel;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;
    @Inject
    CountryLocationAdapter countryLocationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addcountrylocation);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = DaggerComponenet.create();
        component.InjectAddCountryLocation(this);

        countryModel = (CountryModel)getIntent().getSerializableExtra(DataManager.Data);
        InitView();
        GetLocation();
    }

    private void GetLocation(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(countryLocationAdapter);

        viewModel.GetLocationData(countryModel.getCountryName()).observe(this, locationModels -> {
            countryLocationAdapter.setList(locationModels);
            countryLocationAdapter.notifyDataSetChanged();
            if(locationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView(){
        binding.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddCountryLocation.this);
        });

        binding.setCountry(countryModel);
        binding.AddLocation.setOnClickListener(view -> {
            var dialog = new AlertDialog.Builder(AddCountryLocation.this);
            AddlocationdialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.addlocationdialog, null, false);
            dialog.setView(binding.getRoot());

            var mydialog = dialog.create();
            mydialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mydialog.show();


            binding.AddLocationBtn.setOnClickListener(view1 -> {
                var Province = binding.ProvinceInput.getText().toString().trim();
                var District = binding.DistrictInput.getText().toString().trim();

                if(Province.isEmpty()){
                    Toast.SetMessage(AddCountryLocation.this, "Province empty");
                }else if(District.isEmpty()){
                    Toast.SetMessage(AddCountryLocation.this, "District empty");
                }else {
                    progressDialog.ProgressDialog(AddCountryLocation.this);
                    viewModel.AddLocation(countryModel.getCountryName(), Province, District).observe(this, aBoolean -> {
                        if(aBoolean){
                            progressDialog.CancelProgressDialog();
                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                    binding.ProvinceInput.setText(null);
                    binding.DistrictInput.setText(null);
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddCountryLocation.this);
    }
}