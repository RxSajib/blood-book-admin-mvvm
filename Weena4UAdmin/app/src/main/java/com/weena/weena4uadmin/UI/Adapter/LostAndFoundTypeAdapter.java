package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.UI.ViewHolder.LostAndFoundTypeViewHolder;
import com.blood.bloodbankadmin.databinding.LostandfoundtypeitemBinding;
import java.util.List;
import javax.inject.Inject;
import lombok.Getter;
import lombok.Setter;

public class LostAndFoundTypeAdapter extends RecyclerView.Adapter<LostAndFoundTypeViewHolder> {
    @Setter @Getter
    private List<LostAndFoundTypeModel> list;
    private OnClick OnClick;
    private DeleteItem DeleteItem;

    @Inject
    public LostAndFoundTypeAdapter(){

    }

    @NonNull
    @Override
    public LostAndFoundTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LostandfoundtypeitemBinding.inflate(l, parent, false);
        return new LostAndFoundTypeViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull LostAndFoundTypeViewHolder holder, int position) {
        holder.binding.setLostAndFoundType(list.get(position));
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getCategory());
        });

        holder.binding.DeleteBtn.setOnClickListener(view -> {
            DeleteItem.Remove(list.get(position).getDocumentKey());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String Category);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }

    public interface DeleteItem{
        void Remove(long DocumentID);
    }
    public void OnDeleteState(DeleteItem DeleteItem){
        this.DeleteItem = DeleteItem;
    }
}
