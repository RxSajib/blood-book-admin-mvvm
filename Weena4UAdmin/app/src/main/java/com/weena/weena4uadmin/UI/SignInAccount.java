package com.blood.bloodbankadmin.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.SigninaccountBinding;

import javax.inject.Inject;

public class SignInAccount extends AppCompatActivity {

    private SigninaccountBinding binding;
    @Inject ProgressDialog progressDialog;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private ViewModel viewModel;
    private FirebaseAuth Mauth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.signinaccount);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        var component = DaggerComponenet.create();
        component.InjectSignIn(this);
        Mauth = FirebaseAuth.getInstance();

        InitView();
        SignInAccount();
        creating_request();
        SignInEmailPassword();
    }

    private void SignInEmailPassword(){
        binding.LoginButton.setOnClickListener(view -> {
            var Email = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();
            if(Email.isEmpty()){
                Toast.SetMessage(SignInAccount.this, getResources().getString(R.string.EmailEmpty));
            }else if(Password.isEmpty()){
                Toast.SetMessage(SignInAccount.this, getResources().getString(R.string.PasswordEmpty));
            }else {
                progressDialog.ProgressDialog(SignInAccount.this);
                viewModel.SignInEmailPassword(Email, Password).observe(this, aBoolean -> {
                    if(aBoolean){
                        HandleActivity.GotoHome(SignInAccount.this);
                        finish();
                        Animatoo.animateSlideLeft(SignInAccount.this);
                        progressDialog.CancelProgressDialog();
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void InitView(){
        binding.GoogleSignIn.setOnClickListener(view -> {
            signIn();
        });
        binding.JoinNow.setOnClickListener(view -> {
            HandleActivity.GotoSignUp(SignInAccount.this);
            Animatoo.animateSlideLeft(SignInAccount.this);
        });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @SuppressLint("NotConstructor")
    private void SignInAccount(){
        binding.JoinNow.setOnClickListener(view -> {
            var Email = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();
            binding.PasswordMessage.setVisibility(View.GONE);
            binding.EmailMessage.setVisibility(View.GONE);


            if(Email.isEmpty()){
                binding.EmailMessage.setVisibility(View.VISIBLE);
            }else if(Password.isEmpty()){
                binding.PasswordMessage.setVisibility(View.VISIBLE);
            }else {
                progressDialog.ProgressDialog(SignInAccount.this);
                viewModel.EmailPasswordSignInAccount(Email, Password).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        viewModel.EmailPasswordSignInAccount(Email, Password).observe(SignInAccount.this, aBoolean1 -> {
                            if(aBoolean1){
                                HandleActivity.GotoHome(SignInAccount.this);
                                Animatoo.animateSlideLeft(SignInAccount.this);
                                finish();
                            }
                        });
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SignInAccount.this);
    }




    private void creating_request() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(SignInAccount.this, gso);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            progressDialog.ProgressDialog(SignInAccount.this);
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Toast.SetMessage(SignInAccount.this, e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {

        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        Mauth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String Email = task.getResult().getUser().getEmail();
                    String Name = task.getResult().getUser().getDisplayName();
                    String PhotoUri = task.getResult().getUser().getPhotoUrl().toString();
                    SetUpProfileData(Email, Name, PhotoUri);
                } else {

                    Toast.SetMessage(SignInAccount.this, task.getException().getMessage());
                }
            }
        });
    }


    private void SetUpProfileData(String Email, String Name, String PhotoUri){
        viewModel.CreateProfile(Email, Name, PhotoUri).observe(this, aBoolean -> {
            if (aBoolean) {
                progressDialog.CancelProgressDialog();
                HandleActivity.GotoHome(SignInAccount.this);
                Animatoo.animateSlideLeft(SignInAccount.this);
                finish();
            }else {
                progressDialog.CancelProgressDialog();
                HandleActivity.GotoHome(SignInAccount.this);
                Animatoo.animateSlideLeft(SignInAccount.this);
                finish();
            }
        });
    }
}