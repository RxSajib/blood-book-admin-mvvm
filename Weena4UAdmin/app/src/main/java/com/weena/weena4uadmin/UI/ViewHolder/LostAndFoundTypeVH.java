package com.blood.bloodbankadmin.UI.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.LostandfoundtypeBinding;

public class LostAndFoundTypeVH extends RecyclerView.ViewHolder {

    public LostandfoundtypeBinding binding;

    public LostAndFoundTypeVH(@NonNull LostandfoundtypeBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
