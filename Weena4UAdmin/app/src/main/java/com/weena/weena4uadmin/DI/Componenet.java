package com.blood.bloodbankadmin.DI;

import com.blood.bloodbankadmin.UI.AdManager;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.AddCountry;
import com.blood.bloodbankadmin.UI.AddCountryLocation;
import com.blood.bloodbankadmin.UI.AddHospitalData;
import com.blood.bloodbankadmin.UI.AddLostAndFound;
import com.blood.bloodbankadmin.UI.AppUsers;
import com.blood.bloodbankadmin.UI.BloodBank;
import com.blood.bloodbankadmin.UI.BloodDonors;
import com.blood.bloodbankadmin.UI.BloodRequest;
import com.blood.bloodbankadmin.UI.LostAndFound;
import com.blood.bloodbankadmin.UI.SelectCountry;
import com.blood.bloodbankadmin.UI.SignInAccount;
import com.blood.bloodbankadmin.UI.SignUpAccount;
import com.blood.bloodbankadmin.UI.TeliphonyManager;
import com.blood.bloodbankadmin.UI.UpdateTeliphonyManager;

import dagger.Component;

@Component
public interface Componenet {

    public void InjectSignIn(SignInAccount signInAccount);
    public void InjectSignUp(SignUpAccount signUpAccount);
    public void InjectBloodDonor(BloodDonors bloodDonors);
    public void InjectBloodBank(BloodBank bloodBank);
    public void InjectAddHospital(AddHospitalData addHospitalData);
    public void InjectTeliphonyManager(TeliphonyManager teliphonyManager);
    public void UpdateTeliphonyManager(UpdateTeliphonyManager updateTeliphonyManager);
    public void InjectSelectCountry(SelectCountry selectCountry);
    public void InjectAddCountry(AddCountry addCountry);
    public void InjectAddCountryLocation(AddCountryLocation addCountryLocation);
    public void InjectAddLostAndFound(AddLostAndFound addLostAndFound);
    public void InjectLostAndFound(LostAndFound lostAndFound);
    public void InjectAdManager(AdManager adManager);
    public void InjectUser(AppUsers appUsers);
    public void InjectBloodRequest(BloodRequest bloodRequest);
}
