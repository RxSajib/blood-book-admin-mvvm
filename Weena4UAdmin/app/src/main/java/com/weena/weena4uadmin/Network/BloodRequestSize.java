package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;

public class BloodRequestSize {

    private Application application;
    private MutableLiveData<Integer> data;
    private FirebaseAuth Mauth;
    private CollectionReference BloodRequestRef;

    public BloodRequestSize(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodRequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<Integer> GetBloodRequestSize(){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            BloodRequestRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }

                    if(!value.isEmpty()){
                        data.setValue(value.size());
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }
        return data;
    }
}
