package com.blood.bloodbankadmin.Widget;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.blood.bloodbankadmin.R;

import javax.inject.Inject;

public class FilterOptionDialog extends DialogFragment {

    @Inject
    public FilterOptionDialog(){

    }

    private Onclick Onclick;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        var alertdialog = new AlertDialog.Builder(getActivity());
        alertdialog.setTitle("Select Attachment Option");

        var item= getActivity().getResources().getStringArray(R.array.Filter);
        alertdialog.setItems(item, (dialogInterface, i) -> {
            Onclick.Click(i);
        });
        return alertdialog.create();

    }

    public interface Onclick{
        void Click(int Item);
    }

    public void OnclickLisiner(Onclick Onclick){
        this.Onclick = Onclick;
    }
}


