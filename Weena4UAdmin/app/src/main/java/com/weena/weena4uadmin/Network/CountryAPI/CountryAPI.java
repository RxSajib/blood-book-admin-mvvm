package com.blood.bloodbankadmin.Network.CountryAPI;

import com.blood.bloodbankadmin.CountryNameResponse;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CountryAPI {

    @GET("all")
    Call<List<CountryNameResponse>> GetCountryResponse();
}
