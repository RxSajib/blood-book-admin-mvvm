package com.blood.bloodbankadmin.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.OrganizationModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.OrganizationItemAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.Utils.Permission;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;
import com.blood.bloodbankadmin.databinding.OrgnaizationtypedialogBinding;
import com.blood.bloodbankadmin.databinding.OrgtypecategorydialogBinding;
import com.blood.bloodbankadmin.databinding.UpdateteliphonymanagerBinding;

import javax.inject.Inject;

public class UpdateTeliphonyManager extends AppCompatActivity {

    private UpdateteliphonymanagerBinding binding;
    private ActivityResultLauncher<Intent> launcher;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;
    private String ImageDownload = null;
    @Inject
    LocationDataAdapter locationDataAdapter;
    @Inject
    ProvinceNameDataAdapter provinceNameDataAdapter;
    @Inject
    DistrictNameDataAdapter districtNameDataAdapter;
    @Inject
    OrganizationItemAdapter organizationItemAdapter;
    private LocationViewModel locationViewModel;
    private OrganizationModel organizationModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.updateteliphonymanager);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);

        organizationModel = (OrganizationModel) getIntent().getSerializableExtra(DataManager.Data);
        binding.setOrganizationModel(organizationModel);

        var component = DaggerComponenet.create();
        component.UpdateTeliphonyManager(this);

        GetImageFromGallery();
        LocationDialog();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        InitView();
    }

    private void LocationDialog() {
        binding.CountryName.setKeyListener(null);
        binding.CountryName.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(UpdateTeliphonyManager.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);
            alertdialog.show();

            viewModel.GetCountryName().observe(this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });
            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.CountryName.setText(CountryName);
                alertdialog.dismiss();
                binding.SelectDistrictInput.setText(null);
                binding.ProvinceStateInput.setText(null);
            });

        });

        binding.ProvinceStateInput.setOnClickListener(view -> {
            var CountryName = binding.CountryName.getText().toString().trim();
            if (CountryName.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Country name empty");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(UpdateTeliphonyManager.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });
                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    binding.ProvinceStateInput.setText(ProvinceName);
                    alertdialog.dismiss();
                    binding.SelectDistrictInput.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.SelectDistrictInput.setOnClickListener(view -> {
            var CountryName = binding.CountryName.getText().toString().trim();
            var ProvincesName = binding.ProvinceStateInput.getText().toString().trim();
            if (CountryName.isEmpty()) {
                Toast.SetMessage(UpdateTeliphonyManager.this, "Select Country Name");
            } else if (ProvincesName.isEmpty()) {
                Toast.SetMessage(UpdateTeliphonyManager.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(UpdateTeliphonyManager.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.show();

                viewModel.GetDistrictName(CountryName, ProvincesName).observe(this, districtNameModels -> {
                    locationViewModel.InsertDistrict(districtNameModels);
                });
                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    binding.SelectDistrictInput.setText(DictrictName);
                    alertdialog.dismiss();
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });

            }
        });
    }

    private void InitView() {
        binding.OrgTypeInput.setKeyListener(null);
        binding.OrgTypeInput.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(UpdateTeliphonyManager.this);
            OrgnaizationtypedialogBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.orgnaizationtypedialog, null, false);
            alertdialog.setView(binding.getRoot());
            binding.RecyclerView.setHasFixedSize(true);
            binding.RecyclerView.setAdapter(organizationItemAdapter);

            viewModel.GetOrgnizationType().observe(this, organizationTypeModels -> {
                organizationItemAdapter.setList(organizationTypeModels);
                organizationItemAdapter.notifyDataSetChanged();
            });

            var dialog = alertdialog.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        });
        organizationItemAdapter.OnSelectLisiner(CategoryType -> {
            binding.OrgTypeInput.setText(CategoryType);
        });

        organizationItemAdapter.OnRemoveLisiner(DocumentKey -> {
            progressDialog.ProgressDialog(UpdateTeliphonyManager.this);
            viewModel.OrganizationItemDelete(String.valueOf(DocumentKey)).observe(this, aBoolean -> {
                if (aBoolean) {
                    progressDialog.CancelProgressDialog();
                } else {
                    progressDialog.CancelProgressDialog();
                }
            });
        });

        binding.AddBtn.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(UpdateTeliphonyManager.this);
            OrgtypecategorydialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.orgtypecategorydialog, null, false);
            alertdialog.setView(v.getRoot());

            var d = alertdialog.create();
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.show();

            v.AddBtn.setOnClickListener(view1 -> {
                var Title = v.OrgTypeInput.getText().toString().trim();
                if (Title.isEmpty()) {
                    Toast.SetMessage(UpdateTeliphonyManager.this, "Title require");
                } else {
                    progressDialog.ProgressDialog(UpdateTeliphonyManager.this);
                    viewModel.AddOrgType(Title).observe(this, aBoolean -> {
                        if (aBoolean) {
                            progressDialog.CancelProgressDialog();
                            binding.OrgNameInput.setText(null);
                        } else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                }
            });
        });


        binding.LogoUpload.setOnClickListener(view -> {
            if (Permission.ReadExternalMemoryPermission(UpdateTeliphonyManager.this, 15)) {
                var intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                launcher.launch(intent);
            }
        });

        binding.UploadButton.setOnClickListener(view -> {
            var NameOfOrg = binding.OrgNameInput.getText().toString().trim();
            var OrgType = binding.OrgTypeInput.getText().toString().trim();
            var Number = binding.TelephoneNumbersInput.getText().toString().trim();
            var Country = binding.CountryName.getText().toString().trim();
            var Province = binding.ProvinceStateInput.getText().toString().trim();
            var District = binding.SelectDistrictInput.getText().toString().trim();
            var Address = binding.AddressInput.getText().toString().trim();

            if (NameOfOrg.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Name of organization empty");
            } else if (OrgType.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Organization type empty");
            } else if (Number.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Number empty");
            } else if (Country.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Country name empty");
            } else if (Province.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Province name empty");
            } else if (District.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "District name empty");
            } else if (Address.isEmpty()) {
                Toast.SetMessage(getApplicationContext(), "Address name empty");
            } else {
                progressDialog.ProgressDialog(UpdateTeliphonyManager.this);
                viewModel.TelephoneyManagerUpdate(organizationModel.getDocumentKey(), NameOfOrg, OrgType, Number, Country, Province, District, Address, ImageDownload).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.animateSlideRight(UpdateTeliphonyManager.this);
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    private void GetImageFromGallery() {
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            progressDialog.ProgressDialog(UpdateTeliphonyManager.this);
            binding.LogoUpload.setImageURI(result.getData().getData());
            if (result.getResultCode() == RESULT_OK) {
                viewModel.OrganizationLogo(result.getData().getData()).observe(this, s -> {
                    if (s != null) {
                        progressDialog.CancelProgressDialog();
                        ImageDownload = s;
                    }
                    progressDialog.CancelProgressDialog();
                });
            } else {
                ImageDownload = null;
                progressDialog.CancelProgressDialog();
            }
        });
    }


    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(UpdateTeliphonyManager.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(UpdateTeliphonyManager.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(UpdateTeliphonyManager.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(UpdateTeliphonyManager.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(UpdateTeliphonyManager.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(UpdateTeliphonyManager.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(UpdateTeliphonyManager.this);
    }
}