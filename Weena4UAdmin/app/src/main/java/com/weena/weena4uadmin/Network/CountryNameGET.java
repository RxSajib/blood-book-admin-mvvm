package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.CountryNameModel;
import com.blood.bloodbankadmin.Data.DataManager;

import java.util.List;

public class CountryNameGET{

    private Application application;
    private MutableLiveData<List<CountryNameModel>> data;
    private CollectionReference CountryRef;
    private FirebaseAuth Mauth;

    public CountryNameGET(Application application) {
        this.application = application;
        CountryRef = FirebaseFirestore.getInstance().collection(DataManager.Location);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<CountryNameModel>> GetCountryNameData() {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {
            CountryRef.addSnapshotListener((value, error) -> {
                if (error != null) {
                    data.setValue(null);
                    return;
                }

                if (!value.isEmpty()) {
                    for (var ds : value.getDocumentChanges()) {
                        if (ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED) {
                            data.setValue(value.toObjects(CountryNameModel.class));
                        }
                    }
                } else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
