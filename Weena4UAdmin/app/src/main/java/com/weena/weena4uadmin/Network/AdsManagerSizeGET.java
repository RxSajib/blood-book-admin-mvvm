package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.blood.bloodbankadmin.Data.DataManager;

public class AdsManagerSizeGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private CollectionReference AdsManagerRef;
    private FirebaseAuth Mauth;

    public AdsManagerSizeGET(Application application){
        this.application = application;
        AdsManagerRef = FirebaseFirestore.getInstance().collection(DataManager.AdManager);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Integer> GetSizeOFAdManager(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            AdsManagerRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.size());
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
