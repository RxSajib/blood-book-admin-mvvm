package com.blood.bloodbankadmin.UI.ViewHolder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.LostandfounditemBinding;

public class LostAndFoundVH extends RecyclerView.ViewHolder {

    public LostandfounditemBinding binding;

    public LostAndFoundVH(@NonNull LostandfounditemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
