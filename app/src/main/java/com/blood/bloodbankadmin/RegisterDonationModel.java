package com.blood.bloodbankadmin;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import java.io.Serializable;

import lombok.Data;

@Data
public class RegisterDonationModel implements Serializable {

    private String BloodDonationDate, Country, District, HospitalName, Province, QuantityOfBlood,RegisterDonationBloodImage;
    private String SenderUID;
    private long Timestamp, DocumentKey;


    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
