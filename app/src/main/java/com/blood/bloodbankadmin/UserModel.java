package com.blood.bloodbankadmin;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserModel implements Serializable {

    private String Email, FatherName, FirstName, MiddleName, ProfileImage, Surname, Token, IpAddress, DocumentKey;
    private boolean IsActive;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
