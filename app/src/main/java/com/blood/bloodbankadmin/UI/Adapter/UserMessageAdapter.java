package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.NotificationModel;
import com.blood.bloodbankadmin.UI.ViewHolder.UserMessageVH;
import com.blood.bloodbankadmin.databinding.UsermessageitemBinding;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import lombok.Getter;
import lombok.Setter;

@Singleton
public class UserMessageAdapter extends RecyclerView.Adapter<UserMessageVH> {

    @Setter @Getter
    private List<NotificationModel> list;
    private OnClick OnClick;

    @Inject
    public UserMessageAdapter(){

    }

    @NonNull
    @Override
    public UserMessageVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = UsermessageitemBinding.inflate(l, parent, false);
        return new UserMessageVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserMessageVH holder, int position) {
        holder.binding.setNotification(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getDocumentKey());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }


    public interface OnClick{
        void Click(long DocumentKey);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
