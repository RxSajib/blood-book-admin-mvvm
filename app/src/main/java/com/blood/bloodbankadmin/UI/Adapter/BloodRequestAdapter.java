package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.BloodRequestModel;
import com.blood.bloodbankadmin.UI.ViewHolder.BloodRequestViewHolder;
import com.blood.bloodbankadmin.databinding.BloodrequestitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class BloodRequestAdapter extends RecyclerView.Adapter<BloodRequestViewHolder>{
    @Setter
    @Getter
    private List<BloodRequestModel> list;
    private OnClick OnClick;

    @Inject
    public BloodRequestAdapter(){}

    @NonNull
    @Override
    public BloodRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = BloodrequestitemBinding.inflate(l, parent, false);
        return new BloodRequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BloodRequestViewHolder holder, int position) {
        holder.binding.setBloodData(list.get(position));
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }


    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }


    public interface OnClick{
        void Click(BloodRequestModel bloodRequestModel);
    }
    public void OnClickEvent(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
