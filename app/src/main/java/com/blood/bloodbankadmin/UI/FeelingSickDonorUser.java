package com.blood.bloodbankadmin.UI;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.FeelingSickAdapter;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.FeelingsickdonoruserBinding;


public class FeelingSickDonorUser extends Fragment {

    private FeelingsickdonoruserBinding binding;
    private FeelingSickAdapter feelingSickAdapter = new FeelingSickAdapter();
    private ViewModel viewModel;
    private ProgressDialog progressDialog;

    public FeelingSickDonorUser() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.feelingsickdonoruser, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        progressDialog = new ProgressDialog();

        GetData();
        return binding.getRoot();
    }

    private void GetData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(feelingSickAdapter);

        viewModel.GetFeelingSickUser(DataManager.RegisterAsDonor).observe(getActivity(), donorModels -> {
            feelingSickAdapter.setList(donorModels);
            feelingSickAdapter.notifyDataSetChanged();

            if(donorModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });

        feelingSickAdapter.OnClickState((UID, val) -> {
            progressDialog.ProgressDialog(getActivity());
            viewModel.UpdateDonorUserShowToUser(UID, val).observe(getActivity(), aBoolean -> {
                if(aBoolean){
                    progressDialog.CancelProgressDialog();
                }else {
                    progressDialog.CancelProgressDialog();
                }
            });
        });
    }




}