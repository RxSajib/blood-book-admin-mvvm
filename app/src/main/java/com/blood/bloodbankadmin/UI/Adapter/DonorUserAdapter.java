package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.UI.ViewHolder.DonorViewHolder;
import com.blood.bloodbankadmin.databinding.DonoruseritemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class DonorUserAdapter extends RecyclerView.Adapter<DonorViewHolder> {
    @Getter @Setter
    private List<DonorModel> list;

    @Inject
    public DonorUserAdapter(){

    }

    @NonNull
    @Override
    public DonorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = DonoruseritemBinding.inflate(l, parent, false);
        return new DonorViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DonorViewHolder holder, int position) {
        holder.binding.setDonor(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
}
