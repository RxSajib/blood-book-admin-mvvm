package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.FeelingsickuseritemBinding;

public class FeelingSickViewHolder extends RecyclerView.ViewHolder {

    public FeelingsickuseritemBinding binding;

    public FeelingSickViewHolder(@NonNull FeelingsickuseritemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
