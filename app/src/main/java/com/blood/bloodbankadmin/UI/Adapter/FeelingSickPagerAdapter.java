package com.blood.bloodbankadmin.UI.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class FeelingSickPagerAdapter extends FragmentPagerAdapter {

    private final ArrayList<Fragment> list = new ArrayList<>();
    private final ArrayList<String> data = new ArrayList<>();

    public FeelingSickPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void AddFragement(Fragment fragment, String Title){
        list.add(fragment);
        data.add(Title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position);
    }
}
