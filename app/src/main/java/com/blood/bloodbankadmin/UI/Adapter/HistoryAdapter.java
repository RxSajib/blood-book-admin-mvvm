package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.RegisterDonationModel;
import com.blood.bloodbankadmin.UI.ViewHolder.HistoryViewHolder;
import com.blood.bloodbankadmin.databinding.HistoryitemBinding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {

    @Getter @Setter
    private List<RegisterDonationModel> list;
    private OnClick OnClick;
    private CollectionReference MuserRef;

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = HistoryitemBinding.inflate(l, parent, false);
        return new HistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        MuserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        holder.binding.setHistory(list.get(position));
        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });

        MuserRef.document(list.get(position).getSenderUID())
                .addSnapshotListener((value, error) -> {
                   if(error != null){
                       return;
                   }
                   if(value.exists()){
                       var photo = value.getString(DataManager.ProfileImage);
                       Picasso.get().load(photo).into(holder.binding.Image);
                   }
                });

    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }


    public interface OnClick{
        void Click(RegisterDonationModel registerDonationModel);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
