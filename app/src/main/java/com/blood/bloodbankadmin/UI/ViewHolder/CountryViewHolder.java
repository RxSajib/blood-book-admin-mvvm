package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.CountryitemBinding;

public class CountryViewHolder extends RecyclerView.ViewHolder {

    public CountryitemBinding binding;

    public CountryViewHolder(@NonNull CountryitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
