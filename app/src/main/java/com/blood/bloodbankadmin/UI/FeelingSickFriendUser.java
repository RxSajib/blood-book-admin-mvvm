package com.blood.bloodbankadmin.UI;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.FeelingSickAdapter;
import com.blood.bloodbankadmin.databinding.FeelingsickfrienduserBinding;

public class FeelingSickFriendUser extends Fragment {

    private FeelingsickfrienduserBinding binding;
    private ViewModel viewModel;
    private FeelingSickAdapter feelingSickAdapter = new FeelingSickAdapter();

    public FeelingSickFriendUser() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.feelingsickfrienduser, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        InitView();

        return binding.getRoot();
    }


    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(feelingSickAdapter);
        viewModel.GetFeelingSickUser(DataManager.RegisterAsFriend).observe(getActivity(), donorModels -> {
            feelingSickAdapter.setList(donorModels);
            feelingSickAdapter.notifyDataSetChanged();

            if(donorModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });

        feelingSickAdapter.OnClickState((UID, val) -> {

        });
    }
}