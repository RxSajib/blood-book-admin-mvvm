package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.BloodBankModel;
import com.blood.bloodbankadmin.UI.ViewHolder.BloodBankVH;
import com.blood.bloodbankadmin.databinding.HospitalitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class BloodBankAdapter extends RecyclerView.Adapter<BloodBankVH> {

    @Inject
    public BloodBankAdapter(){

    }

    @Setter @Getter
    private List<BloodBankModel> list;

    @NonNull
    @Override
    public BloodBankVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        var l = LayoutInflater.from(parent.getContext());
        var v = HospitalitemBinding.inflate(l, parent, false);
        return new BloodBankVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BloodBankVH holder, int position) {
        holder.binding.setBloodBank(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
}
