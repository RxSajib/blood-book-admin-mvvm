package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.databinding.UsernotificationmessageBinding;

public class UserNotificationMessage extends AppCompatActivity {

    private UsernotificationmessageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.usernotificationmessage);

        InitView();
    }

    private void InitView(){

    }
}