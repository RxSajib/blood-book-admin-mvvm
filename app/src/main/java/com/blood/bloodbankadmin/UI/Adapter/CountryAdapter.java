package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.CountryNameResponse;
import com.blood.bloodbankadmin.UI.ViewHolder.CountryViewHolder;
import com.blood.bloodbankadmin.databinding.CountryitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class CountryAdapter extends RecyclerView.Adapter<CountryViewHolder> {

    @Inject
    public CountryAdapter(){

    }

    @Setter
    @Getter
    private List<CountryNameResponse> list;
    private OnClick OnClick;

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = CountryitemBinding.inflate(l, parent, false);
        return new CountryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryViewHolder holder, int position) {
        holder.binding.setCountryFlag(list.get(position).getFlagsData());
        holder.binding.setCountryName(list.get(position).getCountryData());

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getCountryData().getCountryName(), list.get(position).getFlagsData().getPNG());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
       void Click(String Name, String Icon);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }

}
