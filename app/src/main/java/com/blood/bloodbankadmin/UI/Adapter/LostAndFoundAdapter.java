package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.LostAndFoundModel;
import com.blood.bloodbankadmin.UI.ViewHolder.LostAndFoundVH;
import com.blood.bloodbankadmin.databinding.LostandfounditemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class LostAndFoundAdapter extends RecyclerView.Adapter<LostAndFoundVH> {
    @Setter @Getter
    private List<LostAndFoundModel> list;
    private OnClick OnClick;
    private OnLongClick OnLongClick;

    @Inject
    public LostAndFoundAdapter(){

    }

    @NonNull
    @Override
    public LostAndFoundVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LostandfounditemBinding.inflate(l, parent, false);
        return new LostAndFoundVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LostAndFoundVH holder, int position) {
        holder.binding.setLostAndFound(list.get(position));

        holder.binding.CallIcon.setOnClickListener(view -> {
            OnClick.Call(list.get(position));
        });

        holder.itemView.setOnLongClickListener(view -> {
            OnLongClick.Click(list.get(position));
            return true;
        });

    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Call(LostAndFoundModel organizationModel);
    }
    public void OnCallLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }



    public interface OnLongClick{
        void Click(LostAndFoundModel lostAndFoundModel);
    }
    public void OnLongClickState(OnLongClick OnLongClick){
        this.OnLongClick = OnLongClick;
    }
}
