package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.UsermessageitemBinding;

public class UserMessageVH extends RecyclerView.ViewHolder {

    public UsermessageitemBinding binding;

    public UserMessageVH(@NonNull UsermessageitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
