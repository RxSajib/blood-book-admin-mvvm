package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.LostandfoundtypeitemBinding;

public class LostAndFoundTypeViewHolder extends RecyclerView.ViewHolder {

    public LostandfoundtypeitemBinding binding;

    public LostAndFoundTypeViewHolder(@NonNull LostandfoundtypeitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
