package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.databinding.AddsubadminBinding;

public class AddSubAdmin extends AppCompatActivity {

    private AddsubadminBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.addsubadmin);


        InitView();
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.AddSubAdmin));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AddSubAdmin.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AddSubAdmin.this);
    }
}