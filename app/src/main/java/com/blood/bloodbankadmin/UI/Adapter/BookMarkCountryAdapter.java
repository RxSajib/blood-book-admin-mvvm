package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.CountryModel;
import com.blood.bloodbankadmin.UI.ViewHolder.BookMarkCountryVH;
import com.blood.bloodbankadmin.databinding.CountrybookmarkitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class BookMarkCountryAdapter extends RecyclerView.Adapter<BookMarkCountryVH> {
    @Setter
    @Getter
    private List<CountryModel> list;
    private OnClick OnClick;

    @Inject
    public BookMarkCountryAdapter(){

    }

    @NonNull
    @Override
    public BookMarkCountryVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = CountrybookmarkitemBinding.inflate(l, parent, false);
        return new BookMarkCountryVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookMarkCountryVH holder, int position) {
        holder.binding.setCountry(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }


    public interface OnClick{
        void Click(CountryModel countryModel);
    }
    public void OnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
