package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.OrganizationModel;
import com.blood.bloodbankadmin.UI.ViewHolder.TeliphonyManagerVH;
import com.blood.bloodbankadmin.databinding.TeliphonedirectoryitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class TelephonyManagerAdapter extends RecyclerView.Adapter<TeliphonyManagerVH> {

    @Inject
    public TelephonyManagerAdapter(){
    }

    @Setter @Getter
    private List<OrganizationModel> list;
    private OnClick OnClick;
    private OnClickEvant OnClickEvant;

    @NonNull
    @Override
    public TeliphonyManagerVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = TeliphonedirectoryitemBinding.inflate(l, parent, false);
        return new TeliphonyManagerVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TeliphonyManagerVH holder, int position) {
        holder.binding.setOrganization(list.get(position));

        holder.binding.CallNowBtn.setOnClickListener(view -> {
            OnClick.Call(list.get(position));
        });

        holder.itemView.setOnClickListener(view -> {
            OnClickEvant.Click(list.get(position));
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Call(OrganizationModel organizationModel);
    }
    public void OnCallLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }


    public interface OnClickEvant{
        void Click(OrganizationModel organizationModel);
    }
    public void OnClickState(OnClickEvant OnClickEvant){
        this.OnClickEvant = OnClickEvant;
    }
}
