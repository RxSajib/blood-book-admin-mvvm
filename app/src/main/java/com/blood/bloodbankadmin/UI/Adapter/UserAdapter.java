package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.UI.ViewHolder.UserHolder;
import com.blood.bloodbankadmin.UserModel;
import com.blood.bloodbankadmin.databinding.UseritemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;

@Singleton
public class UserAdapter extends RecyclerView.Adapter<UserHolder> {
    @Setter @Getter
    private List<UserModel> list;
    private OnClick OnClick;
    private OnItemClick OnItemClick;


    @Inject
    public UserAdapter(){

    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = UseritemBinding.inflate(l, parent, false);
        return new UserHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.binding.setUser(list.get(position));

       holder.binding.CheckBox.setOnClickListener(view -> {
           if(holder.binding.CheckBox.isChecked()){
               OnClick.Click(list.get(position).getDocumentKey(), true);
           }else {
               OnClick.Click(list.get(position).getDocumentKey(), false);
           }
       });

       holder.itemView.setOnClickListener(view -> {
           OnItemClick.Click(list.get(position).getDocumentKey());
       });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }



    public interface OnClick{
        void Click(String DocumentID, boolean IsActive);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }



    public interface OnItemClick{
        void Click(String DocumentID);
    }
    public void OnItemClcikState(OnItemClick OnItemClick){
        this.OnItemClick = OnItemClick;
    }

}
