package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.UI.ViewHolder.LostAndFoundTypeVH;
import com.blood.bloodbankadmin.databinding.LostandfoundtypeBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class FilterLostAndFoundAdapter extends RecyclerView.Adapter<LostAndFoundTypeVH> {
    @Setter @Getter
    private List<LostAndFoundTypeModel> list;
    private OnClick OnClick;

    @Inject
    public FilterLostAndFoundAdapter(){

    }

    @NonNull
    @Override
    public LostAndFoundTypeVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LostandfoundtypeBinding.inflate(l, parent, false);
        return new LostAndFoundTypeVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LostAndFoundTypeVH holder, int position) {
        holder.binding.setLostAndFoundType(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getCategory());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String Category);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
