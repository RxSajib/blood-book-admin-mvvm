package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.OrganizationTypeAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.TelephonyManagerAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Permission;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.FilterOptionDialog;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.FilterdialogBinding;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;
import com.blood.bloodbankadmin.databinding.TeliphonymanagerBinding;
import com.blood.bloodbankadmin.databinding.UpdatedeletedialogBinding;

public class TeliphonyManager extends AppCompatActivity {

    private TeliphonymanagerBinding binding;
    private ViewModel viewModel;

    TelephonyManagerAdapter telephonyManagerAdapter;

    ProgressDialog progressDialog;

    FilterOptionDialog filterOptionDialog;

    LocationDataAdapter locationDataAdapter;

    ProvinceNameDataAdapter provinceNameDataAdapter;

    DistrictNameDataAdapter districtNameDataAdapter;

    OrganizationTypeAdapter organizationTypeAdapter;
    private LocationViewModel locationViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.teliphonymanager);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
      //  var component = DaggerComponenet.create();
      //  component.InjectTeliphonyManager(this);

        telephonyManagerAdapter = new TelephonyManagerAdapter();
        progressDialog = new ProgressDialog();
        filterOptionDialog =  new FilterOptionDialog();
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();
        organizationTypeAdapter = new OrganizationTypeAdapter();

        InitView();
        LoadData();
        filter();
        InitView();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        GetOrganizationType();
        SearchByOrgName();
    }


    private void SearchByOrgName() {
        binding.SearchInput.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {

                if (binding.SearchInput.getText().toString().trim().isEmpty()) {
                    Toast.SetMessage(getApplicationContext(), "Search by organization name empty");
                } else {
                    SearchData(binding.SearchInput.getText().toString().trim());
                }

                return true;
            }
            return false;
        });
    }

    private void SearchData(String OrgName) {
        viewModel.SearchOrganization(OrgName).observe(this, organizationModels -> {
            telephonyManagerAdapter.setList(organizationModels);
            telephonyManagerAdapter.notifyDataSetChanged();
            if (organizationModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });
    }

    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(TeliphonyManager.this);
        });

        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.TelephoneDirectory));
        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.GotoAddHospitalData(TeliphonyManager.this);
            Animatoo.animateSlideLeft(TeliphonyManager.this);
        });

        telephonyManagerAdapter.OnCallLisiner(organizationModel -> {
            if (Permission.PermissionCall(TeliphonyManager.this, 150)) {
                var intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + organizationModel.getPhoneNumber()));
                startActivity(intent);
            }
        });
    }

    private void filter() {
        filterOptionDialog.OnclickLisiner(Item -> {
            if (Item == 0) {
                LoadData();
            }
            if (Item == 1) {
                var alertdialog = new AlertDialog.Builder(TeliphonyManager.this);
                FilterdialogBinding v = DataBindingUtil.inflate(getLayoutInflater(), R.layout.filterdialog, null, false);
                alertdialog.setView(v.getRoot());

                var dialog = alertdialog.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                 DialogView(dialog, v);
                v.ClearBtn.setOnClickListener(view -> {
                    dialog.dismiss();
                });
            }
        });

        binding.SearchInput.setOnTouchListener((view, motionEvent) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (binding.SearchInput.getRight() - binding.SearchInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    filterOptionDialog.show(getSupportFragmentManager(), "filter");
                    return true;
                }
            }
            return false;
        });



        telephonyManagerAdapter.OnCallLisiner(organizationModel -> {
            if (Permission.PermissionCall(TeliphonyManager.this, 150)) {
                var intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + organizationModel.getPhoneNumber()));
                startActivity(intent);
            }
        });

    }

    private void LoadData(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(telephonyManagerAdapter);
        viewModel.GetOrganizationData().observe(this, organizationModels -> {
            telephonyManagerAdapter.setList(organizationModels);
            telephonyManagerAdapter.notifyDataSetChanged();
            if(organizationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });



       telephonyManagerAdapter.OnClickState(organizationModel -> {
            var alertdialog = new AlertDialog.Builder(TeliphonyManager.this);
            UpdatedeletedialogBinding u = DataBindingUtil.inflate(getLayoutInflater(), R.layout.updatedeletedialog, null, false);
            alertdialog.setView(u.getRoot());

            var dialog = alertdialog.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            u.UpdateBtn.setOnClickListener(view -> {
                HandleActivity.GotoUpdateTelephonyManager(TeliphonyManager.this, organizationModel);
                Animatoo.animateSlideLeft(TeliphonyManager.this);
                dialog.dismiss();
            });

            u.RemoveBtn.setOnClickListener(view -> {
                progressDialog.ProgressDialog(TeliphonyManager.this);
                viewModel.TeliphonyManagerDelete(organizationModel.getDocumentKey()).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        dialog.dismiss();
                    }else {
                        dialog.dismiss();
                        progressDialog.CancelProgressDialog();
                    }
                });
            });
        });
    }


    private void DialogView(AlertDialog dialog, FilterdialogBinding v) {

        v.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var mydialog = new android.app.AlertDialog.Builder(TeliphonyManager.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            mydialog.setView(locationitemBinding.getRoot());

            var alertdialog = mydialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();

            viewModel.GetCountryName().observe(TeliphonyManager.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);
                GetCountryName();

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                v.Country.setText(CountryName);
                v.District.setText(null);
                v.Province.setText(null);
                alertdialog.dismiss();
            });
        });

        v.SelectProvince.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(TeliphonyManager.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var provincedialog = new android.app.AlertDialog.Builder(TeliphonyManager.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                provincedialog.setView(locationitemBinding.getRoot());

                var alertdialog = provincedialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(TeliphonyManager.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    v.Province.setText(ProvinceName);
                    v.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        v.DistrictInput.setOnClickListener(view -> {
            var CountryName = v.Country.getText().toString().trim();
            var ProvincesName = v.Province.getText().toString().trim();

            if (CountryName == "") {
                Toast.SetMessage(TeliphonyManager.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(TeliphonyManager.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var districtdialog = new android.app.AlertDialog.Builder(TeliphonyManager.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                districtdialog.setView(locationitemBinding.getRoot());

                var alertdialog = districtdialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(TeliphonyManager.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    v.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });

        v.SelectOrgTypeInput.setOnClickListener(view -> {
            var districtdialog = new android.app.AlertDialog.Builder(TeliphonyManager.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            districtdialog.setView(locationitemBinding.getRoot());

            var alertdialog = districtdialog.create();
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(organizationTypeAdapter);
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertdialog.show();

            viewModel.GetOrganizationType().observe(this, organizationTypeModels -> {
                locationViewModel.InsertOrganizationTypeData(organizationTypeModels);
            });

            organizationTypeAdapter.OnClickListner(Name -> {
                v.OrgType.setText(Name);
                alertdialog.dismiss();
            });

            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var s = editable.toString();
                    if (s.isEmpty()) {
                        GetOrganizationType();
                    } else {
                        SearchOrganizationType(s);
                        Toast.SetMessage(getApplicationContext(), "ok");
                    }
                }
            });
        });


        v.ApplyBtn.setOnClickListener(view -> {
            var Country = v.Country.getText().toString().trim();
            var Province = v.Province.getText().toString().trim();
            var District = v.District.getText().toString().trim();
            var OrgType = v.OrgType.getText().toString().trim();

            if (Country == "") {
                Toast.SetMessage(getApplicationContext(), "Country empty");
            } else if (Province == "") {
                Toast.SetMessage(getApplicationContext(), "Province empty");
            } else if (District == "") {
                Toast.SetMessage(getApplicationContext(), "District empty");
            } else if (OrgType == "") {
                Toast.SetMessage(getApplicationContext(), "Organization Type empty");
            } else {
                FilterTelephonyManager(Country, Province, District, OrgType);
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(TeliphonyManager.this);
    }




    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(TeliphonyManager.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(TeliphonyManager.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(TeliphonyManager.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(TeliphonyManager.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(TeliphonyManager.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(TeliphonyManager.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetOrganizationType() {
        locationViewModel.GetOrganizationType().observe(this, organizationTypeModels -> {
            organizationTypeAdapter.setList(organizationTypeModels);
            organizationTypeAdapter.notifyDataSetChanged();
        });
    }

    private void SearchOrganizationType(String Name) {
        locationViewModel.SearchOrganizationType(Name).observe(this, organizationTypeModels -> {
            organizationTypeAdapter.setList(organizationTypeModels);
            organizationTypeAdapter.notifyDataSetChanged();
        });
    }


    private void FilterTelephonyManager(String CountryName, String Province, String District, String OrgType) {
        viewModel.FilterOrganizationData(CountryName, Province, District, OrgType).observe(this, organizationModels -> {
            telephonyManagerAdapter.setList(organizationModels);
            telephonyManagerAdapter.notifyDataSetChanged();
            if (organizationModels != null) {
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            } else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });
    }
}