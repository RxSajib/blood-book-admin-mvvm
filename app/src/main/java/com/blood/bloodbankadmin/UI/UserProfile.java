package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.databinding.UserprofileBinding;

public class UserProfile extends AppCompatActivity {

    private UserprofileBinding binding;
    private String ReceiverUID;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.userprofile);
        ReceiverUID = getIntent().getStringExtra(DataManager.ReceiverUID);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        InitView();
        GetDonorUserData();
    }

    private void GetDonorUserData(){
        viewModel.SingleDonorUser(ReceiverUID).observe(this, donorModel -> {
            if(donorModel != null){
                binding.setBloodDonor(donorModel);
            }
        });
    }
    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSwipeRight(UserProfile.this);
        });
        binding.Toolbar.ToolbarTitle.setText("Profile");
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(UserProfile.this);
    }
}