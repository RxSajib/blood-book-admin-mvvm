package com.blood.bloodbankadmin.UI.ViewHolder;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.CountrylocationitemBinding;

public class CountryLocationVH extends RecyclerView.ViewHolder {

    public CountrylocationitemBinding binding;

    public CountryLocationVH(@NonNull CountrylocationitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
