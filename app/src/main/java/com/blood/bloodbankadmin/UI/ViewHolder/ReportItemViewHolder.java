package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.ReportitemBinding;

public class ReportItemViewHolder extends RecyclerView.ViewHolder {

    public ReportitemBinding binding;

    public ReportItemViewHolder(@NonNull ReportitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
