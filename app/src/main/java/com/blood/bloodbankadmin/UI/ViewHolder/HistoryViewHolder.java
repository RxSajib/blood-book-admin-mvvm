package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.HistoryitemBinding;

public class HistoryViewHolder extends RecyclerView.ViewHolder {

    public HistoryitemBinding binding;

    public HistoryViewHolder(@NonNull HistoryitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
