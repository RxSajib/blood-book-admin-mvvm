package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.UI.ViewHolder.LocationViewHolder;
import com.blood.bloodbankadmin.databinding.LocationitemBinding;

import java.util.List;

import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public class FilterLostAndFoundTypeAdapter extends RecyclerView.Adapter<LocationViewHolder>{
    @Setter @Getter
    private List<LostAndFoundTypeModel> list;
    private OnClick OnClick;

    @Inject
    public FilterLostAndFoundTypeAdapter(){

    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = LocationitemBinding.inflate(l, parent, false);
        return new LocationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.binding.LocationName.setText(list.get(position).getCategory());

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getCategory());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(String CategoryName);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
