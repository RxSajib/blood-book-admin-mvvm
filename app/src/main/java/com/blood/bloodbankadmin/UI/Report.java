package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.ReportAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.ReportBinding;

public class Report extends AppCompatActivity {
    private ReportBinding binding;
    private ViewModel viewModel;
    private ReportAdapter reportAdapter = new ReportAdapter();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.report);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        progressDialog = new ProgressDialog();


        InitView();
        GetData();
    }

    private void GetData() {
        viewModel.GetAllReport().observe(this, reportModels -> {
            reportAdapter.setList(reportModels);
            reportAdapter.notifyDataSetChanged();
            if (reportModels != null) {
                binding.Message.setVisibility(View.GONE);
                binding.IconMessage.setVisibility(View.GONE);
            } else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.IconMessage.setVisibility(View.VISIBLE);
            }
        });


        reportAdapter.OnClickState((DocumentID, ReceiverID) -> {
            var dialog = new AlertDialog.Builder(Report.this);
            dialog.setTitle("Delete");
            dialog.setMessage("Are u sure delete?");
            dialog.setPositiveButton("Delete", (dialogInterface, i) -> {
                progressDialog.ProgressDialog(Report.this);
                viewModel.DeleteReport(String.valueOf(DocumentID)).observe(this, aBoolean -> {
                    if (aBoolean) {
                        progressDialog.CancelProgressDialog();
                    } else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            });
            dialog.setNegativeButton("Cancel", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });
            dialog.setNeutralButton("View Profile", (dialogInterface, i) -> {
                HandleActivity.GotoReportUserProfile(Report.this, ReceiverID);
                Animatoo.animateSlideLeft(Report.this);
            });

            var d = dialog.create();
            d.show();
        });
    }

    private void InitView() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(reportAdapter);
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(Report.this);
        });
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.Report));
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(Report.this);
    }
}