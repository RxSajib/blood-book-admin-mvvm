package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbankadmin.Application.Application;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Data.Model.Data;
import com.blood.bloodbankadmin.Data.Model.NotifactionResponse;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.UserMessageAdapter;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.BrothcastmessagedialogBinding;
import com.blood.bloodbankadmin.databinding.UserbroadcastmessageBinding;

import javax.inject.Inject;

public class UserBroadcastMessage extends AppCompatActivity {

    private UserbroadcastmessageBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;
    private String DocumentKey;
    @Inject UserMessageAdapter userMessageAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.userbroadcastmessage);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        DocumentKey = getIntent().getStringExtra(DataManager.DocumentKey);

        var component = Application.component;
        component.InjectUserBroadcastMessage(this);

        SendNotification();
        InitView();
        GetData();
    }

    private void SendNotification(){
        var data = new Data("body", "title", "", "");
        var response = new NotifactionResponse(data, "dILJwv_VQtmOvXp7oxuAlS:APA91bHsWE_L91mIwmz42xR3tedB5bbVQVV7BCSMLeXjVFkzs2vvpdM1UpEywpm0J9861pV5HArIGkOyyNJLvfznH5CPy");
        viewModel.SendNotificationToUser(response).observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {

            }
        });
    }

    private void GetData(){
        viewModel.GetUserBroadcastMessage(DocumentKey).observe(this, notificationModels -> {
            userMessageAdapter.setList(notificationModels);
            userMessageAdapter.notifyDataSetChanged();

            if(notificationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });

        userMessageAdapter.OnClickState(DocumentKey -> {
            var alertdialog = new AlertDialog.Builder(UserBroadcastMessage.this);
            alertdialog.setTitle("Delete");
            alertdialog.setMessage("Do you want to remove?");
            alertdialog.setPositiveButton("Yes", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                progressDialog.ProgressDialog(this);
                viewModel.DeleteBroadcastMessage(String.valueOf(DocumentKey)).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            });
            alertdialog.setNegativeButton("No", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });

            var d = alertdialog.create();
            d.show();
        });
    }


    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(userMessageAdapter);
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(UserBroadcastMessage.this);
        });
        binding.Toolbar.ToolbarTitle.setText("Message");

        binding.FloatingActionButton.setOnClickListener(view -> {
            var dialog = new AlertDialog.Builder(UserBroadcastMessage.this);
            var v = BrothcastmessagedialogBinding.inflate(getLayoutInflater(), null, false);
            dialog.setView(v.getRoot());

            var d = dialog.create();
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.show();

            v.SendBroadcastBtn.setOnClickListener(view1 -> {
                var Topic = v.TopicInput.getText().toString().trim();
                var Details = v.DetailsInput.getText().toString().trim();
                if(Topic.isEmpty()){
                    Toast.SetMessage(getApplicationContext(), "Topic is empty");
                }else if(Details.isEmpty()){
                    Toast.SetMessage(getApplicationContext(), "Details is empty");
                }else {
                    d.dismiss();
                    progressDialog.ProgressDialog(UserBroadcastMessage.this);
                    viewModel.SendNotification(Details, DataManager.UserBroadcastMessage, FirebaseAuth.getInstance().getCurrentUser().getUid(), null, Topic)
                            .observe(this, aBoolean -> {
                                if(aBoolean){
                                    progressDialog.CancelProgressDialog();
                                }else {
                                    progressDialog.CancelProgressDialog();
                                }
                            });
                }
            });
        });
    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(UserBroadcastMessage.this);
    }
}