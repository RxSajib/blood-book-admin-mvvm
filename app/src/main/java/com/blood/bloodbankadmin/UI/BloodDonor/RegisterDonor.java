package com.blood.bloodbankadmin.UI.BloodDonor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DonorItemAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.RegisterdonorBinding;


public class RegisterDonor extends Fragment {

    private RegisterdonorBinding binding;
    private ViewModel viewModel;
    private DonorItemAdapter donorItemAdapter;
    private ProgressDialog progressDialog;

    public RegisterDonor() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.registerdonor, container, false);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        donorItemAdapter = new DonorItemAdapter();
        progressDialog = new ProgressDialog();

        InitView();
        Getdata();

        return binding.getRoot();
    }

    private void Getdata() {
        viewModel.RegisterAsDonorGET().observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();

            if (donorModels != null) {
                InVisibleInfo();
            } else {
                VisibleInfo(R.drawable.ic_user, "No user found");
            }
        });
    }

    private void InitView() {
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(donorItemAdapter);

        donorItemAdapter.OnClickState(model -> {
            HandleActivity.GotoDetailsOFRegisterBloodDonor(getActivity(), model);
            Animatoo.animateSlideLeft(getActivity());
        });

        donorItemAdapter.OnDeleteState(DocumentKey -> {
            var dialog = new AlertDialog.Builder(getActivity());
            dialog.setTitle("Delete !");
            dialog.setMessage("Do you want to delete ?");
            dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    progressDialog.ProgressDialog(getActivity());
                    viewModel.DonorUserDelete(DocumentKey).observe(getActivity(), aBoolean -> {
                        if(aBoolean){
                            progressDialog.CancelProgressDialog();
                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                }
            });

            dialog.setNegativeButton("No", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });
            var d = dialog.create();
            d.show();
        });


        binding.SearchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (binding.SearchInput.getText().toString().trim().isEmpty()) {
                    Toast.SetMessage(getActivity(), "Number is empty");
                } else {
                    SearchByNumber(binding.SearchInput.getText().toString().trim());
                }
                return true;
            }
            return false;
        });

        binding.SearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var data = editable.toString();
                if (data.isEmpty()) {
                    Getdata();
                }
            }
        });
    }

    private void SearchByNumber(String Number) {
        viewModel.SearchDonorUserByNumber(DataManager.RegisterAsDonor, Number).observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();

            if (donorModels != null) {
                InVisibleInfo();
            } else {
                VisibleInfo(R.drawable.ic_search, "Not found anythings");
            }
        });
    }


    private void InVisibleInfo() {
        binding.Icon.setVisibility(View.GONE);
        binding.Message.setVisibility(View.GONE);
    }

    private void VisibleInfo(int ImageResID, String Message) {
        binding.Icon.setImageResource(ImageResID);
        binding.Message.setText(Message);

        binding.Message.setVisibility(View.VISIBLE);
        binding.Icon.setVisibility(View.VISIBLE);
    }

}