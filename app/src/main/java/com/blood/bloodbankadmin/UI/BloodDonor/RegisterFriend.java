package com.blood.bloodbankadmin.UI.BloodDonor;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DonorItemAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.databinding.RegisterfriendBinding;

public class RegisterFriend extends Fragment {

    private RegisterfriendBinding binding;
    private DonorItemAdapter donorItemAdapter;
    private ViewModel viewModel;

    public RegisterFriend() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.registerfriend, container, false);
        donorItemAdapter = new DonorItemAdapter();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        InitView();
        GetData();
        return binding.getRoot();
    }

    private void GetData(){
        viewModel.RegisterAsFriendGET().observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();
        });
    }
    private void InitView(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(donorItemAdapter);

        donorItemAdapter.OnClickState(model -> {
            HandleActivity.GotoRegisterAsFriend(getActivity(), model);
            Animatoo.animateSlideLeft(getActivity());
        });

        binding.SearchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if(binding.SearchInput.getText().toString().trim().isEmpty()){
                    Toast.SetMessage(getActivity(), "Number is empty");
                }else {
                    SearchByNumber(binding.SearchInput.getText().toString().trim());
                }
                return true;
            }
            return false;
        });


        binding.SearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var data = editable.toString();
                if(data.isEmpty()){
                    GetData();
                }
            }
        });

        binding.AddBtn.setOnClickListener(view -> {
            HandleActivity.GotoAddRegisterAsFriend(getActivity());
            Animatoo.animateSlideLeft(getActivity());
        });
    }

    private void SearchByNumber(String Number){
        viewModel.SearchDonorUserByNumber(DataManager.RegisterAsFriend, Number).observe(getActivity(), donorModels -> {
            donorItemAdapter.setList(donorModels);
            donorItemAdapter.notifyDataSetChanged();

            if(donorModels != null){
                InVisibleInfo();
            }else {
                VisibleInfo(R.drawable.ic_search, "Not found anythings");
            }
        });
    }

    private void InVisibleInfo(){
        binding.Icon.setVisibility(View.GONE);
        binding.Message.setVisibility(View.GONE);
    }

    private void VisibleInfo(int ImageResID, String Message){
        binding.Icon.setImageResource(ImageResID);
        binding.Message.setText(Message);

        binding.Message.setVisibility(View.VISIBLE);
        binding.Icon.setVisibility(View.VISIBLE);
    }
}