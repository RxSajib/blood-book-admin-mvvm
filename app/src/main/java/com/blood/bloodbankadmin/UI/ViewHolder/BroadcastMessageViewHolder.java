package com.blood.bloodbankadmin.UI.ViewHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.databinding.BrothcastmessageitemBinding;

public class BroadcastMessageViewHolder extends RecyclerView.ViewHolder {

    public BrothcastmessageitemBinding binding;

    public BroadcastMessageViewHolder(@NonNull BrothcastmessageitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
