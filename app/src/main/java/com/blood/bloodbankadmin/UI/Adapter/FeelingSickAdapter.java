package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.UI.ViewHolder.FeelingSickViewHolder;
import com.blood.bloodbankadmin.databinding.FeelingsickuseritemBinding;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class FeelingSickAdapter extends RecyclerView.Adapter<FeelingSickViewHolder> {

    @Setter
    @Getter
    private List<DonorModel> list;
    private FirebaseAuth Mauth;
    private CollectionReference UserRef;
    private OnClick OnClick;

    @NonNull
    @Override
    public FeelingSickViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
        var l = LayoutInflater.from(parent.getContext());
        var v = FeelingsickuseritemBinding.inflate(l, parent, false);
        return new FeelingSickViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FeelingSickViewHolder holder, int position) {
        holder.binding.setFeelingSick(list.get(position));

        UserRef.document(list.get(position).getSenderUID()).addSnapshotListener((value, error) -> {
            if (error != null) {
                return;
            }
            if (value.exists()) {
                var ImageUri = value.getString(DataManager.ProfileImage);
                if (ImageUri != null) {
                    Picasso.get().load(ImageUri).into(holder.binding.ProfileImage);
                }
            }
        });


        holder.binding.CheckBox.setOnClickListener(view -> {
            if(holder.binding.CheckBox.isChecked()){
                OnClick.Click(list.get(position).getSenderUID(), true);
            }else {
                OnClick.Click(list.get(position).getSenderUID(), false);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public interface OnClick{
        void Click(String UID, boolean val);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
