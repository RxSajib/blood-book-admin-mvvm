package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.os.Handler;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.databinding.SplashscreenBinding;

public class SplashScreen extends AppCompatActivity {

    private SplashscreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.splashscreen);

        new Handler().postDelayed(() -> {
            HandleActivity.GotoHome(SplashScreen.this);
            Animatoo.animateSlideLeft(SplashScreen.this);
            finish();
        }, 3000);
    }
}