package com.blood.bloodbankadmin.UI;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Utils.HideKeyBoard;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.SigninaccountBinding;

public class SignInAccount extends AppCompatActivity {

    private SigninaccountBinding binding;
    ProgressDialog progressDialog;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private ViewModel viewModel;
    private FirebaseAuth Mauth;
    private Boolean IsPasswordShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.signinaccount);

        viewModel = new ViewModelProvider(this).get(ViewModel.class);
    //    var component = DaggerComponenet.create();
     //   component.InjectSignIn(this);
        progressDialog = new ProgressDialog();
        Mauth = FirebaseAuth.getInstance();

        InitView();
        SignInAccount();
        creating_request();
        CheckItem();
    }


    private void CheckItem() {

        binding.ForgotPasswordBtn.setOnClickListener(view -> {
            HandleActivity.GotoForgotPassword(SignInAccount.this);
            Animatoo.animateSlideLeft(SignInAccount.this);
        });
        binding.EmailInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String email = binding.EmailInput.getText().toString().trim();

                if (!editable.toString().isEmpty()) {
                    if (email.matches(emailPattern) && editable.toString().length() > 0) {
                        binding.EmailInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_correct, 0);
                    } else {
                        binding.EmailInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_uncheck, 0);
                    }
                }
            }
        });

        binding.PasswordInput.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (binding.PasswordInput.getRight() - binding.PasswordInput.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    if (!IsPasswordShow) {
                        HideKeyBoard.hideKeyboard(SignInAccount.this);
                        binding.PasswordInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eyesoff, 0);
                        binding.PasswordInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        IsPasswordShow = true;
                    } else {
                        HideKeyBoard.hideKeyboard(SignInAccount.this);
                        binding.PasswordInput.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_eyes, 0);
                        binding.PasswordInput.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        IsPasswordShow = false;
                    }
                    return true;
                }
            }
            return false;
        });

    }

    private void InitView(){

        binding.ForgotPasswordBtn.setOnClickListener(view -> {
            HandleActivity.GotoForgotPassword(SignInAccount.this);
            Animatoo.animateSlideLeft(SignInAccount.this);
        });

        binding.GoogleSignIn.setOnClickListener(view -> {
            signIn();
        });
        binding.SignInButton.setOnClickListener(view -> {
            HandleActivity.GotoSignUp(SignInAccount.this);
            Animatoo.animateSlideLeft(SignInAccount.this);
        });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void SignInAccount(){
        binding.LoginButton.setOnClickListener(view -> {
            var Email = binding.EmailInput.getText().toString().trim();
            var Password = binding.PasswordInput.getText().toString().trim();
            binding.PasswordMessage.setVisibility(View.GONE);
            binding.EmailMessage.setVisibility(View.GONE);


            if(Email.isEmpty()){
                binding.EmailMessage.setVisibility(View.VISIBLE);
            }else if(Password.isEmpty()){
                binding.PasswordMessage.setVisibility(View.VISIBLE);
            }else {
                progressDialog.ProgressDialog(SignInAccount.this);
                viewModel.EmailPasswordSignInAccount(Email, Password).observe(this, aBoolean -> {
                    if(aBoolean){

                        viewModel.EmailPasswordSignInAccount(Email, Password).observe(SignInAccount.this, aBoolean1 -> {
                            if(aBoolean1){
                                SetUpProfileData(Email, null,  null, null);
                            }
                        });
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(SignInAccount.this);
    }

    private void creating_request() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(SignInAccount.this, gso);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            progressDialog.ProgressDialog(SignInAccount.this);
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                Toast.SetMessage(SignInAccount.this, e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {

        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        Mauth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String Email = task.getResult().getUser().getEmail();
                    String Name = task.getResult().getUser().getDisplayName();
                    String PhotoUri = task.getResult().getUser().getPhotoUrl().toString();
                    SetUpProfileData(Email, null,  Name, PhotoUri);
                } else {

                    Toast.SetMessage(SignInAccount.this, task.getException().getMessage());
                }
            }
        });
    }


    private void SetUpProfileData(String Email, String Password,  String Name, String PhotoUri){
        viewModel.SetAdminProfile(Email, Password, Name, PhotoUri).observe(SignInAccount.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    progressDialog.CancelProgressDialog();
                    HandleActivity.GotoHome(SignInAccount.this);
                    Animatoo.animateSlideLeft(SignInAccount.this);
                    finish();
                }else {
                    progressDialog.CancelProgressDialog();
                }
            }
        });
    }
}