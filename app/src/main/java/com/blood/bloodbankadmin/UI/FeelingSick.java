package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentPagerAdapter;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.FeelingSickPagerAdapter;
import com.blood.bloodbankadmin.databinding.FeelingsickBinding;

public class FeelingSick extends AppCompatActivity {

    private FeelingsickBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.feelingsick);

        InitView();
        SetFragment();
    }

    private void SetFragment(){
        binding.TabLayout.setupWithViewPager(binding.ViewPager);
        var fadapter = new FeelingSickPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        fadapter.AddFragement(new FeelingSickDonorUser(), getResources().getString(R.string.RegisterDonor));
        fadapter.AddFragement(new FeelingSickFriendUser(), getResources().getString(R.string.RegisterFriend));
        binding.ViewPager.setAdapter(fadapter);
    }

    private void InitView(){

        binding.Toolbar.ToolbarTitle.setText("Feeling Sick");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(FeelingSick.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(FeelingSick.this);
    }
}