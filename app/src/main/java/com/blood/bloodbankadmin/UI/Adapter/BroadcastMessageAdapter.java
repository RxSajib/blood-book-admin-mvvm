package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.NotificationModel;
import com.blood.bloodbankadmin.UI.ViewHolder.BroadcastMessageViewHolder;
import com.blood.bloodbankadmin.Utils.TimestampDayConverter;
import com.blood.bloodbankadmin.databinding.BrothcastmessageitemBinding;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.Getter;
import lombok.Setter;
@Singleton
public class BroadcastMessageAdapter extends RecyclerView.Adapter<BroadcastMessageViewHolder> {

    @Setter @Getter
    private List<NotificationModel> list;
    private OnClick OnClick;

    @Inject
    public BroadcastMessageAdapter(){

    }

    @NonNull
    @Override
    public BroadcastMessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = BrothcastmessageitemBinding.inflate(l, parent, false);
        return new BroadcastMessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BroadcastMessageViewHolder holder, int position) {
        holder.binding.setBroadcast(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getDocumentKey());
        });
        holder.binding.TimeDate.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }



    public interface OnClick{
        void Click(long DocumentKey);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
