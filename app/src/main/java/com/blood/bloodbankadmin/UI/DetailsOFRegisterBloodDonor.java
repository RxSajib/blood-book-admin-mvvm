package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Utils.BetweenTwoDate;
import com.blood.bloodbankadmin.Widget.DatePickerDialogFragment;
import com.blood.bloodbankadmin.Widget.TermsAndConditionDialog;
import com.nex3z.togglebuttongroup.button.CircularToggle;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.LocationViewModel;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.DistrictNameDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.LocationDataAdapter;
import com.blood.bloodbankadmin.UI.Adapter.ProvinceNameDataAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.DetailsofregisterblooddonorBinding;
import com.blood.bloodbankadmin.databinding.LocationdialogBinding;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

public class DetailsOFRegisterBloodDonor extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

 /*   private DetailsofregisterblooddonorBinding binding;

    private LocationViewModel locationViewModel;
    private LocationDataAdapter locationDataAdapter;
    private ProvinceNameDataAdapter provinceNameDataAdapter;
    private DistrictNameDataAdapter districtNameDataAdapter;
    private ViewModel viewModel;
    private String BloodGroup = null;
    private CircularToggle circularToggle;
    private ProgressDialog progressDialog;*/


    private String[] RegisterDonationDate = {"3 Months", "4 Months", "5 Months", "6 Months", "7 Months", "8 Months", "9 Months", "10 Months", "11 Months", "12 Months"};
    private String SelectRadioItem = null;
    TermsAndConditionDialog termsAndConditionDialog;
    ProgressDialog progressDialog;
    private String Month;
    private ViewModel viewModel;
    private int LastDonationTotalDay;
    private LocationDataAdapter locationDataAdapter;
    private ProvinceNameDataAdapter provinceNameDataAdapter;
    private LocationViewModel locationViewModel;
    private DistrictNameDataAdapter districtNameDataAdapter;
    private String BloodGroup = null;
    private CircularToggle circularToggle;
    private DetailsofregisterblooddonorBinding binding;
    private DonorModel donorModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.detailsofregisterblooddonor);


        termsAndConditionDialog = new TermsAndConditionDialog();
        termsAndConditionDialog.OnClickLisiner(() -> {

        });
        donorModel = (DonorModel) getIntent().getSerializableExtra(DataManager.Data);
        binding.setBloodDonor(donorModel);
        progressDialog = new ProgressDialog();
        //todo new
        viewModel = new ViewModelProvider(this).get(ViewModel.class);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();


        InitView();
        UploadRegisterDonor();
        SetLoactionData();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        //todo new



        BloodGroupSet();

        /*
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        locationDataAdapter = new LocationDataAdapter();
        provinceNameDataAdapter = new ProvinceNameDataAdapter();
        districtNameDataAdapter = new DistrictNameDataAdapter();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        SetToolbar();
        InitView();
        UpdateData();
        SetLocation();
        GetCountryName();
        GetProvincesName();
        GetDistrictName();
        BloodGroupSet();
        */
    }

    private void BloodGroupSet(){
        if(donorModel.getBloodGroup().equals(DataManager.APlus)){
            binding.APlus.setChecked(true);
            BloodGroup = DataManager.APlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.AMinus)){
            binding.AMinus.setChecked(true);
            BloodGroup  = DataManager.AMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BPlus)){
            binding.BPlus.setChecked(true);
            BloodGroup = DataManager.BPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BMinus)){
            binding.BMinus.setChecked(true);
            BloodGroup = DataManager.BMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABPlus)){
            binding.ABPlus.setChecked(true);
            BloodGroup = DataManager.ABPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABMinus)){
            binding.ABMinus.setChecked(true);
            BloodGroup = DataManager.ABMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OPlus)){
            binding.OPlus.setChecked(true);
            BloodGroup = DataManager.OPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OMinus)){
            binding.OMinus.setChecked(true);
            BloodGroup = DataManager.OMinus;
        }
    }


    /*
    private void BloodGroupSet(){
        if(donorModel.getBloodGroup().equals(DataManager.APlus)){
            binding.APlus.setChecked(true);
            BloodGroup = DataManager.APlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.AMinus)){
            binding.AMinus.setChecked(true);
            BloodGroup  = DataManager.AMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BPlus)){
            binding.BPlus.setChecked(true);
            BloodGroup = DataManager.BPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.BMinus)){
            binding.BMinus.setChecked(true);
            BloodGroup = DataManager.BMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABPlus)){
            binding.ABPlus.setChecked(true);
            BloodGroup = DataManager.ABPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.ABMinus)){
            binding.ABMinus.setChecked(true);
            BloodGroup = DataManager.ABMinus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OPlus)){
            binding.OPlus.setChecked(true);
            BloodGroup = DataManager.OPlus;
        }
        if(donorModel.getBloodGroup().equals(DataManager.OMinus)){
            binding.OMinus.setChecked(true);
            BloodGroup = DataManager.OMinus;
        }
    }


    private void SetLocation() {

        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
                locationViewModel.InsertCounyryNameData(countryNameModels);

            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(DetailsOFRegisterBloodDonor.this, provinceNameModels -> {
                    locationViewModel.InsertProvincesNameData(provinceNameModels);
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
                    locationViewModel.InsertDistrict(districtNameModelList);
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });
    }

    private void UpdateData() {

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });
        binding.UploadBtn.setOnClickListener(view -> {
            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameInput.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();
            var PhoneNuber = binding.MainNumberInput.getText().toString().trim();
            var RoshanNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();


            if (Integer.valueOf(Age) < 17) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Weight must be at least 55");
            }else {
                progressDialog.ProgressDialog(DetailsOFRegisterBloodDonor.this);
                viewModel.UpdateRegisterAsDonor(donorModel.getSenderUID(), GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName, PhoneNuber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress)
                        .observe(this, aBoolean -> {
                            if(aBoolean){
                                progressDialog.CancelProgressDialog();
                                finish();
                                Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
                            }else {
                                progressDialog.CancelProgressDialog();
                            }
                        });
            }

        });
    }

    private void InitView() {
        binding.EditBtn.setOnClickListener(view -> {
            var intent = new Intent(this, EditregisterDonationDate.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });

        binding.DonationHistoryBtn.setOnClickListener(view -> {
            HandleActivity.GotoLastDonationHistory(this, donorModel.getSenderUID());
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });
    }

    private void SetToolbar() {
        binding.Toolbar.ToolbarTitle.setText("Edit/Delete");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
    }


    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }


     */
    //todo get all location name


    //todo new
    private void SetLoactionData() {
        binding.CountryInput.setOnClickListener(view -> {
            locationViewModel.DeleteAllCountryNameData();
            var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
            LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
            dialog.setView(locationitemBinding.getRoot());

            var alertdialog = dialog.create();
            alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
            locationitemBinding.LocationRecyclerView.setAdapter(locationDataAdapter);

            alertdialog.show();
            viewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
                if(countryNameModels != null){
                    locationViewModel.InsertCounyryNameData(countryNameModels);
                }
            });
            locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    var CountryName = editable.toString();
                    if (CountryName.isEmpty()) {
                        GetCountryName();
                    } else {
                        GetCountryNameSearch(CountryName);
                    }
                }
            });

            locationDataAdapter.OnCountryLisiner(CountryName -> {
                binding.Country.setText(CountryName);
                alertdialog.dismiss();
            });
        });

        binding.ProvinceNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Please select your country name first");
            } else {
                locationViewModel.DeleteAllProvinceNameData();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(provinceNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();

                viewModel.GetProvinceName(CountryName).observe(DetailsOFRegisterBloodDonor.this, provinceNameModels -> {
                    if(provinceNameModels != null){
                        locationViewModel.InsertProvincesNameData(provinceNameModels);
                    }
                });

                provinceNameDataAdapter.OnClickLisiner(ProvinceName -> {
                    alertdialog.dismiss();
                    binding.Province.setText(ProvinceName);
                    binding.District.setText(null);
                });
                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetProvincesName();
                        } else {
                            SearchProvinceName(Data);
                        }
                    }
                });
            }
        });

        binding.DistrictNameInput.setOnClickListener(view -> {
            var CountryName = binding.Country.getText().toString().trim();
            var ProvincesName = binding.Province.getText().toString().trim();
            if (CountryName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Country Name");
            } else if (ProvincesName == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Select Province Name");
            } else {
                locationViewModel.DeleteDistrct();
                var dialog = new AlertDialog.Builder(DetailsOFRegisterBloodDonor.this);
                LocationdialogBinding locationitemBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.locationdialog, null, false);
                dialog.setView(locationitemBinding.getRoot());

                var alertdialog = dialog.create();
                locationitemBinding.LocationRecyclerView.setHasFixedSize(true);
                locationitemBinding.LocationRecyclerView.setAdapter(districtNameDataAdapter);
                alertdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertdialog.show();


                viewModel.GetDistrictName(CountryName, ProvincesName).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
                    if(districtNameModelList != null){
                        locationViewModel.InsertDistrict(districtNameModelList);
                    }
                });

                districtNameDataAdapter.OnClickLisiner(DictrictName -> {
                    alertdialog.dismiss();
                    binding.District.setText(DictrictName);
                });

                locationitemBinding.SearchInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        var Data = editable.toString();
                        if (Data.isEmpty()) {
                            GetDistrictName();
                        } else {
                            SearchDustrictName(Data);
                        }
                    }
                });
            }
        });

    }

    //todo get all location name
    private void GetCountryNameSearch(String CountryName) {
        locationViewModel.SearchByCountryName(CountryName).observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetCountryName() {
        locationViewModel.GetCountryName().observe(DetailsOFRegisterBloodDonor.this, countryNameModels -> {
            locationDataAdapter.setList(countryNameModels);
            locationDataAdapter.notifyDataSetChanged();
        });
    }


    private void GetProvincesName() {
        locationViewModel.GetProvincesNameData().observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchProvinceName(String Name) {
        locationViewModel.SearchProvincesName(Name).observe(DetailsOFRegisterBloodDonor.this, provinceNameModelList -> {
            provinceNameDataAdapter.setList(provinceNameModelList);
            provinceNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void GetDistrictName() {
        locationViewModel.GetDistrictName().observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }

    private void SearchDustrictName(String Name) {
        locationViewModel.SearchDistrictName(Name).observe(DetailsOFRegisterBloodDonor.this, districtNameModelList -> {
            districtNameDataAdapter.setList(districtNameModelList);
            districtNameDataAdapter.notifyDataSetChanged();
        });
    }
    //todo get all location name


    private void DisableDate() {
        binding.LastDonationDateInput.setVisibility(View.GONE);
    }

    private void EnableDate() {
        binding.LastDonationDateInput.setVisibility(View.VISIBLE);
    }

    private void InitView() {
        binding.Toolbar.ToolbarTitle.setText("Details of donor");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
        });
        binding.DonationHistoryBtn.setOnClickListener(view -> {
            HandleActivity.GotoLastDonationHistory(this, donorModel.getSenderUID());
            Animatoo.animateSlideLeft(DetailsOFRegisterBloodDonor.this);
        });

        binding.BloodGroup.setOnCheckedChangeListener((group, checkedId) -> {
            circularToggle = findViewById(checkedId);
            BloodGroup = circularToggle.getText().toString();
        });

        binding.Never.setChecked(true);
        DisableDate();
        binding.Never.setOnClickListener(view -> {
            DisableDate();
        });
        binding.SelectDate.setOnClickListener(view -> {
            EnableDate();
        });


        var AdapterRegisterDonate = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnerlayout, R.id.SpinnerTitle, RegisterDonationDate);
        binding.HowManyMonthSpinner.setAdapter(AdapterRegisterDonate);
        binding.HowManyMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                var Name = adapterView.getItemAtPosition(i).toString();
                Month = Name.replaceAll("[^0-9]", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.LastDonationDateInput.setKeyListener(null);

        binding.LastDonationDateInput.setOnClickListener(view -> {
            var dialog = new DatePickerDialogFragment();
            dialog.show(getSupportFragmentManager(), "sdsaasd");
        });

    }

    private void UploadRegisterDonor() {
        binding.CheckBox.setOnClickListener(view1 -> {
            if (binding.CheckBox.isChecked()) {
                termsAndConditionDialog.Show(DetailsOFRegisterBloodDonor.this);
            }
        });

        binding.UploadBtn.setOnClickListener(view -> {

            var sdf = new SimpleDateFormat(DataManager.DatePattern);
            long timestamp = System.currentTimeMillis();
            var calender = Calendar.getInstance(Locale.ENGLISH);
            calender.setTimeInMillis(timestamp);
            var currentdate = DateFormat.format(DataManager.DatePattern, calender).toString();


            RadioButton DateRadioButton;
            var RadioButtonID = binding.RadioGroup.getCheckedRadioButtonId();
            if (RadioButtonID > 0) {
                DateRadioButton = findViewById(RadioButtonID);
                SelectRadioItem = DateRadioButton.getText().toString();
            }


            var GivenName = binding.GivenNamesInput.getText().toString().trim();
            var SureName = binding.SurnameText.getText().toString().trim();
            var FatherName = binding.FatherNameInput.getText().toString().trim();
            var Age = binding.AgeInput.getText().toString().trim();
            var Weight = binding.WeightInput.getText().toString().trim();
            var Country = binding.Country.getText().toString().trim();
            var Province = binding.Province.getText().toString().trim();
            var District = binding.District.getText().toString().trim();
            var AreaName = binding.AreaNameInput.getText().toString().trim();


            var MainPhonenumber = binding.MainNumberInput.getText().toString().trim();
            var RoshanPhoneNumber = binding.RoshanNumberInput.getText().toString().trim();
            var EtisalatPhoneNumber = binding.EtisalatNumberInput.getText().toString().trim();
            var AWCCPhoneNumber = binding.AWCCNumberInput.getText().toString().trim();
            var MTNPhoneNumber = binding.MTNNumberInput.getText().toString().trim();
            var WhatsappNumber = binding.WhatsappNumberInput.getText().toString().trim();
            var EmailAddress = binding.EmailAddressInput.getText().toString().trim();

            if (GivenName.isEmpty()) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Given name is empty");
            } else if (SureName.isEmpty()) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Sure name is empty");
            } else if (FatherName.isEmpty()) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Father name is empty");
            } else if (BloodGroup == null) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Blood group is empty");
            } else if (Age.isEmpty()) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Age is empty");
            } else if (Weight.isEmpty()) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Weight is empty");
            } else if (Country == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Country is empty");
            } else if (Province == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Province is empty");
            } else if (District == "") {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "District is empty");
            } else if (AreaName.isEmpty()) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Area name is empty");
            } else if (Integer.valueOf(Age) < 17) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Age must be at least 18 years");
            } else if (Integer.valueOf(Weight) < 54) {
                Toast.SetMessage(DetailsOFRegisterBloodDonor.this, "Weight must be at least 55");
            } else {

                if (binding.CheckBox.isChecked()) {
                    if (SelectRadioItem.equals(getResources().getString(R.string.never))) {
                        UploadData(GivenName, SureName, FatherName, BloodGroup, Age, Weight, Country, Province, District, AreaName,
                                MainPhonenumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                                EmailAddress, Month, null, false, null);
                    }
                    if (SelectRadioItem.equals(getResources().getString(R.string.select_a_date))) {
                        var LastDonationDate = binding.LastDonationDateInput.getText().toString().trim();
                        if (LastDonationDate.isEmpty()) {
                            Toast.SetMessage(getApplicationContext(), "Select your last donation date");
                        } else {
                            try {
                                var fromdate = sdf.parse(currentdate);
                                var lastdate = sdf.parse(LastDonationDate);
                                if (fromdate.compareTo(lastdate) < 0) {
                                    Toast.SetMessage(getApplicationContext(), "error your blood donation date is wrong");
                                } else if (fromdate.compareTo(lastdate) == 0) {
                                    Toast.SetMessage(getApplicationContext(), "error your blood donation date and today date was same");
                                } else {
                                    LastDonationTotalDay = BetweenTwoDate.daysBetweenDates(currentdate, LastDonationDate);
                                    UploadData(GivenName, SureName, FatherName, "A+", Age, Weight, Country, Province, District, AreaName,
                                            MainPhonenumber, RoshanPhoneNumber, EtisalatPhoneNumber, AWCCPhoneNumber, MTNPhoneNumber, WhatsappNumber,
                                            EmailAddress, Month, LastDonationDate, false, String.valueOf(LastDonationTotalDay));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                } else {
                    Toast.SetMessage(getApplicationContext(), "you must agree our terms and condition first");
                }

            }


        });
    }

    private void UploadData(String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                            String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                            String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                            String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount) {

        progressDialog.ProgressDialog(DetailsOFRegisterBloodDonor.this);

        //todo update it

        viewModel.UpdateRegisterAsDonor(donorModel.getSenderUID(), GivenName, SureName, FatherName, BloodGroup, Age, Width, Country, Province, District, AreaName, MainPhoneNumber, RoshanNumber, EtisalatNumber, AWCCNumber, MTNNumber, WhatsappNumber, EmailAddress, DonateMonth, LastDonationDate, LastDonationDayCount)
                        .observe(this, aBoolean -> {
                            if (aBoolean) {
                                progressDialog.CancelProgressDialog();
                                finish();
                                Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
                                Toast.SetMessage(getApplicationContext(), "Update success");
                            } else {
                                progressDialog.CancelProgressDialog();
                            }
                        });

    }


    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(DetailsOFRegisterBloodDonor.this);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        var format = new DecimalFormat("00");
        var day = format.format(Double.valueOf(i2));
        var month = format.format(Double.valueOf(i1));
        binding.LastDonationDateInput.setText(i + "-" + month + "-" + day);
    }
    //todo new
}