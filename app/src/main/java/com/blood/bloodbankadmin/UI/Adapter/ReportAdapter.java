package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.Utils.Toast;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.ReportModel;
import com.blood.bloodbankadmin.UI.ViewHolder.ReportItemViewHolder;
import com.blood.bloodbankadmin.Utils.TimestampDayConverter;
import com.blood.bloodbankadmin.databinding.ReportitemBinding;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ReportAdapter extends RecyclerView.Adapter<ReportItemViewHolder> {

    @Setter @Getter
    private List<ReportModel> list;
    private CollectionReference UserRef;
    private OnClick OnClick;

    @NonNull
    @Override
    public ReportItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        var l = LayoutInflater.from(parent.getContext());
        var v = ReportitemBinding.inflate(l, parent, false);
        return new ReportItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportItemViewHolder holder, int position) {
        holder.binding.setReport(list.get(position));
        holder.binding.TimeDate.setText(TimestampDayConverter.getMyPrettyDate(list.get(position).getTimestamp()));

        UserRef.document(list.get(position).getReceiverUID()).addSnapshotListener((value, error) -> {
           if(error != null){
               return;
           }
           if(value.exists()){
               var image = value.getString(DataManager.ProfileImage).toString();
               Picasso.get().load(image).into(holder.binding.ImageOFProfile);
               var ipaddress = value.getString(DataManager.IpAddress).toString();
               holder.binding.IpAddress.setText(ipaddress);
               holder.binding.ReportUserName.setText(value.getString(DataManager.FirstName));
           }
        });

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position).getDocumentKey(), list.get(position).getReceiverUID());
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click(long DocumentID, String ReceiverUID);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
