package com.blood.bloodbankadmin.UI;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Application.Application;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.Permission;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.VideoadsBinding;

import javax.inject.Inject;

public class VideoAds extends AppCompatActivity {

    private VideoadsBinding binding;
    private ActivityResultLauncher<Intent> launcher;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.videoads);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        Application.component.InjectVideoAds(this);

        GetVideoFromGallery();
        SetVideo();
        InitView();

    }


    private void GetVideoFromGallery(){
        launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            progressBar.ProgressDialog(VideoAds.this);
            if(result.getResultCode() == RESULT_OK){
                viewModel.UploadVideoFileAds(result.getData().getData()).observe(this, s -> {
                    if(s != null){
                        viewModel.UploadVideoAds(s).observe(VideoAds.this, aBoolean -> {
                            if(aBoolean){
                                progressBar.CancelProgressDialog();
                            }else {
                                progressBar.CancelProgressDialog();
                            }
                        });
                    }
                });
            }else {
                progressBar.CancelProgressDialog();
            };
        });
    }

    private void SetVideo(){
        var uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.sample);
        binding.VideoView.setVideoURI(uri);
        binding.VideoView.start();


        var mediacontroler = new MediaController(this);
        binding.VideoView.setMediaController(mediacontroler);
        mediacontroler.setAnchorView(binding.VideoView);
    }


    private void InitView(){

        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(VideoAds.this);
        });
        binding.Toolbar.ToolbarTitle.setText("Manage videoAds");

        binding.AddVideo.setOnClickListener(view -> {
            if(Permission.ReadExternalMemoryPermission(this, 12)){
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("video/mp4");
                launcher.launch(intent);
            }
        });

        binding.RemoveVideo.setOnClickListener(view -> {
            var alertdialog = new AlertDialog.Builder(VideoAds.this);
            alertdialog.setTitle("Delete");
            alertdialog.setMessage("Do you want to remove video?");
            alertdialog.setPositiveButton("Yes", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                progressBar.ProgressDialog(VideoAds.this);
                viewModel.VideoAdsRemove().observe(this, aBoolean -> {
                    if(aBoolean){
                        progressBar.CancelProgressDialog();
                    }else {
                        progressBar.CancelProgressDialog();
                    }
                });
            });

            alertdialog.setNegativeButton("No", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });

            var d = alertdialog.create();
            d.show();
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(VideoAds.this);
    }
}