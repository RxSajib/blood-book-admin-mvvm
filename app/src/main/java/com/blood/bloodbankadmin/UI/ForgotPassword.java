package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.ForgotpasswordBinding;

public class ForgotPassword extends AppCompatActivity {

    private ForgotpasswordBinding binding;
    private ProgressDialog progressDialog;
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.forgotpassword);
        progressDialog = new ProgressDialog();
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        InitView();
        ResetPassword();
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText("Reset Password");
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(ForgotPassword.this);
        });
    }
    private void ResetPassword(){
        binding.ContinueBtn.setOnClickListener(view -> {
            var EmailAddress = binding.EmailInput.getText().toString().trim();
            if(EmailAddress.isEmpty()){
                Toast.SetMessage(ForgotPassword.this, "Email address empty");
            }else {
                progressDialog.ProgressDialog(ForgotPassword.this);
                viewModel.ResetPassword(EmailAddress).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                        finish();
                        Animatoo.animateSlideRight(ForgotPassword.this);
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(ForgotPassword.this);
    }
}