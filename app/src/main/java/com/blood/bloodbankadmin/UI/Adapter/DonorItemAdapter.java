package com.blood.bloodbankadmin.UI.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Data.SharePref;
import com.blood.bloodbankadmin.DonorModel;
import com.blood.bloodbankadmin.UI.ViewHolder.DonorItemViewHolder;
import com.blood.bloodbankadmin.databinding.DonoritemBinding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class DonorItemAdapter extends RecyclerView.Adapter<DonorItemViewHolder> {
    @Setter @Getter
    private List<DonorModel> list;
    private OnClick OnClick;
    private CollectionReference MUserRef;
    private FirebaseAuth Mauth;
    private SharePref sharePref;
    private OnRemove OnRemove;

    @NonNull
    @Override
    public DonorItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        sharePref = new SharePref(parent.getContext());
        Mauth = FirebaseAuth.getInstance();
        MUserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        var l = LayoutInflater.from(parent.getContext());
        var v = DonoritemBinding.inflate(l, parent, false);
        return new DonorItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DonorItemViewHolder holder, int position) {
        holder.binding.setDonor(list.get(position));

        holder.itemView.setOnClickListener(view -> {
            OnClick.Click(list.get(position));
            sharePref.SetData(DataManager.RegisterDonorUID, list.get(position).getSenderUID());
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                OnRemove.Remove(list.get(position).getDocumentKey());
                return false;
            }
        });

        MUserRef.document(list.get(position).getSenderUID()).addSnapshotListener((value, error) -> {
           if(error != null){

           }
           if(value.exists()){
               var image = value.getString(DataManager.ProfileImage);
               Picasso.get().load(image).into(holder.binding.ProfileImage);
           }
        });
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }

    public interface OnClick{
        void Click( DonorModel model);
    }
    public void OnClickState(OnClick OnClick){
        this.OnClick = OnClick;
    }



    //todo remove donor
    public interface OnRemove{
        void Remove(String DocumentKey);
    }
    public void OnDeleteState(OnRemove OnRemove){
        this.OnRemove = OnRemove;
    }
    //todo remove donor
}
