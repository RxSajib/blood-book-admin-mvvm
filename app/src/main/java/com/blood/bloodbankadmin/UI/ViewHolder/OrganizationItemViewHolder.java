package com.blood.bloodbankadmin.UI.ViewHolder;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.blood.bloodbankadmin.databinding.OrganizationitemBinding;

public class OrganizationItemViewHolder extends RecyclerView.ViewHolder {

    public OrganizationitemBinding binding;

    public OrganizationItemViewHolder(@NonNull OrganizationitemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
