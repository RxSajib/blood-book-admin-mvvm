package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.UserAdapter;
import com.blood.bloodbankadmin.Utils.HandleActivity;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.AppusersBinding;

import javax.inject.Inject;

public class    AppUsers extends AppCompatActivity {

    private AppusersBinding binding;
    private ViewModel viewModel;
    @Inject
    UserAdapter userAdapter;
    @Inject
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.appusers);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);


        var component = DaggerComponenet.create();
        component.InjectUser(this);

        InitView();
        LoadUser();
        SearchByName();
    }

    private void SearchByName() {
        binding.SearchInput.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {

                if (binding.SearchInput.getText().toString().trim().isEmpty()) {
                    Toast.SetMessage(getApplicationContext(), "Search by name is empty");
                } else {
                    Searching(binding.SearchInput.getText().toString().trim());
                }

                return true;
            }
            return false;
        });
    }

    private void Searching(String Name){
        viewModel.SearchUserByName(Name).observe(this, userModels -> {
            userAdapter.setList(userModels);
            userAdapter.notifyDataSetChanged();

            if(userModels != null){
                binding.Message.setVisibility(View.GONE);
                binding.Icon.setVisibility(View.GONE);
            }else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });


    }

    private void LoadUser(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(userAdapter);
        viewModel.GetUser().observe(this, userModels -> {
            userAdapter.setList(userModels);
            userAdapter.notifyDataSetChanged();

            if(userModels != null){
                binding.Message.setVisibility(View.GONE);
                binding.Icon.setVisibility(View.GONE);
            }else {
                binding.Message.setVisibility(View.VISIBLE);
                binding.Icon.setVisibility(View.VISIBLE);
            }
        });

        userAdapter.OnItemClcikState(DocumentID -> {
            HandleActivity.GotoUserBrothcastMessage(getApplicationContext(), DocumentID);
            Animatoo.animateSlideLeft(AppUsers.this);
        });
    }

    private void InitView(){
        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.AppUsers));
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(AppUsers.this);
        });

        binding.SearchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                var name = editable.toString();
                if(name.isEmpty()){
                    LoadUser();
                }
            }
        });

        userAdapter.OnClickState((DocumentID, IsActive) -> {
            progressDialog.ProgressDialog(this);
            viewModel.UserActiveUpdate(IsActive, DocumentID).observe(this, aBoolean -> {
                if(aBoolean){
                    viewModel.DonorUserUpdate(DocumentID, IsActive).observe(this, aBoolean1 -> {
                        if(aBoolean1){
                            progressDialog.CancelProgressDialog();
                        }else {
                            progressDialog.CancelProgressDialog();
                        }
                    });
                }else {
                    progressDialog.CancelProgressDialog();
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(AppUsers.this);
    }
}