package com.blood.bloodbankadmin.UI;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.blood.bloodbankadmin.Application.Application;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Network.AndroidViewModel.ViewModel;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.UI.Adapter.BroadcastMessageAdapter;
import com.blood.bloodbankadmin.Utils.Toast;
import com.blood.bloodbankadmin.Widget.ProgressDialog;
import com.blood.bloodbankadmin.databinding.BrothcastmessageBinding;
import com.blood.bloodbankadmin.databinding.BrothcastmessagedialogBinding;

import javax.inject.Inject;

public class BrothcastMessage extends AppCompatActivity {

    private BrothcastmessageBinding binding;
    private ViewModel viewModel;
    @Inject
    ProgressDialog progressDialog;
    @Inject
    BroadcastMessageAdapter broadcastMessageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.brothcastmessage);
        viewModel = new ViewModelProvider(this).get(ViewModel.class);

        var component = Application.component;
        component.InjectBrothcastMessage(this);


        InitView();
        GetBroadcastMessage();
    }


    private void GetBroadcastMessage(){
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setAdapter(broadcastMessageAdapter);
        viewModel.BroadcastMessageGET().observe(this, notificationModels -> {
            broadcastMessageAdapter.setList(notificationModels);
            broadcastMessageAdapter.notifyDataSetChanged();

            if(notificationModels != null){
                binding.Icon.setVisibility(View.GONE);
                binding.Message.setVisibility(View.GONE);
            }else {
                binding.Icon.setVisibility(View.VISIBLE);
                binding.Message.setVisibility(View.VISIBLE);
            }
        });

        broadcastMessageAdapter.OnClickState(DocumentKey -> {

            var alertdialog = new AlertDialog.Builder(BrothcastMessage.this);
            alertdialog.setTitle("Delete");
            alertdialog.setMessage("Do you want to remove?");
            alertdialog.setPositiveButton("Yes", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                progressDialog.ProgressDialog(this);
                viewModel.DeleteBroadcastMessage(String.valueOf(DocumentKey)).observe(this, aBoolean -> {
                    if(aBoolean){
                        progressDialog.CancelProgressDialog();
                    }else {
                        progressDialog.CancelProgressDialog();
                    }
                });
            });
            alertdialog.setNegativeButton("No", (dialogInterface, i) -> {
                dialogInterface.dismiss();
            });

            var d = alertdialog.create();
            d.show();
        });
    }
    private void InitView(){
        binding.Toolbar.BackButton.setOnClickListener(view -> {
            finish();
            Animatoo.animateSlideRight(BrothcastMessage.this);
        });

        binding.Toolbar.ToolbarTitle.setText(getResources().getString(R.string.BroadcastMessage));

        binding.FloatingActionButton.setOnClickListener(view -> {
            var dialog = new AlertDialog.Builder(BrothcastMessage.this);
            var v = BrothcastmessagedialogBinding.inflate(getLayoutInflater(), null, false);
            dialog.setView(v.getRoot());

            var d = dialog.create();
            d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            d.show();

            v.SendBroadcastBtn.setOnClickListener(view1 -> {
                var Topic = v.TopicInput.getText().toString().trim();
                var Details = v.DetailsInput.getText().toString().trim();
                if(Topic.isEmpty()){
                    Toast.SetMessage(getApplicationContext(), "Topic is empty");
                }else if(Details.isEmpty()){
                    Toast.SetMessage(getApplicationContext(), "Details is empty");
                }else {
                    d.dismiss();
                    progressDialog.ProgressDialog(BrothcastMessage.this);
                    viewModel.SendNotification(Details, DataManager.Broadcast, null, null, Topic)
                            .observe(this, aBoolean -> {
                                if(aBoolean){
                                    progressDialog.CancelProgressDialog();
                                }else {
                                    progressDialog.CancelProgressDialog();
                                }
                            });
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Animatoo.animateSlideRight(BrothcastMessage.this);
    }
}