package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class DashboardAdPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference AdManager;
    private FirebaseAuth Mauth;

    public DashboardAdPOST(Application application) {
        this.application = application;
        AdManager = FirebaseFirestore.getInstance().collection(DataManager.AdManager);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> DashboardAdsPOST(String WebUrl, String PhotoUrl, String Type) {
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {

            var Timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.DashBoard, Type);
            map.put(DataManager.WebUrl, WebUrl);
            map.put(DataManager.PhotoUrl, PhotoUrl);

            AdManager.document(DataManager.DashBoard)
                    .set(map).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            data.setValue(true);
                            Toast.SetMessage(application, "add success");
                        } else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });
        }
        return data;
    }
}
