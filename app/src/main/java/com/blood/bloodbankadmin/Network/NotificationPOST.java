package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class NotificationPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference NotificationRef;
    private FirebaseAuth Mauth;

    public NotificationPOST(Application application){
        this.application = application;
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SendNotification(String Message, String MessageType, String ReceiverUID, String SenderUID, String NotificationTopic){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            var Timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.Message, Message);
            map.put(DataManager.MessageType, MessageType);
            map.put(DataManager.ReceiverUID, ReceiverUID);
            map.put(DataManager.SenderUID, SenderUID);
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.MessageTopic, NotificationTopic);

            NotificationRef.document(String.valueOf(Timestamp)).set(map)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                            Toast.SetMessage(application, "Send success");
                        }else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });
        }
        return data;
    }
}
