package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class CreateProfilePOST {

    private MutableLiveData<Boolean> data;
    private CollectionReference MSuperAdminRe;
    private FirebaseAuth Mauth;
    private Application application;

    public CreateProfilePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MSuperAdminRe = FirebaseFirestore.getInstance().collection(DataManager.SuperAdmin);
    }

    public LiveData<Boolean> SetUpProfile(String Email, String Name, String PhotoUri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {

            FirebaseMessaging.getInstance().getToken().addOnSuccessListener(s -> {
                var map = new HashMap<String, Object>();
                map.put(DataManager.Name, Name);
                map.put(DataManager.Email, Email);
                map.put(DataManager.PhotoUri, PhotoUri);
                map.put(DataManager.Token, s);


                MSuperAdminRe.document(FirebaseUser.getUid()).get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (!task.getResult().exists()) {
                            MSuperAdminRe.document(FirebaseUser.getUid())
                                    .set(map).addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            data.setValue(true);
                                            Toast.SetMessage(application, "Success");
                                        } else {
                                            data.setValue(false);
                                            Toast.SetMessage(application, task1.getException().getMessage());
                                        }
                                    }).addOnFailureListener(e -> {
                                        data.setValue(false);
                                        Toast.SetMessage(application, e.getMessage());
                                    });
                        }else {
                            data.setValue(false);
                            Toast.SetMessage(application, "Sign In Success");
                        }
                    } else {
                        Toast.SetMessage(application, task.getException().getMessage());
                        data.setValue(false);
                    }
                }).addOnFailureListener(e -> {
                    Toast.SetMessage(application, e.getMessage());
                    data.setValue(false);
                });
            });

        }
        return data;
    }
}
