package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;

import java.util.List;

public class LostAndFoundTypeGET {

    private Application application;
    private MutableLiveData<List<LostAndFoundTypeModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference LostAndFoundTypeRef;

    public LostAndFoundTypeGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LostAndFoundTypeRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFoundType);
    }

    public LiveData<List<LostAndFoundTypeModel>> GetLostAndFoundType(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            LostAndFoundTypeRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(LostAndFoundTypeModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
