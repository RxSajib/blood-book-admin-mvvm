package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class RegisterAsDonorUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference RegisterAsDonorRef;
    private FirebaseAuth Auth;

    public RegisterAsDonorUpdate(Application application){
        this.application = application;
        Auth = FirebaseAuth.getInstance();
        RegisterAsDonorRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Boolean> UpdateRegisterAsDonor(String UID, String GivenName, String SureName, String FatherName,
                                                   String BloodGroup, String Age, String Weight, String Country, String Province, String District, String AreaName, String PhoneNumber, String RoshanNumber, String EtisalatNumber,
                                                   String AWCCNumber, String MTNNumber, String WhatsappNumber, String EmailAddress, String DonateMonth, String LastDonationDate, String LastDonationDayCount){
        data = new MutableLiveData<>();
        var FirebaseUser = Auth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.GivenName, GivenName);
            map.put(DataManager.Surname, SureName);
            map.put(DataManager.FatherName, FatherName);
            map.put(DataManager.BloodGroup, BloodGroup);
            map.put(DataManager.Age, Age);
            map.put(DataManager.Width, Weight);
            map.put(DataManager.Country, Country);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.AreaName, AreaName);
            map.put(DataManager.MainPhoneNumber, PhoneNumber);
            map.put(DataManager.RoshanNumber, RoshanNumber);
            map.put(DataManager.EtisalatNumber, EtisalatNumber);
            map.put(DataManager.AWCCNumber, AWCCNumber);
            map.put(DataManager.MTNNumber, MTNNumber);
            map.put(DataManager.WhatsappNumber, WhatsappNumber);
            map.put(DataManager.EmailAddress, EmailAddress);
            map.put(DataManager.DonateMonth, DonateMonth);
            map.put(DataManager.LastDonationDate, LastDonationDate);
            map.put(DataManager.LastDonationDayCount, LastDonationDayCount);

            RegisterAsDonorRef.document(UID)
                    .update(map).addOnCompleteListener(task -> {
                       if(task.isSuccessful()){
                           data.setValue(true);
                           Toast.SetMessage(application, "Update success");
                       }else {
                           data.setValue(false);
                           Toast.SetMessage(application, task.getException().getMessage());
                       }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });
        }
        return data;
    }
}
