package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.DonorModel;

public class SingleDonorUserGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference DonorRef;
    private MutableLiveData<DonorModel> data;

    public SingleDonorUserGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<DonorModel> GetDonorUser(String UID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            DonorRef.document(UID).addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(value.exists()){

                }else {
                    data.setValue(null);
                }

            });
        }
        return data;
    }
}
