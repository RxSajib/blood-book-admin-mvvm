package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class VideoAdsPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference VideoAdsRef;
    private FirebaseAuth Mauth;

    public VideoAdsPOST(Application application){
        this.application = application;
        VideoAdsRef = FirebaseFirestore.getInstance().collection(DataManager.VideoAds);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UploadVideoAds(String Uri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var map = new HashMap<String, Object>();
            map.put(DataManager.VideoAdsUri, Uri);
            VideoAdsRef.document(DataManager.VideoAds).set(map).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.SetMessage(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
