package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbankadmin.Utils.Toast;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPasswordPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public ResetPasswordPOST(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> ResetPassword(String EmailAddress) {
        data = new MutableLiveData<>();

        Mauth.sendPasswordResetEmail(EmailAddress).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                data.setValue(true);
                Toast.SetMessage(application, "A reset link will be sent to your registered email address");
            } else {
                data.setValue(false);
                Toast.SetMessage(application, task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
            data.setValue(false);
            Toast.SetMessage(application, e.getMessage());
        });
        return data;
    }
}
