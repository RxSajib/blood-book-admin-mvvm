package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class LostAndFoundRemove {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference LostAndFoundRef;

    public LostAndFoundRemove(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LostAndFoundRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFound);
    }

    public LiveData<Boolean> RemoveLostAndFound(long DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            LostAndFoundRef.document(String.valueOf(DocumentID)).delete()
                    .addOnCompleteListener(task -> {
                       if(task.isSuccessful()){
                           data.setValue(true);
                           Toast.SetMessage(application, "Delete done");
                       }else {
                           Toast.SetMessage(application, task.getException().getMessage());
                           data.setValue(false);
                       }
                    }).addOnFailureListener(e -> {
                        Toast.SetMessage(application, e.getMessage());
                        data.setValue(false);
                    });
        }
        return data;
    }
}
