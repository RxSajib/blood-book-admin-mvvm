package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import java.util.HashMap;

public class AdminProfilePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference AdminRef;
    private FirebaseAuth Mauth;


    public AdminProfilePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        AdminRef = FirebaseFirestore.getInstance().collection(DataManager.Admin);
    }

    public LiveData<Boolean> SetAdminProfile(String Email, String Password, String Name, String PhotoUri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){


            FirebaseMessaging.getInstance().deleteToken().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    FirebaseMessaging.getInstance().getToken().addOnSuccessListener(new OnSuccessListener<String>() {
                        @Override
                        public void onSuccess(String s) {
                            var map = new HashMap<String, Object>();
                            map.put(DataManager.Email,  Email);
                            map.put(DataManager.Password, Password);
                            map.put(DataManager.Admin, DataManager.Admin);
                            map.put(DataManager.Token, s);
                            map.put(DataManager.Name, Name);
                            map.put(DataManager.PhotoUri, PhotoUri);
                            map.put(DataManager.UID, FirebaseUser.getUid());

                            AdminRef.document(DataManager.User).set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        data.setValue(true);
                                    }else {
                                        data.setValue(false);
                                        Toast.SetMessage(application, task.getException().getMessage());
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    data.setValue(false);
                                    Toast.SetMessage(application, e.getMessage());
                                }
                            });
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });
                }else {
                    data.setValue(false);
                    Toast.SetMessage(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
