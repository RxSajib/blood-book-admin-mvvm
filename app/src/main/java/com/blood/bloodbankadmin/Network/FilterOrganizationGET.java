package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.OrganizationModel;
import java.util.List;

public class FilterOrganizationGET {

    private Application application;
    private MutableLiveData<List<OrganizationModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference OrganizationRef;

    public FilterOrganizationGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        OrganizationRef = FirebaseFirestore.getInstance().collection(DataManager.Organization);
    }

    public LiveData<List<OrganizationModel>> FilterOrganization(String Country, String Province, String District, String OrganizationType){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            var q = OrganizationRef.whereEqualTo(DataManager.Country, Country).whereEqualTo(DataManager.Province, Province)
                    .whereEqualTo(DataManager.District, District).whereEqualTo(DataManager.OrganizationType, OrganizationType);

            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(OrganizationModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
