package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.blood.bloodbankadmin.Data.FcmResponse;
import com.blood.bloodbankadmin.Data.Model.NotifactionResponse;
import com.blood.bloodbankadmin.Network.FCM.FcmApi;
import com.blood.bloodbankadmin.Network.FCM.FcmClint;
import com.blood.bloodbankadmin.Utils.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendNotificationPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FcmApi fcmApi;


    public SendNotificationPOST(Application application){
        this.application = application;
        fcmApi = FcmClint.getRetrofit().create(FcmApi.class);
    }

    public LiveData<Boolean> SendNotificationToUser(NotifactionResponse response){
        data = new MutableLiveData<>();
        fcmApi.sendNotification(response).enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<FcmResponse> call,@NonNull Response<FcmResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(true);
                }else {
                    data.setValue(false);
                    Toast.SetMessage(application, response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<FcmResponse> call,@NonNull Throwable t) {
                data.setValue(false);
                Toast.SetMessage(application, t.getMessage());
            }
        });
        return data;
    }
}
