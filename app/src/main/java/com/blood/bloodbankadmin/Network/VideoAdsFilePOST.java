package com.blood.bloodbankadmin.Network;

import android.app.Application;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class VideoAdsFilePOST {

    private Application application;
    private MutableLiveData<String> data;
    private StorageReference VideoStoreRef;
    private FirebaseAuth Mauth;

    public VideoAdsFilePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        VideoStoreRef = FirebaseStorage.getInstance().getReference().child(DataManager.VideoAds);
    }

    public LiveData<String> UploadVideoFileAds(Uri VideoUri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var Timestamp = System.currentTimeMillis();
            var storage = VideoStoreRef.child(VideoUri.getLastPathSegment()+String.valueOf(Timestamp));
            storage.putFile(VideoUri).addOnSuccessListener(taskSnapshot -> {
                if(taskSnapshot.getMetadata() != null){
                    if(taskSnapshot.getStorage() != null){
                        var uploadtask = taskSnapshot.getStorage().getDownloadUrl();
                        uploadtask.addOnSuccessListener(uri -> {
                            data.setValue(uri.toString());
                        }).addOnFailureListener(e -> {
                            data.setValue(null);
                            Toast.SetMessage(application, e.getMessage());
                        });
                    }
                }
            }).addOnFailureListener(e -> {
                data.setValue(null);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
