package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;
import java.util.HashMap;

public class BloodDonationHistoryUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference RegisterDonationHistoryRef;
    private FirebaseAuth Mauth;

    public BloodDonationHistoryUpdate(Application application){
        this.application = application;
        RegisterDonationHistoryRef = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> RegisterDonationUpdate(String UID, String BloodDonationDate, String Country, String District, String HospitalName, String Province, String QuantityOfBlood){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            var map = new HashMap<String, Object>();
            map.put(DataManager.BloodDonationDate, BloodDonationDate);
            map.put(DataManager.Country, Country);
            map.put(DataManager.District, District);
            map.put(DataManager.HospitalName, HospitalName);
            map.put(DataManager.Province, Province);
            map.put(DataManager.QuantityOfBlood, QuantityOfBlood);

            RegisterDonationHistoryRef.document(UID).update(map).addOnCompleteListener(task -> {
               if(task.isSuccessful()){
                   data.setValue(true);
                   Toast.SetMessage(application, "Success");
               }else {
                   data.setValue(false);
                   Toast.SetMessage(application, task.getException().getMessage());
               }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
