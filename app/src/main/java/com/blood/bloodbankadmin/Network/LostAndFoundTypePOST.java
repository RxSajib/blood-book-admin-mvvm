package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class LostAndFoundTypePOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference LostAndFoundRef;
    private FirebaseAuth Mauth;

    public LostAndFoundTypePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LostAndFoundRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFoundType);
    }

    public LiveData<Boolean> UploadLostAndFoundType(String Category){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.Category, Category);
            map.put(DataManager.DocumentKey, timestamp);
            map.put(DataManager.Timestamp, timestamp);

            LostAndFoundRef.document(String.valueOf(timestamp))
                    .set(map).addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                       data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });
        }
        return data;
    }
}
