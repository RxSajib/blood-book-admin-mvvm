package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class ReportDelete {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference ReportRef;
    private FirebaseAuth Mauth;

    public ReportDelete(Application application){
        this.application = application;
        ReportRef = FirebaseFirestore.getInstance().collection(DataManager.Report);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> ReportDelete(String DocumentID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            ReportRef.document(DocumentID).delete().addOnCompleteListener(task -> {
               if(task.isSuccessful()){
                   data.setValue(true);
               }else {
                   data.setValue(false);
                   Toast.SetMessage(application, task.getException().getMessage());
               }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
