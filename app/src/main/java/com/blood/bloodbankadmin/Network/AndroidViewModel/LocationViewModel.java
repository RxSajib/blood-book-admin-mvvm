package com.blood.bloodbankadmin.Network.AndroidViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.blood.bloodbankadmin.CountryNameModel;
import com.blood.bloodbankadmin.DistrictNameModel;
import com.blood.bloodbankadmin.LostAndFoundTypeModel;
import com.blood.bloodbankadmin.OrganizationTypeModel;
import com.blood.bloodbankadmin.ProvinceNameModel;
import com.blood.bloodbankadmin.Utils.CountryNameDatabase;
import com.blood.bloodbankadmin.Utils.LocationDao;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class LocationViewModel extends AndroidViewModel {

    private CountryNameDatabase countryNameDatabase;
    private LocationDao countryDao;
    private Executor executor;


    public LocationViewModel(@NonNull Application application) {
        super(application);
        countryNameDatabase = CountryNameDatabase.GetContactDatabase(application);
        countryDao = countryNameDatabase.countryDao();
        executor = Executors.newSingleThreadExecutor();
    }

    //todo country name operation ---
    public void InsertCounyryNameData(List<CountryNameModel> countryNameModel){
        executor.execute(() -> {
            countryDao.InsertCountryNameData(countryNameModel);
        });
    }
    public LiveData<List<CountryNameModel>> GetCountryName(){
        return countryDao.GetCountryName();
    }
    public LiveData<List<CountryNameModel>> SearchByCountryName(String CountryName){
        return countryDao.SearchByCountryName(CountryName);
    }
    public void DeleteAllCountryNameData(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.DeleteAllFROMCOuntryData();
            }
        });
    }
    //todo country name operation ---


    //todo privince name operation ---
    public void InsertProvincesNameData(List<ProvinceNameModel> provinceNameModelList){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.InsertProvinces(provinceNameModelList);
            }
        });
    }
    public LiveData<List<ProvinceNameModel>> GetProvincesNameData(){
        return countryDao.GetProvinceName();
    }
    public LiveData<List<ProvinceNameModel>> SearchProvincesName(String Name){
        return countryDao.SearchProvince(Name);
    }
    public void DeleteAllProvinceNameData(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.DeleteAllFromProvincesData();
            }
        });
    }
    //todo privince name operation ---


    //todo district name operation --
    public void InsertDistrict(List<DistrictNameModel> list){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.InsertDistrict(list);
            }
        });
    }
    public LiveData<List<DistrictNameModel>> GetDistrictName(){
        return countryDao.GetDistrictName();
    }
    public LiveData<List<DistrictNameModel>> SearchDistrictName(String Name){
        return countryDao.GetDistrictName(Name);
    }
    public void DeleteDistrct(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.DeleteDistrict();
            }
        });
    }
    //todo district name operation --



    //todo organization data operation -----
    public void InsertOrganizationTypeData(List<OrganizationTypeModel> organizationTypeModel){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.InsertOrganizationType(organizationTypeModel);
            }
        });
    }
    public LiveData<List<OrganizationTypeModel>> GetOrganizationType(){
        return countryDao.GetOrganizationType();
    }
    public LiveData<List<OrganizationTypeModel>> SearchOrganizationType(String Name){
        return countryDao.SearchOrganizationType(Name);
    }
    public void DeleteOrganizationType(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                countryDao.DeleteOrganizationType();
            }
        });
    }
    //todo organization data operation -----


    //todo add lost and found ------------------
    public void InsertLostAndFound(List<LostAndFoundTypeModel> list){
        executor.execute(() -> {
            countryDao.InsertLostAndFound(list);
        });
    }
    public LiveData<List<LostAndFoundTypeModel>> GetLostAndFoundType(){
        return countryDao.GetLostAndType();
    }
    public void DeleteLostAndFoundType(){
        executor.execute(() -> {
            countryDao.DeleteLostAndFoundType();
        });
    }
    public LiveData<List<LostAndFoundTypeModel>> SearchLostAndFound(String NameType){
        return countryDao.SearchLostAndFoundType(NameType);
    }
    //todo add lost and found ------------------
}
