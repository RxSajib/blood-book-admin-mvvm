package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;
import java.util.Locale;

public class AddHospitalDataPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference BloodBankRef;
    private FirebaseAuth Mauth;

    public AddHospitalDataPOST(Application application) {
        this.application = application;
        BloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.Organization);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UploadOrganization(String NameOfOrganization, String OrganizationType, String PhoneNumber, String Country, String Province, String District, String Address, String ImageUri) {
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if (FirebaseUser != null) {

            var timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.NameOfOrganization, NameOfOrganization);
            map.put(DataManager.Search, NameOfOrganization.toLowerCase(Locale.ROOT));
            map.put(DataManager.OrganizationType, OrganizationType);
            map.put(DataManager.PhoneNumber, PhoneNumber);
            map.put(DataManager.Country, Country);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.Address, Address);
            map.put(DataManager.OrganizationLogo, ImageUri);
            map.put(DataManager.Timestamp, timestamp);
            map.put(DataManager.DocumentKey, timestamp);

            BloodBankRef.document(String.valueOf(timestamp)).set(map).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    data.setValue(true);
                } else {
                    data.setValue(false);
                    Toast.SetMessage(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
