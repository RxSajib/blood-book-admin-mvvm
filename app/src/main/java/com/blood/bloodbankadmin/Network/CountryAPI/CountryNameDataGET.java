package com.blood.bloodbankadmin.Network.CountryAPI;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.blood.bloodbankadmin.CountryNameResponse;
import com.blood.bloodbankadmin.Utils.Toast;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryNameDataGET {

    private Application application;
    private MutableLiveData<List<CountryNameResponse>> data;
    private CountryAPI countryAPI;

    public CountryNameDataGET(Application application){
        this.application = application;
        countryAPI = CountryClint.getRetrofit().create(CountryAPI.class);
    }

    public LiveData<List<CountryNameResponse>> GetCountryName(){
        data = new MutableLiveData<>();
        countryAPI.GetCountryResponse().enqueue(new Callback<List<CountryNameResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<CountryNameResponse>> call,@NonNull Response<List<CountryNameResponse>> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    data.setValue(null);
                    Toast.SetMessage(application, "Error get country name");
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<CountryNameResponse>> call,@NonNull Throwable t) {
                data.setValue(null);
                Toast.SetMessage(application, t.getMessage());
            }
        });
        return data;
    }
}
