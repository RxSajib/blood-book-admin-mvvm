package com.blood.bloodbankadmin.Network;

import android.app.Application;
import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class BloodBankLogoUpdatePOST {

    private Application application;
    private MutableLiveData<String> data;
    private StorageReference BloodBankRef;
    private FirebaseAuth Mauth;

    public BloodBankLogoUpdatePOST(Application application){
        this.application = application;
        BloodBankRef = FirebaseStorage.getInstance().getReference().child(DataManager.BloodBank);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<String> UploadBloodBankIcon(Uri ImageUri){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var storeref = BloodBankRef.child(ImageUri.getLastPathSegment()+String.valueOf(System.currentTimeMillis()+".jpg"));
            storeref.putFile(ImageUri).addOnSuccessListener(taskSnapshot -> {
                if(taskSnapshot.getMetadata() != null){
                    if(taskSnapshot.getMetadata().getReference() != null){
                        var task = taskSnapshot.getStorage().getDownloadUrl();
                        task.addOnSuccessListener(uri -> data.setValue(uri.toString())).addOnFailureListener(e -> {
                            Toast.SetMessage(application, e.getMessage());
                            data.setValue(null);
                        });
                    }
                }
            }).addOnFailureListener(e -> {
                data.setValue(null);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
