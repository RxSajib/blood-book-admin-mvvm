package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;

public class RegisterDonorNumberExistsGET {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference DonorUserRef;
    private FirebaseAuth Mauth;

    public RegisterDonorNumberExistsGET(Application application){
        this.application = application;
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> SearchDonorNumber(String MainPhoneNumber){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = DonorUserRef.whereEqualTo(DataManager.MainPhoneNumber, MainPhoneNumber);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(false);
                    return;
                }
                if(value.isEmpty()){
                    data.setValue(false);
                }else {
                    data.setValue(true);
                }
            });
        }
        return data;
    }
}
