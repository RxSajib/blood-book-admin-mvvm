package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;

public class FeelingSickCountGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private CollectionReference DonorUserRef;
    private FirebaseAuth Mauth;

    public FeelingSickCountGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorUserRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Integer> GetFeelingSickUserCount(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = DonorUserRef.whereEqualTo(DataManager.ShowToOther, false);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }

                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.size());
                        }
                    }

                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
