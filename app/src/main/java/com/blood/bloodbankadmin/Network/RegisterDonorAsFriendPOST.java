package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class RegisterDonorAsFriendPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference DonorRef;

    public RegisterDonorAsFriendPOST(Application application) {
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        DonorRef = FirebaseFirestore.getInstance().collection(DataManager.DonorUser);
    }

    public LiveData<Boolean> RegisterBloodDonorFriend(String GivenName, String SureName, String FatherName, String BloodGroup, String Age, String Width,
                                                      String Country, String Province, String District, String AreaName, String MainPhoneNumber,
                                                      String RoshanNumber, String EtisalatNumber, String AWCCNumber, String MTNNumber, String WhatsappNumber,
                                                      String EmailAddress, String DonateMonth, String LastDonationDate, boolean EnableMyNumber, String LastDonationDayCount) {

        var FirebaseUser = Mauth.getCurrentUser();
        data = new MutableLiveData<>();
        if (FirebaseUser != null) {
            var timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.GivenName, GivenName);
            map.put(DataManager.Surname, SureName);
            map.put(DataManager.FatherName, FatherName);
            map.put(DataManager.BloodGroup, BloodGroup);
            map.put(DataManager.Age, Age);
            map.put(DataManager.Width, Width);
            map.put(DataManager.Country, Country);
            map.put(DataManager.Province, Province);
            map.put(DataManager.District, District);
            map.put(DataManager.AreaName, AreaName);
            map.put(DataManager.MainPhoneNumber, MainPhoneNumber);
            map.put(DataManager.RoshanNumber, RoshanNumber);
            map.put(DataManager.EtisalatNumber, EtisalatNumber);
            map.put(DataManager.AWCCNumber, AWCCNumber);
            map.put(DataManager.MTNNumber, MTNNumber);
            map.put(DataManager.WhatsappNumber, WhatsappNumber);
            map.put(DataManager.EmailAddress, EmailAddress);
            map.put(DataManager.DonateMonth, DonateMonth);
            map.put(DataManager.LastDonationDate, LastDonationDate);
            map.put(DataManager.SenderUID, FirebaseUser.getUid());
            map.put(DataManager.EnableMyNumber, null);
            map.put(DataManager.LastDonationDayCount, LastDonationDayCount);
            map.put(DataManager.Timestamp, timestamp);
            map.put(DataManager.DocumentKey, timestamp);
            map.put(DataManager.Type, DataManager.TypeRegisterAsFriend);

            DonorRef.document(String.valueOf(timestamp))
                    .set(map).addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                            Toast.SetMessage(application, "Send success");
                        }else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        Toast.SetMessage(application, e.getMessage());
                        data.setValue(false);
                    });
        }
        return data;
    }
}
