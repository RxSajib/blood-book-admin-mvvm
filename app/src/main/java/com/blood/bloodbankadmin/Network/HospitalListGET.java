package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.BloodBankModel;
import com.blood.bloodbankadmin.Data.DataManager;

import java.util.List;

public class HospitalListGET {

    private Application application;
    private MutableLiveData<List<BloodBankModel>> data;
    private CollectionReference BloodBankRef;
    private FirebaseAuth Mauth;

    public HospitalListGET(Application application){
        this.application = application;
        BloodBankRef = FirebaseFirestore.getInstance().collection(DataManager.Hospital);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<BloodBankModel>> GetHospitalName(){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = BloodBankRef.whereEqualTo(DataManager.UID, FirebaseUser.getUid());
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(BloodBankModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
