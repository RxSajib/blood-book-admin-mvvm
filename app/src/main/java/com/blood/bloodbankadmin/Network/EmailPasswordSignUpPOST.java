package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.blood.bloodbankadmin.Utils.Toast;

public class EmailPasswordSignUpPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public EmailPasswordSignUpPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> EmailPasswordSignUp(String Email, String Password){
        data = new MutableLiveData<>();

        Mauth.createUserWithEmailAndPassword(Email, Password).addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                data.setValue(true);
                Toast.SetMessage(application, "Success");
            }else {
                data.setValue(false);
                Toast.SetMessage(application, task.getException().getMessage());
            }
        }).addOnFailureListener(e -> {
           data.setValue(false);
            Toast.SetMessage(application, e.getMessage());
        });
        return data;
    }
}
