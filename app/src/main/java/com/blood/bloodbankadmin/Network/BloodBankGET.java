package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.blood.bloodbankadmin.BloodBankModel;
import com.blood.bloodbankadmin.Data.DataManager;

public class BloodBankGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference BloodbankRef;
    private MutableLiveData<BloodBankModel> data;

    public BloodBankGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        BloodbankRef = FirebaseFirestore.getInstance().collection(DataManager.BloodBank);
    }

    public LiveData<BloodBankModel> GetBloodBank(){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            BloodbankRef.document(FirebaseUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(error != null){
                        data.setValue(null);
                        return;
                    }
                    if(value.exists()){
                        data.setValue(value.toObject(BloodBankModel.class));

                    }else {
                        data.setValue(null);
                    }
                }
            }) ;
        }
        return data;
    }
}
