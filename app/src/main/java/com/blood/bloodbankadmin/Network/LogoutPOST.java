package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;

public class LogoutPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;

    public LogoutPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> LogOutAccount(){
        data = new MutableLiveData<>();
        Mauth.signOut();
        data.setValue(true);

        return data;
    }

}
