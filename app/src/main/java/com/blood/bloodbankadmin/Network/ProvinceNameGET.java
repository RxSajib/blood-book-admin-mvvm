package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.ProvinceNameModel;

import java.util.List;

public class ProvinceNameGET {
    private Application application;
    private MutableLiveData<List<ProvinceNameModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference LocationRef;


    public ProvinceNameGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LocationRef = FirebaseFirestore.getInstance().collection(DataManager.Location);
    }

    public LiveData<List<ProvinceNameModel>> GetProvinceData(String CountryName){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = LocationRef.whereEqualTo(DataManager.CountryName, CountryName);
            q.addSnapshotListener((value, error) -> {
                if (error != null) {
                    data.setValue(null);
                    return;
                }
                if (!value.isEmpty()) {
                    for (var ds : value.getDocumentChanges()) {
                        if (ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.MODIFIED) {
                            data.setValue(value.toObjects(ProvinceNameModel.class));
                        }
                    }
                } else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
