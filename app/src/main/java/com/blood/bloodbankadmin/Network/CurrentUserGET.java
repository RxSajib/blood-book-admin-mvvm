package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.UserModel;

public class CurrentUserGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference MUserRef;
    private MutableLiveData<UserModel> data;

    public CurrentUserGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        MUserRef = FirebaseFirestore.getInstance().collection(DataManager.SuperAdmin);
    }

    public LiveData<UserModel> GetCurrentUser(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser.getUid() != null){
            MUserRef.document(FirebaseUser.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot value, @Nullable FirebaseFirestoreException error) {
                    if(value != null){
                        data.setValue(null);
                        return;
                    }
                    if(value.exists()){
                        data.setValue(value.toObject(UserModel.class));
                    }else {
                        data.setValue(null);
                    }
                }
            });
        }
        return data;
    }
}
