package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.LostAndFoundModel;

import java.util.List;
import java.util.Locale;

public class SearchLostAndFoundGET {

    private Application application;
    private MutableLiveData<List<LostAndFoundModel>> data;
    private CollectionReference LostAndFoundRef;
    private FirebaseAuth Mauth;

    public SearchLostAndFoundGET(Application application){
        this.application = application;
        LostAndFoundRef = FirebaseFirestore.getInstance().collection(DataManager.LostAndFound);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<LostAndFoundModel>> SearchLostAndFound(String Title){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var lowecase = Title.toLowerCase(Locale.ROOT);
            var q = LostAndFoundRef.orderBy(DataManager.Title).startAt(lowecase).endAt(lowecase+"\uf8ff");
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(LostAndFoundModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
