package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.RegisterDonationModel;

import java.util.List;

public class RegisterDonationGET {

    private Application application;
    private MutableLiveData<List<RegisterDonationModel>> data;
    private CollectionReference RegisterDonationRef;
    private FirebaseAuth Mauth;

    public RegisterDonationGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        RegisterDonationRef = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
    }

    public LiveData<List<RegisterDonationModel>> GetRegisterDonation(String UID){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = RegisterDonationRef.whereEqualTo(DataManager.SenderUID, UID);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(RegisterDonationModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
