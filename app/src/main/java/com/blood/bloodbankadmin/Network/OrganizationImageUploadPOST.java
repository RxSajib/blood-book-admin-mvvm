package com.blood.bloodbankadmin.Network;

import android.app.Application;
import android.net.Uri;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class OrganizationImageUploadPOST {

    private Application application;
    private MutableLiveData<String> data;
    private StorageReference OrganizationImageRef;
    private FirebaseAuth Mauth;

    public OrganizationImageUploadPOST(Application application){
        this.application = application;
        OrganizationImageRef = FirebaseStorage.getInstance().getReference().child(DataManager.OrganizationLogo);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<String> UploadOrganizationImage(Uri ImageUri){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var timestamp = System.currentTimeMillis();
            var Ref = OrganizationImageRef.child(ImageUri.getLastPathSegment()+String.valueOf(timestamp));
            Ref.putFile(ImageUri).addOnSuccessListener(taskSnapshot -> {
                if (taskSnapshot.getMetadata() != null) {
                    if (taskSnapshot.getMetadata().getReference() != null) {
                        var task = taskSnapshot.getStorage().getDownloadUrl();
                        task.addOnSuccessListener(uri -> data.setValue(uri.toString()));
                    }
                }
            }).addOnFailureListener(e -> {
                data.setValue(null);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
