package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.NotificationModel;
import com.google.firebase.firestore.Query;

import java.util.List;

public class BroadcastMessageGET {

    private Application application;
    private MutableLiveData<List<NotificationModel>> data;
    private CollectionReference NotificationRef;
    private FirebaseAuth Mauth;

    public BroadcastMessageGET(Application application){
        this.application  = application;
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<List<NotificationModel>> GetBroadcastMessage(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = NotificationRef.whereEqualTo(DataManager.MessageType, DataManager.Broadcast).orderBy(DataManager.Timestamp, Query.Direction.DESCENDING);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(NotificationModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
