package com.blood.bloodbankadmin.Network.FCM;


import com.blood.bloodbankadmin.Data.DataManager;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FcmClint {

    private static Retrofit retrofit;
    public static Retrofit getRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(DataManager.MessagingBASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
