package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.OrganizationTypeModel;

import java.util.List;

public class OrganizationTypeGET {

    private Application application;
    private MutableLiveData<List<OrganizationTypeModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference OrganizationRef;

    public OrganizationTypeGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        OrganizationRef = FirebaseFirestore.getInstance().collection(DataManager.OrganizationType);
    }

    public LiveData<List<OrganizationTypeModel>> GetOrganizationType(){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            OrganizationRef.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }

                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(OrganizationTypeModel.class));
                        }
                    }
                }
            });
        }
        return data;
    }
}
