package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class VideoAdsDelete {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference VideoAdsRef;

    public VideoAdsDelete(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        VideoAdsRef = FirebaseFirestore.getInstance().collection(DataManager.VideoAds);
    }

    public LiveData<Boolean> DeleteVideo(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            VideoAdsRef.document(DataManager.VideoAds).delete().addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                    Toast.SetMessage(application, "Success");
                }else {
                    data.setValue(false);
                    Toast.SetMessage(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
