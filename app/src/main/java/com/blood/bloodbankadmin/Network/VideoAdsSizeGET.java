package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;

public class VideoAdsSizeGET {

    private Application application;
    private MutableLiveData<Integer> data;
    private CollectionReference VideoAdsRef;
    private FirebaseAuth Mauth;

    public VideoAdsSizeGET(Application application){
        this.application = application;
        VideoAdsRef  = FirebaseFirestore.getInstance().collection(DataManager.VideoAds);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Integer> GetVideoAdsSize(){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            VideoAdsRef.document(DataManager.VideoAds).addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(value.exists()){
                    data.setValue(1);
                }else {
                    data.setValue(0);
                }
            });
        }
        return data;
    }
}
