package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.blood.bloodbankadmin.BloodRequestModel;
import com.blood.bloodbankadmin.Data.DataManager;

import java.util.List;

public class BloodRequestGET {

    private Application application;
    private FirebaseAuth Mauth;
    private CollectionReference RequestRef;
    private MutableLiveData<List<BloodRequestModel>> data;

    public BloodRequestGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        RequestRef = FirebaseFirestore.getInstance().collection(DataManager.BloodRequest);
    }

    public LiveData<List<BloodRequestModel>> GetBloodRequest(){
        var FirebaseUser = Mauth.getCurrentUser();
        data = new MutableLiveData<>();
        if(FirebaseUser != null){
            var q = RequestRef.orderBy(DataManager.BloodRequestTimestamp, Query.Direction.DESCENDING);
            q.addSnapshotListener((value, error) -> {
                if(error != null){
                    data.setValue(null);
                    return;
                }
                if(!value.isEmpty()){
                    for(var ds : value.getDocumentChanges()){
                        if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                            data.setValue(value.toObjects(BloodRequestModel.class));
                        }
                    }
                }else {
                    data.setValue(null);
                }
            });
        }
        return data;
    }
}
