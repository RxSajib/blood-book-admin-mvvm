package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class AddRegisterDonationPOST {

    private Application application;
    private CollectionReference RegisterDonationRef;
    private FirebaseAuth Mauth;
    private MutableLiveData<Boolean> data;

    public AddRegisterDonationPOST(Application application){
        this.application = application;
        RegisterDonationRef = FirebaseFirestore.getInstance().collection(DataManager.RegisterDonation);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> AddRegisterDonation(String Image, String SenderUID, String BloodDonationDate, String Country, String District, String HospitalName, String Province, String QuantityOfBlood){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var Timestamp = System.currentTimeMillis();
            var map = new HashMap<String, Object>();
            map.put(DataManager.BloodDonationDate, BloodDonationDate);
            map.put(DataManager.Country, Country);
            map.put(DataManager.District, District);
            map.put(DataManager.HospitalName, HospitalName);
            map.put(DataManager.Province, Province);
            map.put(DataManager.QuantityOfBlood, QuantityOfBlood);
            map.put(DataManager.Timestamp, Timestamp);
            map.put(DataManager.DocumentKey, Timestamp);
            map.put(DataManager.SenderUID, SenderUID);
            map.put(DataManager.RegisterDonationBloodImage, Image);

            RegisterDonationRef.document(String.valueOf(Timestamp)).set(map).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    data.setValue(true);
                    Toast.SetMessage(application, "Success");
                }else {
                    data.setValue(false);
                    Toast.SetMessage(application, task.getException().getMessage());
                }
            }).addOnFailureListener(e -> {
                data.setValue(false);
                Toast.SetMessage(application, e.getMessage());
            });
        }
        return data;
    }
}
