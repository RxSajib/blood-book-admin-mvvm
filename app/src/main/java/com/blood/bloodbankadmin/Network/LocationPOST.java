package com.blood.bloodbankadmin.Network;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class LocationPOST {

    private Application application;
    private MutableLiveData<Boolean> data;
    private FirebaseAuth Mauth;
    private CollectionReference LocationRef;

    public LocationPOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LocationRef = FirebaseFirestore.getInstance().collection(DataManager.Location);
    }

    public LiveData<Boolean> AddLocation(String Country, String Province, String District){
        data = new MutableLiveData<>();

        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){

            var map = new HashMap<String, Object>();
            map.put(DataManager.CountryName, Country);
            map.put(DataManager.District, District);
            map.put(DataManager.Provance, Province);

            LocationRef.document().set(map)
                    .addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            data.setValue(true);
                        }else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });

        }
        return data;
    }
}
