package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

import java.util.HashMap;

public class UserActiveUpdate {

    private Application application;
    private MutableLiveData<Boolean> data;
    private CollectionReference UserRef;
    private FirebaseAuth Mauth;


    public UserActiveUpdate(Application application){
        this.application = application;
        UserRef = FirebaseFirestore.getInstance().collection(DataManager.User);
        Mauth = FirebaseAuth.getInstance();
    }

    public LiveData<Boolean> UserActiveUpdate(boolean Status, String DocumentKey){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();

        if(FirebaseUser != null) {
            var map = new HashMap<String, Object>();
            map.put(DataManager.IsActive, Status);

            UserRef.document(DocumentKey)
                    .update(map).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            data.setValue(true);
                        } else {
                            data.setValue(false);
                            Toast.SetMessage(application, task.getException().getMessage());
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(false);
                        Toast.SetMessage(application, e.getMessage());
                    });


        }
        return data;

    }

}
