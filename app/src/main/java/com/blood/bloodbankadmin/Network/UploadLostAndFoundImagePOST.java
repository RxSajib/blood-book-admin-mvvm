package com.blood.bloodbankadmin.Network;

import android.app.Application;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.Utils.Toast;

public class UploadLostAndFoundImagePOST {

    private Application application;
    private FirebaseAuth Mauth;
    private MutableLiveData<String> data;
    private StorageReference LostAndFoundRef;

    public UploadLostAndFoundImagePOST(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        LostAndFoundRef = FirebaseStorage.getInstance().getReference().child(DataManager.LostAndFound);
    }

    public LiveData<String> UploadLostAndFoundImage(Uri ImageUri){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth. getCurrentUser();
        if(FirebaseUser != null){
            var Timestamp = System.currentTimeMillis();
            var storeref = LostAndFoundRef.child(ImageUri.getLastPathSegment()+String.valueOf(Timestamp));
            storeref.putFile(ImageUri)
                    .addOnSuccessListener(taskSnapshot -> {
                        if(taskSnapshot.getMetadata() != null){
                            if(taskSnapshot.getStorage() != null){
                                var task = taskSnapshot.getStorage().getDownloadUrl();
                                task.addOnSuccessListener(uri -> {
                                    data.setValue(uri.toString());
                                }).addOnFailureListener(e -> {
                                    data.setValue(null);
                                    Toast.SetMessage(application, e.getMessage());
                                });
                            }
                        }
                    }).addOnFailureListener(e -> {
                        data.setValue(null);
                        Toast.SetMessage(application, e.getMessage());
                    });
        }
        return data;
    }
}
