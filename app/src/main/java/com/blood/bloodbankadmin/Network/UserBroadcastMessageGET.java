package com.blood.bloodbankadmin.Network;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.blood.bloodbankadmin.Data.DataManager;
import com.blood.bloodbankadmin.NotificationModel;

import java.util.List;

public class UserBroadcastMessageGET {

    private Application application;
    private MutableLiveData<List<NotificationModel>> data;
    private FirebaseAuth Mauth;
    private CollectionReference NotificationRef;

    public UserBroadcastMessageGET(Application application){
        this.application = application;
        Mauth = FirebaseAuth.getInstance();
        NotificationRef = FirebaseFirestore.getInstance().collection(DataManager.Notification);
    }

    public LiveData<List<NotificationModel>> GetUserBroadcastMessage(String DocumentKey){
        data = new MutableLiveData<>();
        var FirebaseUser = Mauth.getCurrentUser();
        if(FirebaseUser != null){
            var q = NotificationRef.whereEqualTo(DataManager.ReceiverUID, DocumentKey).whereEqualTo(DataManager.MessageType, DataManager.UserBroadcastMessage);
            q.addSnapshotListener((value, error) -> {
               if(error != null){
                   data.setValue(null);
                   return;
               }
               if(!value.isEmpty()){
                   for(var ds : value.getDocumentChanges()){
                       if(ds.getType() == DocumentChange.Type.ADDED || ds.getType() == DocumentChange.Type.MODIFIED || ds.getType() == DocumentChange.Type.REMOVED){
                           data.setValue(value.toObjects(NotificationModel.class));
                       }
                   }
               }else {
                   data.setValue(null);
               }
            });
        }
        return data;
    }

}
