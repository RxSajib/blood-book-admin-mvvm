package com.blood.bloodbankadmin;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.squareup.picasso.Picasso;

import lombok.Data;

@Data
public class BloodBankModel {

    private String HospitalName, Email, Location, HospitalLogo;
    private long Timestamp, DocumentKey;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
