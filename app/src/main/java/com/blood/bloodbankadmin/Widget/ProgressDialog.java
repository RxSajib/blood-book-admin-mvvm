package com.blood.bloodbankadmin.Widget;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import androidx.databinding.DataBindingUtil;
import com.blood.bloodbankadmin.R;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ProgressDialog {
    @Inject
    public ProgressDialog(){

    }

    static AlertDialog alertDialog;


    public static void ProgressDialog(Context context){

        var layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        var Mbuilder = new AlertDialog.Builder(context);
        var binding = DataBindingUtil.inflate(layoutInflater, R.layout.progressbar, null, false);
        Mbuilder.setView(binding.getRoot());

        alertDialog = Mbuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    public static void CancelProgressDialog(){
        if(alertDialog != null){
            alertDialog.dismiss();
        }
    }
}
