package com.blood.bloodbankadmin.Widget;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import androidx.databinding.DataBindingUtil;
import com.blood.bloodbankadmin.R;
import com.blood.bloodbankadmin.databinding.TermsandconditionBinding;
import javax.inject.Inject;

public class TermsAndConditionDialog {

    @Inject
    public TermsAndConditionDialog(){}

    static AlertDialog alertDialog;
    private static OnClickContinue OnClickContinue;

    public static void Show(Context context){

        var layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        var Mbuilder = new AlertDialog.Builder(context);
        TermsandconditionBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.termsandcondition, null, false);
        Mbuilder.setView(binding.getRoot());

        alertDialog = Mbuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        binding.ContinueBtn.setOnClickListener(view -> {
            OnClickContinue.Click();
            if(alertDialog != null){
                alertDialog.dismiss();
            }
        });
    }

    public interface OnClickContinue{
        void Click();
    }
    public void OnClickLisiner(OnClickContinue OnClickContinue){
        this.OnClickContinue = OnClickContinue;
    }

}
