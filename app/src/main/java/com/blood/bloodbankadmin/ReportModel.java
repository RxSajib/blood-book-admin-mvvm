package com.blood.bloodbankadmin;

import lombok.Data;

@Data
public class ReportModel {
    private long DocumentKey, Timestamp;
    private String Message, ReceiverUID, SenderUID;
}
