package com.blood.bloodbankadmin.Application;

import com.blood.bloodbankadmin.DI.Componenet;
import com.blood.bloodbankadmin.DI.DaggerComponenet;
import javax.inject.Singleton;

@Singleton
public class Application extends android.app.Application {

    public static Componenet component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerComponenet.create();
    }
}
