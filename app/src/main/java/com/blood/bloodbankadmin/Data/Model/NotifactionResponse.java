package com.blood.bloodbankadmin.Data.Model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class NotifactionResponse {
    @SerializedName("notification")
    public com.blood.bloodbankadmin.Data.Model.Data data;

    @SerializedName("to")
    public String to;

    @SerializedName("content_available")
    private boolean content_available;

    @SerializedName("priority")
    private String priority;

    public NotifactionResponse(com.blood.bloodbankadmin.Data.Model.Data data, String to) {
        this.data = data;
        this.to = to;
    }

    public NotifactionResponse(com.blood.bloodbankadmin.Data.Model.Data data, String to, boolean content_available, String priority) {
        this.data = data;
        this.to = to;
        this.content_available = content_available;
        this.priority = priority;
    }
}
