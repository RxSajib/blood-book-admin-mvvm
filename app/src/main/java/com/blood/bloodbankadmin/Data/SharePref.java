package com.blood.bloodbankadmin.Data;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePref {


    private SharedPreferences preferences;
    private String Data;

    public SharePref(Context context){
        preferences = context.getSharedPreferences(Data, Context.MODE_PRIVATE);
    }

    public void SetData(String Key, String Val){
        var edit  = preferences.edit();
        edit.putString(Key, Val);
        edit.apply();
    }

    public String GetData(String Key){
        return preferences.getString(Key, null);
    }
}
