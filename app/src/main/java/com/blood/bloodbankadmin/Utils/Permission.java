package com.blood.bloodbankadmin.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Permission {

    public static boolean ReadExternalMemoryPermission(Activity activity, int PermissionCode){
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(activity, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, PermissionCode);
            return false;
        }
    }



    public static boolean PermissionCall(Activity context, int PermissionCode){
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, PermissionCode);
            return false;
        }
    }
}
