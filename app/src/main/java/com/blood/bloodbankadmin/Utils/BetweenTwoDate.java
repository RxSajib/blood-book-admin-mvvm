package com.blood.bloodbankadmin.Utils;

import android.os.Build;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class BetweenTwoDate {

    public static int daysBetweenDates(String date1, String date2) {

        long diffDays = 0;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            var dt1 = LocalDate.parse(date1);
            var dt2= LocalDate.parse(date2);

            diffDays = ChronoUnit.DAYS.between(dt1, dt2);
        }
        return Math.abs((int)diffDays);
    }
}
