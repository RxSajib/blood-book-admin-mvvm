package com.blood.bloodbankadmin;

import lombok.Data;

@Data
public class LocationModel {

    private String CountryName, District, Provance;
}
