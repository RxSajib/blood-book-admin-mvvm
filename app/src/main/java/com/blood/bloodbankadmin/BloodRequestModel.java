package com.blood.bloodbankadmin;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BloodRequestModel {
    private String PatientName, Age, BloodGroup, BloodCondition, Country, Province, District;
    private String BloodNeedFor, NumberOFQuantityPints, BloodRequiredTime, BloodRequiredDate;
    private String AttendantContactNumber, HospitalName, ExchangePossibility, TransportAvailability;
    private String TypeOfHbLevel, TypeOfPatientsCondition, SenderUID, Status;
    private long BloodRequestTimestamp;
    private long Timestamp, DocumentKey;
    private int AcceptedQuantity;
}
