package com.blood.bloodbankadmin;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(tableName = "LostAndFoundTypeDB")
public class LostAndFoundTypeModel {

    @PrimaryKey(autoGenerate = false)
    private @NonNull String Category;
    private long DocumentKey;
    private long Timestamp;
}
