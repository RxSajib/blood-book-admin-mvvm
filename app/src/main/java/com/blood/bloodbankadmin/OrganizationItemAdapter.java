package com.blood.bloodbankadmin;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.blood.bloodbankadmin.UI.ViewHolder.OrganizationItemViewHolder;
import com.blood.bloodbankadmin.databinding.OrganizationitemBinding;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class OrganizationItemAdapter extends RecyclerView.Adapter<OrganizationItemViewHolder> {
    @Setter @Getter
    private List<OrganizationTypeModel> list;

    @NonNull
    @Override
    public OrganizationItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        var l = LayoutInflater.from(parent.getContext());
        var v = OrganizationitemBinding.inflate(l, parent, false);
        return new OrganizationItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizationItemViewHolder holder, int position) {
        holder.binding.setOrganization(list.get(position));
    }

    @Override
    public int getItemCount() {
        if(list == null) {
            return 0;
        }return list.size();
    }
}
