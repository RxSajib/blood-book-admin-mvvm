package com.blood.bloodbankadmin;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import lombok.Data;

@Data
public class AdModel {

    private String DashBoard, PhotoUrl,WebUrl;
    private long DocumentKey,Timestamp;

    @BindingAdapter("android:loadImage")
    public static void loadImage(ImageView imageView, String Uri){
        com.squareup.picasso.Picasso.get().load(Uri).placeholder(R.drawable.profileplaceholder).into(imageView);
    }
}
