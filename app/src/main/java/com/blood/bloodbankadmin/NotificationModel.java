package com.blood.bloodbankadmin;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NotificationModel {

    private String Message;
    private String MessageType;
    private String ReceiverUID;
    private String SenderUID;
    private String Type;
    private String MessageTopic;
    private long Timestamp, DocumentKey;
}
